var contentBaseInfo = {
  update: '2022-10-10T08:14:05.797Z',
  counts: {
    news: 11,
    events: 4,
    gallery: 6,
    pubs: 6,
    jumbo: 6,
    banner: 2,
    faqs: 3,
    credits: 8,
    orari: 7
  }
};
var contentBase = {
  news: [
    {
      Sheet: 'news',
      id: '11',
      Id: '11',
      Data: '15/05/2022',
      Sopratitolo: "Museo dell'Intrattenimento Elettronico",
      Titolo: 'Commodore da ElettroLudica',
      Sottotitolo: '15 Maggio 2022',
      Brief: 'Un grande evento: FLASHBACK 2022 !!!',
      Testo: '<p>Incontra il CEO e il Management della nuova Commodore\n' +
        'Tra le vecchie glorie del passat️o sarà svelato il prossimo progetto Commodore!</p>',
      Testo2: '',
      Autore: 'Associazione ElettroLudica',
      Image: 'post-12-img.jpg',
      Image2: '',
      Categoria: 'News',
      Tags: 'Eventi;Museo'
    },
    {
      Sheet: 'news',
      id: '10',
      Id: '10',
      Data: '26/12/2021',
      Sopratitolo: "Museo dell'Intrattenimento Elettronico",
      Titolo: 'Aperture per le festività natalizie',
      Sottotitolo: 'All i want for Christmas is ElettroLudica',
      Brief: 'Durante le feste, giocate con noi!',
      Testo: `<p>Per le festività natalizie siamo aperti tutti i giorni (escluso 31/12) dal 24 dicembre al 6 gennaio, dalle ore 15 alle 20.</p><p>ElettroLudica ad Avezzano è:<br> Sale giochi in FREE PLAY: oltre 140 videogiochi "arcade da bar" e 35 flipper perfettamente restaurati e funzionanti. <br>Esposizione: oltre 300 console, 120 home computer e decine di altri cabinati e flipper storici come Computer Space, Pong, Space Invaders, Pac Man. <br>Sale prove con le console e gli home computer che hanno fatto la storia, funzionanti e accessibili. <br><br>Per l'ingresso è richiesta quota associativa, green pass e mascherina.<br><br>Per restare sempre aggiornato, puoi visitare la nostra pagina facebook: <a href="https://www.facebook.com/elettroLudica">https://www.facebook.com/elettroLudica</a></p>`,
      Testo2: '',
      Autore: 'Associazione ElettroLudica',
      Image: 'post-11-img.jpg',
      Image2: '',
      Categoria: 'News',
      Tags: 'Orari;Museo'
    },
    {
      Sheet: 'news',
      id: '8',
      Id: '8',
      Data: '19/07/2021',
      Sopratitolo: "Museo dell'Intrattenimento Elettronico",
      Titolo: 'Date e orari di apertura',
      Sottotitolo: 'Nuova pagina per gli orari!',
      Brief: '',
      Testo: `<p>E' stata creata una pagina dedicata <a href="https://elettroludica.it/#/orari"><b>Orari</b></a>, accessibile dal menu principale (la barra in alto a destra del logo, se siete su Desktop, apribile dal bottone in alto a sinistra se siete su Mobile), con le date e gli orari di apertura sempre aggiornati.</p>`,
      Testo2: '',
      Autore: 'Associazione ElettroLudica',
      Image: 'post-9-img.jpg',
      Image2: '',
      Categoria: 'News',
      Tags: 'Orari;Museo'
    },
    {
      Sheet: 'news',
      id: '9',
      Id: '9',
      Data: '01/08/2021',
      Sopratitolo: "Museo dell'Intrattenimento Elettronico",
      Titolo: 'Green Pass',
      Sottotitolo: 'Nuova normativa anti-covid',
      Brief: '',
      Testo: '<p>In base alla nuova normativa anti-covid, dal 6 agosto per accedere al Museo è obbligatorio il Green Pass.</p>',
      Testo2: '',
      Autore: 'Associazione ElettroLudica',
      Image: 'post-10-img.jpg',
      Image2: '',
      Categoria: 'News',
      Tags: 'Normativa;Covid'
    },
    {
      Sheet: 'news',
      id: '7',
      Id: '7',
      Data: '28/06/2021',
      Sopratitolo: "Museo dell'Intrattenimento Elettronico",
      Titolo: 'Attività del Museo',
      Sottotitolo: 'Ringraziamenti e aggiornamenti',
      Brief: '',
      Testo: '<p>Nel ringraziare tutti coloro che sono venuti a trovarci, gli illustri amici che hanno arricchito l’inaugurazione con conferenze ed eventi e tutte le testate cartacee e digitali che hanno raccontato questi due giorni fantastici, cogliamo l’occasione per comunicare che il Museo rimarrà chiuso per qualche giorno per le necessarie operazioni di sanificazione e pulizia. </p><p>\n' +
        'A breve pubblicheremo sul sito e su questa pagina gli orari di apertura e le modalità di tesseramento, grazie ancora a tutti e a presto!</p>',
      Testo2: '',
      Autore: 'Associazione ElettroLudica',
      Image: 'post-7-img.jpg',
      Image2: '',
      Categoria: 'News',
      Tags: 'Apertura;Beginning;Museo'
    },
    {
      Sheet: 'news',
      id: '1',
      Id: '1',
      Data: '10/06/2021',
      Sopratitolo: "Museo dell'Intrattenimento Elettronico",
      Titolo: 'Inaugurazione: 26 e 27 Giugno',
      Sottotitolo: 'elettroLudica',
      Brief: "Programma dell'inaugurazione",
      Testo: '<p>L’inaugurazione del Museo dell’Intrattenimento Elettronico mira a offrire una vasta gamma di eventi dedicati sia agli appassionati che ai neofiti.\n' +
        '</p><p>\n' +
        'Gli interventi includono diverse conferenze tenute dai maggiori esperti del settore attualmente in attività in Italia, alcuni incontri interattivi durante i quali i visitatori potranno trovare risposta alle loro curiosità e degli eventi musicali dal vivo.\n' +
        '</p><p>\n' +
        'Le conferenze e gli eventi musicali, così come l’intera inaugurazione, rispettano pienamente le vigenti normative anti-Covid e prevedono quindi posti a sedere obbligatori e correttamente distanziati.\n' +
        '</p><p>\n' +
        'Questo il programma completo della manifestazione:\n' +
        '</p>\n' +
        '<h3>Sabato 26 giugno:</h3>\n' +
        '<p>\n' +
        '<b>Conferenze:</b><br>\n' +
        `10:00 - A. Di Berardino "L'Associazione Culturale elettroLudica"<br>\n` +
        '11:00 - M. Vernillo: "Zaccaria: gli arcade"<br>\n' +
        '12:00 - F. Croci: "Zaccaria: i flipper"<br>\n' +
        '16:00 - F. Rubeo: "Nutting Computer Space: il primo coin-op della storia"<br>\n' +
        '17:00 - F. Bortolotti: "Il retrogaming ai tempi di Twitch"<br>\n' +
        '<br>\n' +
        '<b>Incontri Interattivi:</b><br>\n' +
        `15:00 - A. Nati "L'era d'oro dei videogiochi da sala"<br>\n` +
        '<br>\n' +
        '<b>Eventi Musicali:</b><br>\n' +
        '18:00 Sabrina De Mitri e Roberta Mai: "Piano, Sax & Looping"<br>\n' +
        '21:00 Kenobit: "Game Boy Music"<br>\n' +
        '<br>\n' +
        '</p>\n' +
        '<h3>Domenica 27 giugno:</h3>\n' +
        '<p>\n' +
        '<b>Conferenze:</b><br>\n' +
        '11:00 - C. Santagostino: "La nascita del mercato videoludico in Italia"<br>\n' +
        `<strike style='color:#ff0000'> 12:00 - A. Bolgia e I. Maggiorelli: "Video & Giochi"</strike><br>\n` +
        '16:00 - P. Cognetti: "Quando il computer divenne personal"<br>\n' +
        `17:00 - E. Pede: "Il Museo dell'Intrattenimento Elettronico"<br>\n` +
        '<br>\n' +
        '<b>Incontri Interattivi:</b><br>\n' +
        `10:00 - M. Ciciotti "La Commodore: dal VIC all'Amiga"<br>\n` +
        '15:00 - A. Longo "Elettronica e tubi catodici negli arcade"<br>\n' +
        '<br>\n' +
        '<b>Eventi Musicali:</b><br>\n' +
        '18:00 Sabrina De Mitri, Roberta Mai & Guest: "Jam Session"<br>\n' +
        '<br>\n' +
        '</p>',
      Testo2: '',
      Autore: 'Associazione ElettroLudica',
      Image: 'post-1-img.jpg',
      Image2: '',
      Categoria: 'News',
      Tags: 'Apertura;Beginning;Museo'
    },
    {
      Sheet: 'news',
      id: '2',
      Id: '2',
      Data: '10/06/2021',
      Sopratitolo: "Museo dell'Intrattenimento Elettronico",
      Titolo: 'Sicurezza e normative anti-Covid',
      Sottotitolo: 'elettroLudica',
      Brief: "Sicurezza per l'Inaugurazione",
      Testo: '<p>\n' +
        'La perdurante pandemia da Covid-19 ha imposto l’adozione di tutta una serie di misure di sicurezza e di comportamenti da tenere per minimizzare la diffusione del virus. L’inaugurazione del Museo dell’Intrattenimento Elettronico tiene conto di tali fattori e rispetta pienamente le vigenti normative.\n' +
        '</p><p>\n' +
        'Per accedere alla struttura, ai visitatori è richiesto di indossare la mascherina e un paio di guanti monouso. La mascherina non deve essere mai abbassata o rimossa e i guanti devono essere indossati in ogni momento, sino al termine della visita. Le mascherine e i guanti sono disponibili all’ingresso, in caso di necessità.\n' +
        '</p><p>\n' +
        'È inoltre obbligatorio mantenere una distanza di sicurezza di almeno un metro tra una persona e l’altra. Tale distanza deve essere rispettata anche durante gli eventi con posti a sedere, che prevedono il corretto distanziamento delle sedie e ovviamente dei relatori nel caso delle conferenze.\n' +
        '</p><p>\n' +
        'La pandemia sta rallentando, ma non è finita e c’è ancora bisogno degli sforzi di tutti i cittadini per sconfiggerla definitivamente. Chiediamo quindi ai visitatori di fare la loro parte per garantire la sicurezza dell’inaugurazione, così che quest’ultima possa essere una gradevole occasione di divertimento senza alcuna preoccupazione.\n' +
        '</p>',
      Testo2: '',
      Autore: 'Associazione ElettroLudica',
      Image: 'post-2-img.jpg',
      Image2: '',
      Categoria: 'News',
      Tags: 'Apertura;Beginning;Museo'
    },
    {
      Sheet: 'news',
      id: '3',
      Id: '3',
      Data: '10/06/2021',
      Sopratitolo: "Museo dell'Intrattenimento Elettronico",
      Titolo: 'Dove alloggiare',
      Sottotitolo: 'elettroLudica',
      Brief: '',
      Testo: '<p>\n' +
        'L’inaugurazione del Museo dell’Intrattenimento Elettronico vanta un gran numero di eventi che sono stati distribuiti nell’arco di due giornate. Per i visitatori provenienti da altre città o altre regioni potrebbe quindi presentarsi l’esigenza di pernottare nei pressi della struttura.\n' +
        '</p><p>\n' +
        'L’Associazione Culturale elettroLudica ha quindi ritenuto opportuno fornire una lista delle strutture che offrono stanze per il pernottamento, ritenendo di offrire così a chi vorrà venirci a trovare un modo per semplificare la pianificazione della visita.\n' +
        '</p><p>\n' +
        'Non si tratta di una lista completa, dal momento che la zona offre numerose soluzioni dedicate a chi vuole alloggiare nei pressi di Avezzano, ma speriamo che possa rappresentare un buon punto di partenza per i visitatori intenzionati a rimanere per entrambe le giornate della manifestazione d’inaugurazione.\n' +
        '</p><p>\n' +
        '\n' +
        'Hotel Velino – Avezzano (Esercizio convenzionato)<br>Hotel Olimpia – Avezzano<br>\n' +
        '\n' +
        '<br>\n' +
        'B&B Acquaria – Avezzano<br>\n' +
        'Casina Dei Marsi – Avezzano<br>\n' +
        'C’Era un Lago – Avezzano<br>\n' +
        'Garibaldi33 – Avezzano<br>\n' +
        'Il Teatro – Avezzano<br>\n' +
        'La Dolce Vita – Tagliacozzo<br>\n' +
        'Locanda Corte De’ Guasconi – Antrosano<br>\n' +
        'Relais Garibaldi – Avezzano<br>\n' +
        'Relais Marlene – Avezzano<br>\n' +
        'Residence Luisa – Avezzano<br>\n' +
        'Residenza Duomo – Avezzano<br>\n' +
        'Risorgimento – Avezzano<br>\n' +
        'Sul Corso – Avezzano<br>\n' +
        'Villa Giulia – Avezzano<br>\n' +
        '</p>',
      Testo2: '',
      Autore: 'Associazione ElettroLudica',
      Image: 'post-3-img.jpg',
      Image2: '',
      Categoria: 'News',
      Tags: 'Apertura;Beginning;Museo'
    },
    {
      Sheet: 'news',
      id: '4',
      Id: '4',
      Data: '10/06/2021',
      Sopratitolo: "Museo dell'Intrattenimento Elettronico",
      Titolo: "Forze dell'Ordine e Operatori Sanitari",
      Sottotitolo: 'elettroLudica',
      Brief: '',
      Testo: '<p>\n' +
        'Le Forze dell’Ordine e gli operatori sanitari sono da sempre dei punti di riferimento sui quali la cittadinanza può fare affidamento, ma il loro operato è stato ancor più importante ed evidente durante la pandemia e continua a esserlo ora che la campagna vaccinale di massa è in pieno svolgimento.\n' +
        '</p><p>\n' +
        'L’Associazione Culturale elettroLudica ha quindi deciso di riconoscere pubblicamente il contributo offerto dalle Forze dell’Ordine e dagli operatori sanitari alla sicurezza e al benessere dei cittadini proponendo agli esponenti di tali categorie che vorranno farne richiesta una tessera associativa gratuita della durata di un mese.\n' +
        '</p><p>\n' +
        'Si tratta di un piccolo gesto che intende manifestare la nostra gratitudine per quel che le Forze dell’Ordine e gli operatori sanitari hanno fatto e stanno continuando a fare per tutti noi in questo periodo di grandi difficoltà, nella speranza che il loro operato contribuisca insieme a quello di tutti a porre presto fine all’emergenza in corso.\n' +
        '</p>',
      Testo2: '',
      Autore: 'Associazione ElettroLudica',
      Image: 'post-4-img.jpg',
      Image2: '',
      Categoria: 'News',
      Tags: 'Apertura;Beginning;Museo'
    },
    {
      Sheet: 'news',
      id: '5',
      Id: '5',
      Data: '10/06/2021',
      Sopratitolo: "Museo dell'Intrattenimento Elettronico",
      Titolo: "Cos'altro vedere",
      Sottotitolo: 'elettroLudica',
      Brief: 'Avezzano e la Marsica offrono molte opportunità per i visitatori e i turisti, sia dal punto di vista archeologico che da quello naturalistico. Quella che segue è una lista soltanto parziale delle attrattive offerte dal territorio, così numerose da non poter essere elencate neanche per sommi capi in questa sede.',
      Testo: '<p>\n' +
        'L’Associazione Culturale elettroLudica si augura che i visitatori provenienti da altre città o regioni possano trovare spunti interessanti all’interno di questa lista e affiancare altre stimolanti esperienze alla visita al Museo dell’Intrattenimento Elettronico.\n' +
        '</p><p>\n' +
        '<b>Alba Fucens</b><br>\n' +
        "“Alba Fucens è un sito archeologico sorto nel IV secolo a.C. come colonia di diritto latino in una posizione elevata e ben fortificata di circa 34 ettari a quasi 1000 m s.l.m. alle pendici del monte Velino in Abruzzo. Dichiarato monumento nazionale nel 1902, è situato nel contemporaneo comune di Massa d'Albe (AQ) a ridosso del borgo medievale di Albe, a pochi chilometri a nord della città di Avezzano.” - da Wikipedia\n" +
        '</p><p>\n' +
        '<b>Borgo e castello di Albe</b><br>\n' +
        '“Albe fu costruita nel Medioevo più in alto rispetto ad Alba Fucens, sulla collina di San Nicola. Fu feudo degli Orsini (XIV secolo) che vi costruirono il castello in posizione dominante sul lago Fucino. Il maniero si mantenne integro fino al terremoto della Marsica del 1915 quando fu gravemente danneggiato e abbandonato insieme al paese. Albe, ricostruita più in basso alle pendici del colle, è tornata a nuova vita con la realizzazione di varie attività e di una via di accesso ai ruderi del castello e del borgo medievale. ” - da Wikipedia\n' +
        '</p><p>\n' +
        '<b>Cunicoli di Claudio</b><br>\n' +
        `“I Cunicoli di Claudio sono un'opera idraulica di epoca romana, costituita da un lungo canale sotterraneo, sei cunicoli e trentadue pozzi, che l'imperatore Claudio fece costruire tra il 41 e il 52 d.C. per prosciugare il lago Fucino con lo scopo di salvaguardare i paesi marsicani dalle inondazioni o dalle malsane secche e rendere i terreni emersi coltivabili. Dal versante di Avezzano una buona parte delle acque lacustri defluì, attraverso l'emissario ipogeo del monte Salviano, nel fiume Liri dallo sbocco di Capistrello. Il canale sotterraneo rappresenta la più lunga galleria realizzata dai tempi antichi fino all'inaugurazione del traforo ferroviario del Frejus avvenuta nel 1871. […] Nel 1902 i cunicoli di Claudio, erroneamente detti anche "cunicoli di Nerone", sono stati inclusi tra i monumenti nazionali italiani. L'area, che rappresenta un sito d'interesse archeologico e speleologico, è tutelata da un parco inaugurato nel 1977.” - da Wikipedia\n` +
        '</p><p>\n' +
        '<b>Castello Piccolomini</b><br>\n' +
        "“Il Castello Piccolomini è un castello, sito nel centro storico di Celano (AQ), che domina la sottostante piana del Fucino. Dal dicembre 2014 il Ministero per i beni e le attività culturali lo gestisce tramite il Polo museale dell'Abruzzo, nel dicembre 2019 divenuto Direzione regionale Musei.” - da Wikipedia\n" +
        '</p><p>\n' +
        '<b>Monte Salviano</b><br>\n' +
        "“Il monte Salviano è un gruppo montuoso dell'appennino abruzzese, situato lungo lo spartiacque tra la conca del Fucino e i piani Palentini, nella Marsica, che include le cime dei monti Salviano e d'Aria (1011 m s.l.m.), Cimarani (1108 m s.l.m.) e San Felice (1030 m s.l.m.). Incluso nella riserva naturale guidata Monte Salviano è indicato tra i siti di interesse comunitario dell'Abruzzo.” - da Wikipedia\n" +
        '</p><p>\n' +
        '<b>Parco Nazionale d’Abruzzo, Lazio e Molise</b><br>\n' +
        "“Il parco nazionale d'Abruzzo, Lazio e Molise (PNALM, originariamente Parco nazionale d'Abruzzo divenuto tale con legge n. 93 del 23 marzo 2001 senza modificazioni amministrative) è uno dei parchi nazionali più antichi d'Italia, istituito l'11 gennaio 1923 con Regio decreto-legge (subito dopo il parco nazionale del Gran Paradiso) compreso per la maggior parte in provincia dell'Aquila (Abruzzo) e per la restante in quella di Frosinone (Lazio) ed in quella di Isernia (Molise).” - da Wikipedia\n" +
        '</p>',
      Testo2: '',
      Autore: 'Associazione ElettroLudica',
      Image: 'post-6-img.jpg',
      Image2: '',
      Categoria: 'News',
      Tags: 'Apertura;Beginning;Museo'
    },
    {
      Sheet: 'news',
      id: '6',
      Id: '6',
      Data: '10/06/2021',
      Sopratitolo: "Museo dell'Intrattenimento Elettronico",
      Titolo: "L'apertura e il futuro",
      Sottotitolo: 'elettroLudica',
      Brief: '',
      Testo: '<p>\n' +
        'L’inaugurazione del Museo dell’Intrattenimento Elettronico rappresenta un traguardo fondamentale per l’Associazione Culturale elettroLudica, che ha lavorato incessantemente per più di cinque anni per giungere a questo punto. Vogliamo quindi cogliere l’occasione per ringraziare tutti gli amici che ci hanno dato una mano lungo il tragitto e le istituzioni che hanno scelto di credere nel progetto portato avanti dall’Associazione e di supportarlo.\n' +
        '</p><p>\n' +
        'Un traguardo, però, non è necessariamente un punto di arrivo. L’Associazione intende infatti espandere nel tempo le finalità e gli scopi del Museo dell’Intrattenimento Elettronico per farlo diventare un punto di aggregazione, un luogo dedicato alla celebrazione della storia di questo specifico genere di intrattenimento e molto altro.\n' +
        '</p><p>\n' +
        'Saranno quindi numerose e frequenti le iniziative che l’Associazione organizzerà in futuro presso il Museo, così da ampliarne il respiro e da renderlo un vero luogo di rilevanza culturale nell’ambito del territorio. Pubblicheremo tutte le informazioni del caso su questo sito, man mano che diventeranno disponibili, e contiamo di poter parlare presto di nuovi progetti, interessanti collaborazioni, manifestazioni pubbliche e altro ancora.</p>',
      Testo2: '',
      Autore: 'Associazione ElettroLudica',
      Image: 'post-5-img.jpg',
      Image2: '',
      Categoria: 'News',
      Tags: 'Apertura;Beginning;Museo'
    }
  ],
  events: [
    {
      Sheet: 'events',
      id: '1',
      Id: '1',
      Data: '26-27/06/21',
      Sopratitolo: 'elettroLudica',
      Titolo: 'Incontri',
      Sottotitolo: 'Gli incontri previsti per le giornate inaugurali.',
      Brief: "Gli eventi interattivi rappresentano una soluzione perfetta per soddisfare la curiosità dei visitatori durante l'inaugurazione del Museo dell'Intrattenimento Elettronico. Hanno infatti luogo in vari punti della struttura, in modo decisamente dinamico, e permettono quindi ai visitatori di fare ogni genere di domande relative agli argomenti trattati.",
      Testo: '<p>\n' +
        "Antonio Nati, in quanto grande esperto di cabinati originali, ha scelto di collaborare con l'Associazione Culturale elettroLudica per fornire ai visitatori tutte le informazioni possibili riguardo a quella che viene chiamata l'epoca d'oro dei videogiochi da sala, solitamente indicata come il periodo compreso tra la fine degli anni '70 e i primi anni '80.\n" +
        '</p><p>\n' +
        "Mario Ciciotti è un membro dell'Associazione della prima ora e ha seguito l'evoluzione del mercato degli home computer sin dal principio, con particolare interesse per le macchine prodotte da Commodore. Ha quindi scelto di illustrare tali computer ai visitatori, molti dei quali avranno probabilmente parecchie curiosità in merito in virtù della popolarità di tale marchio nel nostro Paese.\n" +
        '</p><p>\n' +
        "Antonio Longo, infine, è il tecnico specializzato dell'Associazione ed è il responsabile della riparazione della maggior parte delle macchine attualmente utilizzabili dai visitatori. Di conseguenza, è senza dubbio la persona più indicata per illustrare l'elettronica interna e il funzionamento dei tubi catodici contenuti nei cabinati da sala, assai più complessi di quanto si potrebbe inizialmente immaginare.\n" +
        '</p>',
      Testo2: '',
      Autore: 'Associazione ElettroLudica',
      Image: 'tour-1.jpg',
      Image2: '',
      Categoria: 'Events',
      Tags: 'Apertura;Beginning'
    },
    {
      Sheet: 'events',
      id: '2',
      Id: '2',
      Data: '26-27/06/21',
      Sopratitolo: 'elettroLudica',
      Titolo: 'Conferenze',
      Sottotitolo: 'Le conferenze del 26 e 27 Giugno',
      Brief: "Le conferenze organizzate dall'Associazione Culturale elettroLudica in occasione dell'inaugurazione del Museo dell'Intrattenimento Elettronico vantano la partecipazione di alcuni dei maggiori esperti del settore attualmente in attività in Italia.",
      Testo: '<p>\n' +
        "Carlo Santagostino e Federico Croci, per esempio, sono stati tra i pionieri del giornalismo videoludico in Italia, oltre a essere indicati come veri e propri punti di riferimento nel campo del 'retrocomputing' per quanto riguarda Carlo e dei flipper per quel che concerne Federico.\n" +
        '</p><p>\n' +
        "Marco Vernillo, da parte sua, è attivissimo nell'ambito del restauro dei cabinati da sala ed è ben noto a tutti gli appassionati del settore, mentre Alessandro Bolgia e Igor Maggiorelli dispensano da tempo le loro conoscenze tecniche in ogni angolo della grande rete.\n" +
        '</p><p>\n' +
        "Paolo Cognetti ha messo a frutto la sua enorme conoscenza dei personal computer tramite le attività del Vintage Computer Club, che ha organizzato alcune delle più grandi manifestazioni dedicate agli appassionati di informatica in Italia. Il nome di Fabio 'Kenobit' Bortolotti, infine, è da tempo sinonimo di musica creata su hardware d'annata: è il principale esponente della 'Game Boy Music' nel nostro Paese e i suoi lavori sono apprezzati in tutto il mondo.\n" +
        '</p>',
      Testo2: '',
      Autore: 'Associazione ElettroLudica',
      Image: 'tour-2.jpg',
      Image2: '',
      Categoria: 'Events',
      Tags: 'Apertura;Free'
    },
    {
      Sheet: 'events',
      id: '3',
      Id: '3',
      Data: '26-27/06/21',
      Sopratitolo: 'elettroLudica',
      Titolo: 'Eventi Musicali',
      Sottotitolo: "L'apertura in musica",
      Brief: '',
      Testo: '<p>\n' +
        '<b>26 Giugno</b><br>\n' +
        'Alle ore 18:00<br>\n' +
        'Sabrina De Mitri e Roberta Mai<br>"Piano, Sax & Looping"<br>\n' +
        '<br>\n' +
        'Alle 21:00<br>\n' +
        'Kenobit<br>"Game Boy Music"<br>\n' +
        '<br>\n' +
        '\n' +
        '<b>27 Giugno</b><br>\n' +
        'Alle ore 18:00<br>\n' +
        'Sabrina De Mitri, Roberta Mai & Guest<br>"Jam Session"<br>\n' +
        '</p>',
      Testo2: '',
      Autore: 'Associazione ElettroLudica',
      Image: 'tour-3.jpg',
      Image2: '',
      Categoria: 'Events',
      Tags: 'Apertura;Beginning'
    },
    {
      Sheet: 'events',
      id: '4',
      Id: '4',
      Data: '26-27/06/21',
      Sopratitolo: 'elettroLudica',
      Titolo: "Le tue giornate al Museo dell'Intrattenimento Elettronico",
      Sottotitolo: 'Via Sandro Pertini, 105 - Avezzano (AQ)',
      Brief: "E' tutto gratutito!",
      Testo: '<p>In occasione dell’inaugurazione, l’accesso al Museo dell’Intrattenimento Elettronico è libero e gratuito. Le uniche limitazioni sono quelle dovute alle normative per la prevenzione della diffusione del virus responsabile per la pandemia da Covid-19, che raccomandiamo di osservare con attenzione.\n' +
        '</p><p>\n' +
        'Dopo l’inaugurazione l’accesso al Museo richiederà la sottoscrizione di una tessera associativa con l’Associazione Culturale elettroLudica. La tessera sarà disponibile in varie versioni caratterizzate da una diversa durata della sottoscrizione; tutte le informazioni in merito potranno essere richieste all’Associazione presso il Museo o tramite i canali di contatto riportati su questo stesso sito.\n' +
        '</p><p>\n' +
        'In entrambi i casi, una volta ottenuto l’accesso al Museo dell’Intrattenimento Elettronico l’uso dei cabinati, dei flipper e delle macchine messe a disposizione dei visitatori nelle sale prove è totalmente gratuito, e in particolare è incluso nella quota di sottoscrizione dopo la manifestazione gratuita per l’inaugurazione.\n' +
        '</p><p>\n' +
        'In sintesi: il gioco è illimitato e gratuito, tutto quel che chiediamo ai nostri visitatori è di divertirsi in sicurezza!\n' +
        '</p>',
      Testo2: '',
      Autore: 'Associazione ElettroLudica',
      Image: 'tour-4.jpg',
      Image2: '',
      Categoria: 'Events',
      Tags: 'Apertura;Free'
    }
  ],
  gallery: [
    {
      Sheet: 'gallery',
      id: '1',
      Id: '1',
      Data: '10/06/2021',
      Sopratitolo: "Museo dell'Intrattenimento Elettronico",
      Titolo: 'Gallery 1 sator',
      Sottotitolo: 'elettroLudica',
      Brief: 'Lorem ipsum sator arepo tenet opera rotas',
      Testo: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
      Testo2: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
      Autore: 'Associazione ElettroLudica',
      Image: 'article-1.jpg',
      Image2: '',
      Categoria: 'Gallery',
      Tags: 'Apertura;Beginning'
    },
    {
      Sheet: 'gallery',
      id: '2',
      Id: '2',
      Data: '10/06/2021',
      Sopratitolo: "Museo dell'Intrattenimento Elettronico",
      Titolo: 'Gallery 2 arepo',
      Sottotitolo: 'elettroLudica',
      Brief: 'Lorem ipsum sator arepo tenet opera rotas',
      Testo: 'Lorem ipsum sator arepo tenet opera rotas',
      Testo2: '',
      Autore: 'Associazione ElettroLudica',
      Image: 'article-2.jpg',
      Image2: '',
      Categoria: 'Gallery',
      Tags: 'Apertura;Free'
    },
    {
      Sheet: 'gallery',
      id: '3',
      Id: '3',
      Data: '10/06/2021',
      Sopratitolo: "Museo dell'Intrattenimento Elettronico",
      Titolo: 'Gallery 3 tenet',
      Sottotitolo: 'elettroLudica',
      Brief: 'Lorem ipsum sator arepo tenet opera rotas',
      Testo: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
      Testo2: '',
      Autore: 'Associazione ElettroLudica',
      Image: 'article-3.jpg',
      Image2: '',
      Categoria: 'Gallery'
    },
    {
      Sheet: 'gallery',
      id: '4',
      Id: '4',
      Data: '10/06/2021',
      Sopratitolo: "Museo dell'Intrattenimento Elettronico",
      Titolo: 'Gallery 4 opera',
      Sottotitolo: 'elettroLudica',
      Brief: 'Lorem ipsum sator arepo tenet opera rotas',
      Testo: 'Lorem ipsum sator arepo tenet opera rotas',
      Testo2: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
      Autore: 'Associazione ElettroLudica',
      Image: 'article-4.jpg',
      Image2: '',
      Categoria: 'Gallery'
    },
    {
      Sheet: 'gallery',
      id: '5',
      Id: '5',
      Data: '10/06/2021',
      Sopratitolo: "Museo dell'Intrattenimento Elettronico",
      Titolo: 'Gallery 5 rotas',
      Sottotitolo: 'elettroLudica',
      Brief: 'Lorem ipsum sator arepo tenet opera rotas',
      Testo: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
      Testo2: 'Lorem ipsum sator arepo tenet opera rotas',
      Autore: 'Associazione ElettroLudica',
      Image: 'article-5.jpg',
      Image2: '',
      Categoria: 'Gallery'
    },
    {
      Sheet: 'gallery',
      id: '6',
      Id: '6',
      Data: '10/06/2021',
      Sopratitolo: "Museo dell'Intrattenimento Elettronico",
      Titolo: 'Gallery 6 amiga rulez',
      Sottotitolo: 'elettroLudica',
      Brief: 'Lorem ipsum sator arepo tenet opera rotas',
      Testo: 'Lorem ipsum sator arepo tenet opera rotas',
      Testo2: '',
      Autore: 'Associazione ElettroLudica',
      Image: 'article-6.jpg',
      Image2: '',
      Categoria: 'Gallery'
    }
  ],
  pubs: [
    {
      Sheet: 'pubs',
      id: '1',
      Id: '1',
      Data: '10/06/2021',
      Sopratitolo: "Museo dell'Intrattenimento Elettronico",
      Titolo: 'Pubblicazione 1 lorem ipsum quae requiesciant',
      Sottotitolo: 'elettroLudica',
      Brief: 'Lorem ipsum sator arepo tenet opera rotas',
      Testo: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
      Testo2: '',
      Autore: 'Associazione ElettroLudica',
      Image: 'post-1-img.jpg',
      Image2: '',
      Categoria: 'Pubs',
      Tags: 'Apertura;Beginning'
    },
    {
      Sheet: 'pubs',
      id: '2',
      Id: '2',
      Data: '10/06/2021',
      Sopratitolo: "Museo dell'Intrattenimento Elettronico",
      Titolo: 'Pubblicazione 2 in nomine patriae',
      Sottotitolo: 'elettroLudica',
      Brief: 'Lorem ipsum sator arepo tenet opera rotas',
      Testo: 'Lorem ipsum sator arepo tenet opera rotas',
      Testo2: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
      Autore: 'Associazione ElettroLudica',
      Image: 'post-2-img.jpg',
      Image2: '',
      Categoria: 'Pubs',
      Tags: 'Apertura;Free'
    },
    {
      Sheet: 'pubs',
      id: '3',
      Id: '3',
      Data: '10/06/2021',
      Sopratitolo: "Museo dell'Intrattenimento Elettronico",
      Titolo: 'Pubblicazione 3 astra zenzero',
      Sottotitolo: 'elettroLudica',
      Brief: 'Lorem ipsum sator arepo tenet opera rotas',
      Testo: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
      Testo2: '',
      Autore: 'Associazione ElettroLudica',
      Image: 'post-3-img.jpg',
      Image2: '',
      Categoria: 'Pubs',
      Tags: 'Apertura;Beginning'
    },
    {
      Sheet: 'pubs',
      id: '4',
      Id: '4',
      Data: '10/06/2021',
      Sopratitolo: "Museo dell'Intrattenimento Elettronico",
      Titolo: 'Pubblicazione 4 pfufi puff',
      Sottotitolo: 'elettroLudica',
      Brief: 'Lorem ipsum sator arepo tenet opera rotas',
      Testo: 'Lorem ipsum sator arepo tenet opera rotas',
      Testo2: 'Lorem ipsum sator arepo tenet opera rotas',
      Autore: 'Associazione ElettroLudica',
      Image: 'post-4-img.jpg',
      Image2: '',
      Categoria: 'Pubs',
      Tags: 'Apertura;Free'
    },
    {
      Sheet: 'pubs',
      id: '5',
      Id: '5',
      Data: '10/06/2021',
      Sopratitolo: "Museo dell'Intrattenimento Elettronico",
      Titolo: 'Pubblicazione 5 jonson and scarlett',
      Sottotitolo: 'elettroLudica',
      Brief: 'Lorem ipsum sator arepo tenet opera rotas',
      Testo: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
      Testo2: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
      Autore: 'Associazione ElettroLudica',
      Image: 'post-video.mp4',
      Image2: '',
      Categoria: 'Pubs',
      Tags: 'Apertura;Beginning'
    },
    {
      Sheet: 'pubs',
      id: '6',
      Id: '6',
      Data: '10/06/2021',
      Sopratitolo: "Museo dell'Intrattenimento Elettronico",
      Titolo: 'Pubblicazione 6 titirae tu rebuba patula spatula stucchi',
      Sottotitolo: 'elettroLudica',
      Brief: 'Lorem ipsum sator arepo tenet opera rotas',
      Testo: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
      Testo2: '',
      Autore: 'Associazione ElettroLudica',
      Image: 'post-5-img.jpg',
      Image2: '',
      Categoria: 'Pubs',
      Tags: 'Apertura;Free'
    }
  ],
  jumbo: [
    {
      Sheet: 'jumbo',
      id: '1',
      Id: '1',
      img: 'travel-1.jpg',
      overtitle: 'Via Sandro Pertini, 105 - Avezzano (AQ)',
      title: 'Benvenuto!',
      description: "Museo dell'Intrattenimento Elettronico",
      link1_href: '',
      link1_title: '',
      link2_href: '/catalog',
      link2_title: 'Vai al catalogo'
    },
    {
      Sheet: 'jumbo',
      id: '2',
      Id: '2',
      img: 'travel-2.jpg',
      overtitle: 'Via Sandro Pertini, 105 - Avezzano (AQ)',
      title: 'Computer',
      description: 'Piu di 110 home e personal computer!',
      link1_href: '',
      link1_title: '',
      link2_href: '/catalog',
      link2_title: 'Vai al catalogo'
    },
    {
      Sheet: 'jumbo',
      id: '3',
      Id: '3',
      img: 'travel-3.jpg',
      overtitle: 'Via Sandro Pertini, 105 - Avezzano (AQ)',
      title: 'Console',
      description: 'Circa 200 console che hanno fatto la storia!',
      link1_href: '',
      link1_title: '',
      link2_href: '/catalog',
      link2_title: 'Vai al catalogo'
    },
    {
      Sheet: 'jumbo',
      id: '4',
      Id: '4',
      img: 'travel-6.jpg',
      overtitle: 'Via Sandro Pertini, 105 - Avezzano (AQ)',
      title: 'Giochi Elettronici',
      description: 'Più di 110 giochi elettronici da tavolo e portatili!',
      link1_href: '',
      link1_title: '',
      link2_href: '/catalog',
      link2_title: 'Vai al catalogo'
    },
    {
      Sheet: 'jumbo',
      id: '5',
      Id: '5',
      img: 'travel-4.jpg',
      overtitle: 'Via Sandro Pertini, 105 - Avezzano (AQ)',
      title: 'Flipper',
      description: 'Piu di 100 flipper di ogni epoca e per tutti i gusti!',
      link1_href: '',
      link1_title: '',
      link2_href: '/catalog',
      link2_title: 'Vai al catalogo'
    },
    {
      Sheet: 'jumbo',
      id: '6',
      Id: '6',
      img: 'travel-5.jpg',
      overtitle: 'Via Sandro Pertini, 105 - Avezzano (AQ)',
      title: 'Arcade',
      description: 'Oltre 200 cabinati restaurati e funzionanti!',
      link1_href: '',
      link1_title: '',
      link2_href: '/catalog',
      link2_title: 'Vai al catalogo'
    }
  ],
  banner: [
    {
      Sheet: 'banner',
      id: '1',
      Id: '1',
      img: 'home-banner1.jpg',
      overtitle: 'Via Sandro Pertini, 105 - Avezzano (AQ)',
      title: 'La più grande esposizione europea con centinaia di cabinati, flipper, computer e console funzionanti!',
      description: '<b>elettroLudica</b> | Avezzano | Abruzzo | Italia'
    },
    {
      Sheet: 'banner',
      id: '2',
      Id: '2',
      img: 'home-banner2.jpg',
      overtitle: 'Via Sandro Pertini, 105 - Avezzano (AQ)',
      title: 'Gioco gratuito e illimitato!',
      description: '<b>elettroLudica</b> | Avezzano | Abruzzo | Italia'
    }
  ],
  faqs: [
    {
      Sheet: 'faqs',
      id: '1',
      Id: '1',
      title: "L'ingresso al museo è gratuito?",
      content: "Si, durante l'inaugurazione del nostro Museo dell'Intrattenimento Elettronico, l'ingresso è aperto a tutti e gratuito, nel rispetto delle normative anti Covid. Dopo l'inaugurazione l'accesso sarà gratuito per i tesserati."
    },
    {
      Sheet: 'faqs',
      id: '2',
      Id: '2',
      title: 'Per giocare si deve pagare o è tutto gratis?',
      content: 'Non si deve pagare nulla, il gioco è gratuito e illimitato! Pensa solo a divertirti!'
    },
    {
      Sheet: 'faqs',
      id: '3',
      Id: '3',
      title: "Si puo' giocare a tutto quello che si vuole?",
      content: "Non tutte le macchine saranno sempre accese. Quelle più 'antiche' e storiche, in particolare, sono delicate e verranno accese in periodi particolari. Potete contattare il museo per avere ogni informazione sui giochi disponibili, che saranno - tra flipper, arcade e computer/console nelle sale prove - comunque sempre centinaia!"
    }
  ],
  credits: [
    {
      Sheet: 'credits',
      id: '1',
      Id: '1',
      nome: 'Fabio Rubeo',
      ruolo: 'Socio Fondatore',
      img: 'fabio.jpg'
    },
    {
      Sheet: 'credits',
      id: '2',
      Id: '2',
      nome: 'Erik Pede',
      ruolo: 'Socio Fondatore',
      img: 'erik.jpg'
    },
    {
      Sheet: 'credits',
      id: '3',
      Id: '3',
      nome: 'Alessandro Di Berardino',
      ruolo: 'Socio Fondatore',
      img: 'nebbia.jpg'
    },
    {
      Sheet: 'credits',
      id: '4',
      Id: '4',
      nome: 'Antonio Longo',
      ruolo: 'Tecnico',
      img: 'antonio.jpg'
    },
    {
      Sheet: 'credits',
      id: '5',
      Id: '5',
      nome: 'Mario Ciciotti',
      ruolo: 'Operations',
      img: 'mario.jpg'
    },
    {
      Sheet: 'credits',
      id: '6',
      Id: '6',
      nome: 'Francesco Serapiglia',
      ruolo: 'Organizer',
      img: 'francesco.jpg'
    },
    {
      Sheet: 'credits',
      id: '7',
      Id: '7',
      nome: 'Marco Di Gennaro',
      ruolo: 'Grafico',
      img: 'marco.jpg'
    },
    {
      Sheet: 'credits',
      id: '8',
      Id: '8',
      nome: 'Alessandro Di Michele',
      ruolo: 'Developer',
      img: 'ale.jpg'
    }
  ],
  orari: [
    {
      Sheet: 'orari',
      id: '1',
      giorno: 'Lunedi',
      data: ' ',
      orario: 'Chiuso'
    },
    {
      Sheet: 'orari',
      id: '2',
      giorno: 'Martedi',
      data: ' ',
      orario: 'Chiuso'
    },
    {
      Sheet: 'orari',
      id: '3',
      giorno: 'Mercoledi',
      data: ' ',
      orario: 'Chiuso'
    },
    {
      Sheet: 'orari',
      id: '4',
      giorno: 'Giovedi',
      data: ' ',
      orario: 'Chiuso'
    },
    {
      Sheet: 'orari',
      id: '5',
      giorno: 'Venerdi',
      data: ' ',
      orario: 'Chiuso'
    },
    {
      Sheet: 'orari',
      id: '6',
      giorno: 'Sabato',
      data: ' ',
      orario: '15:00-19:30'
    },
    {
      Sheet: 'orari',
      id: '7',
      giorno: 'Domenica',
      data: ' ',
      orario: '15:00-19:30'
    }
  ]
}