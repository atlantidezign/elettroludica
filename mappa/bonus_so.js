BonusImpl.prototype.saveSO = function () {
	var toSave = {
		lang : this.lang,
		visitedAreas : this.visitedAreas,
		visitedVetrinas : this.visitedVetrinas,
		visitedModules : this.visitedModules,
		visitedGurus : this.visitedGurus,
		visitedLeggios : this.visitedLeggios,
		visitedHiddens : this.visitedHiddens,
		playerUnlocked : this.playerUnlocked,
		playerSkin : this.playerSkin,
		playerPoints : this.playerPoints,
	};
	localStorage.setItem('elettroCookie', JSON.stringify(toSave));
};
BonusImpl.prototype.loadSO = function () {
	if (localStorage.getItem('elettroCookie')) {
		var toLoad = JSON.parse(localStorage.getItem('elettroCookie'));
		this.lang = toLoad.lang;
		this.visitedAreas = toLoad.visitedAreas;
		this.visitedVetrinas = toLoad.visitedVetrinas;
		this.visitedModules = toLoad.visitedModules;
		this.visitedGurus = toLoad.visitedGurus;
		this.visitedLeggios = toLoad.visitedLeggios;
		this.visitedHiddens = toLoad.visitedHiddens;
		this.playerUnlocked = toLoad.playerUnlocked;
		this.playerSkin = toLoad.playerSkin;
		this.playerPoints = toLoad.playerPoints;
	}
};
BonusImpl.prototype.clearSO = function () {
	this.lang = "it";
	this.visitedAreas = [];
	this.visitedVetrinas = [];
	this.visitedModules = [];
	this.visitedGurus = [];
	this.visitedLeggios = [];
	this.visitedHiddens = [];
	this.playerUnlocked = [];
	this.playerSkin = 2;
	this.playerPoints = 0;
	this.saveSO();
	this.updateBySO();
};
BonusImpl.prototype.updateBySO = function() {
	var that = this;
	for (var g = that.allGurus.length-1; g>=0; g--) {
		var guruCont = that.allGurus[g];
		if (that.visitedGurus.indexOf(guruCont.name) >=0 ) {
			guruCont.guru.gotoAndStop(guruCont.guru.baseAsset + "idle");
			guruCont.button.visible = false;
			guruCont.spoken = true;
			guruCont.alpha = 0;
			guruCont.visible = false;
			if (that.gurus.indexOf(guruCont)>=0) that.gurus.splice(that.gurus.indexOf(guruCont),1);
		} else {
			createjs.Tween.get(guruCont.guru).wait( Math.floor(Math.random()*2000) ).call(function(e) {e.target.alpha = 1; e.target.visible = true; e.target.gotoAndPlay(e.target.baseAsset + "explode")});
			guruCont.button.visible = true;
			guruCont.spoken = false;
			guruCont.alpha = 1;
			guruCont.visible = true;
			if (that.gurus.indexOf(guruCont)< 0) that.gurus.push(guruCont);
		}
	}
	for (var l = that.allLeggios.length-1; l>=0; l--) {
		var leggioCont = that.allLeggios[l];
		if (that.visitedLeggios.indexOf(leggioCont.name) >= 0) {
			leggioCont.button.visible = false;
			leggioCont.spoken = true;
			leggioCont.alpha = 0;
			leggioCont.visible = false;
			if (that.leggios.indexOf(leggioCont) >= 0) that.leggios.splice(that.leggios.indexOf(leggioCont), 1);
		} else {
			leggioCont.button.visible = true;
			leggioCont.spoken = false;
			leggioCont.alpha = 1;
			leggioCont.visible = true;
			if (that.leggios.indexOf(leggioCont) < 0) that.leggios.push(leggioCont);
		}
	}
	that.updateHud();
};
//-------------------------