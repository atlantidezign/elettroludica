function genShadow(stileXXX) {
	var generatedShadow = null;
	if (stileXXX.shadow) {
		var x = 0;
		var y = 0;
		var blur = 2;
		var color = "black";
		if (stileXXX.shadow["x"]) x = stileXXX.shadow["x"];
		if (stileXXX.shadow["y"]) y = stileXXX.shadow["y"];
		if (stileXXX.shadow["x"] && !stileXXX.shadow["y"] && stileXXX.shadow["y"] != 0) y = x;
		if (stileXXX.shadow["y"] && !stileXXX.shadow["x"] && stileXXX.shadow["x"] != 0) x = y;
		if (stileXXX.shadow["blur"]) blur = stileXXX.shadow["blur"];
		if (stileXXX.shadow["color"]) color = stileXXX.shadow["color"];
		generatedShadow = new createjs.Shadow(color, x, y, blur);
	}
	return generatedShadow;
};
//---------
function pTheorem(point1, point2) {
	return Math.floor(Math.sqrt(((point2.x - point1.x) * (point2.x - point1.x)) + ((point2.y - point1.y) * (point2.y - point1.y))));
}
function getAngle(point1, point2) {
	var deltaX, deltaY, angle;
	deltaX = point2.x - point1.x;
	deltaY = point2.y - point1.y;
	angle = Math.atan2(deltaY, deltaX);
	if (angle < 0) {
		angle += 2 * Math.PI;
	}
	return angle * 180 / Math.PI;
}