BonusImpl.prototype.virtualJoystick = null;
BonusImpl.prototype.virtualJoystickSize = 100;
BonusImpl.prototype.virtualJoystickInit = function() {
	var that = this;
	BonusImpl.prototype.virtualJoystick = new createjs.Container();
	BonusImpl.prototype.virtualJoystick.name = "VirtualJoystick";
	BonusImpl.prototype.virtualJoystick.visible = false;
	BonusImpl.prototype.virtualJoystick.mouseEnabled = false;
	BonusImpl.prototype.virtualJoystick.mouseChildren = false;
	that.stage.addChild(BonusImpl.prototype.virtualJoystick);

	var bg = new createjs.Shape();
	bg.graphics.beginFill('#60c5ff').drawCircle(0, 0, BonusImpl.prototype.virtualJoystickSize);
	bg.alpha = 0.25;
	bg.cache(-BonusImpl.prototype.virtualJoystickSize, -BonusImpl.prototype.virtualJoystickSize, BonusImpl.prototype.virtualJoystickSize*2, BonusImpl.prototype.virtualJoystickSize*2);

	var psp = new createjs.Shape();
	psp.graphics.beginFill('#333333').drawCircle(0, 0, BonusImpl.prototype.virtualJoystickSize*0.33);
	psp.alpha = 0.35;
	psp.cache(-BonusImpl.prototype.virtualJoystickSize*0.33, -BonusImpl.prototype.virtualJoystickSize*0.33, BonusImpl.prototype.virtualJoystickSize*0.66, BonusImpl.prototype.virtualJoystickSize*0.66);

	var line = new createjs.Shape();
	line.alpha = 0.35;
	line.cache(-BonusImpl.prototype.virtualJoystickSize,-BonusImpl.prototype.virtualJoystickSize,BonusImpl.prototype.virtualJoystickSize*2,BonusImpl.prototype.virtualJoystickSize*2);

	var vertical = new createjs.Shape();
	vertical.graphics.beginFill('#ff4d4d').drawRect(0, -BonusImpl.prototype.virtualJoystickSize, 2, BonusImpl.prototype.virtualJoystickSize*2);
	vertical.cache(0, -BonusImpl.prototype.virtualJoystickSize, 2, BonusImpl.prototype.virtualJoystickSize*2);
	vertical.alpha = 0.4;
	var horizontal = new createjs.Shape();
	horizontal.graphics.beginFill('#ff4d4d').drawRect(-BonusImpl.prototype.virtualJoystickSize, 0, BonusImpl.prototype.virtualJoystickSize*2, 2);
	horizontal.cache(-BonusImpl.prototype.virtualJoystickSize, 0, BonusImpl.prototype.virtualJoystickSize*2, 2);
	horizontal.alpha = 0.4;

	BonusImpl.prototype.virtualJoystick.addChild(bg);
	BonusImpl.prototype.virtualJoystick.addChild(line);
	BonusImpl.prototype.virtualJoystick.line = line;
	BonusImpl.prototype.virtualJoystick.addChild(psp);
	BonusImpl.prototype.virtualJoystick.psp = psp;

	BonusImpl.prototype.virtualJoystick.addChild(vertical);
	BonusImpl.prototype.virtualJoystick.addChild(horizontal);
};
BonusImpl.prototype.virtualJoystickShow = function(x,y) {
	var that = this;
	if (that.state == that.BonusStateEnum.READY) {
		BonusImpl.prototype.virtualJoystick.x = x;
		BonusImpl.prototype.virtualJoystick.y = y;
		BonusImpl.prototype.virtualJoystick.line.graphics.clear();
		BonusImpl.prototype.virtualJoystick.line.updateCache();
		BonusImpl.prototype.virtualJoystick.psp.x = 0;
		BonusImpl.prototype.virtualJoystick.psp.y = 0;
		BonusImpl.prototype.virtualJoystick.visible = true;
	}
};
BonusImpl.prototype.virtualJoystickHide = function() {
	BonusImpl.prototype.virtualJoystick.visible = false;
};
BonusImpl.prototype.virtualJoystickUpdate = function(_x,_y) {
	if (BonusImpl.prototype.virtualJoystick.visible) {
		var x = _x - BonusImpl.prototype.virtualJoystick.x;
		var y = _y - BonusImpl.prototype.virtualJoystick.y;
		if (x > BonusImpl.prototype.virtualJoystickSize*0.66) x = BonusImpl.prototype.virtualJoystickSize*0.66;
		if (x < -BonusImpl.prototype.virtualJoystickSize*0.66) x = -BonusImpl.prototype.virtualJoystickSize*0.66;
		if (y > BonusImpl.prototype.virtualJoystickSize*0.66) y = BonusImpl.prototype.virtualJoystickSize*0.66;
		if (y < -BonusImpl.prototype.virtualJoystickSize*0.66) y = -BonusImpl.prototype.virtualJoystickSize*0.66;
		BonusImpl.prototype.virtualJoystick.psp.x = x;
		BonusImpl.prototype.virtualJoystick.psp.y = y;
		BonusImpl.prototype.virtualJoystick.line.graphics.clear();
		BonusImpl.prototype.virtualJoystick.line.graphics.setStrokeStyle(5).beginStroke("#666666").moveTo(0,0).lineTo(x,y);
		BonusImpl.prototype.virtualJoystick.line.updateCache();
	}
};
//-------------------------