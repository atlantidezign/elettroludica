BonusImpl.prototype.currentPerIncoda = 1;
BonusImpl.prototype.currentMessaggioTitle = "";
BonusImpl.prototype.currentMessaggioText = "";
BonusImpl.prototype.currentMessaggioIcon = "";
BonusImpl.prototype.codaMessaggiTitle = [];
BonusImpl.prototype.codaMessaggiText = [];
BonusImpl.prototype.codaMessaggiIcon = [];
BonusImpl.prototype.doMessaggio = function (title, textt, icon, incoda) {
	var that = this;
	var incodaa = 1;
	if (incoda === 0) incodaa = 0;
	//console.log(title, textt, incodaa);
	if (icon != null) {
		if (icon == 1) {
			that.MESSAGE.message_ico.visible = false;
			that.MESSAGE.message_leggio_ico.visible = true;
		} else {
			that.MESSAGE.message_ico.visible = true;
			that.MESSAGE.message_leggio_ico.visible = false;
		}
		that.MESSAGE.message_ico_bg.visible = true;
	} else {
		that.MESSAGE.message_leggio_ico.visible = false;
		that.MESSAGE.message_ico_bg.visible = false;
		that.MESSAGE.message_ico.visible = false;
	}
	that.MESSAGE.message_titolo.text = title;
	that.MESSAGE.message_testo.text = textt;
	if (!that.MESSAGE.visible) {
		that.MESSAGE.alpha = 0;
	} else {
		//mette in coda
		if (that.currentMessaggioTitle != "" && that.currentPerIncoda > 0) {
			that.codaMessaggiTitle.push(that.currentMessaggioTitle);
			that.codaMessaggiText.push(that.currentMessaggioText);
			that.codaMessaggiIcon.push(that.currentMessaggioIcon);
		}
	}
	that.MESSAGE.visible = true;
	createjs.Tween.get(that.MESSAGE).to({alpha:1}, 250)
	that.currentMessaggioTitle = title;
	that.currentMessaggioText = textt;
	that.currentMessaggioIcon = icon;
	that.currentPerIncoda = incodaa;
};
BonusImpl.prototype.closeMessaggio = function() {
	var that = this;
	if (this.codaMessaggiTitle.length > 0) {
		var icon = that.codaMessaggiIcon[that.codaMessaggiIcon.length-1];
		var title = that.codaMessaggiTitle[that.codaMessaggiTitle.length-1];
		var textt = that.codaMessaggiText[that.codaMessaggiText.length-1];
		that.codaMessaggiTitle.pop();
		that.codaMessaggiText.pop();
		that.codaMessaggiIcon.pop();
		if (icon != null) {
			if (icon == 1) {
				that.MESSAGE.message_ico.visible = false;
				that.MESSAGE.message_leggio_ico.visible = true;
			} else {
				that.MESSAGE.message_ico.visible = true;
				that.MESSAGE.message_leggio_ico.visible = false;
			}
			that.MESSAGE.message_ico_bg.visible = true;
		} else {
			that.MESSAGE.message_leggio_ico.visible = false;
			that.MESSAGE.message_ico_bg.visible = false;
			that.MESSAGE.message_ico.visible = false;
			if (that.state == that.BonusStateEnum.SEQUENCE ) that.state = that.BonusStateEnum.READY;
		}
		that.MESSAGE.message_titolo.text = title;
		that.MESSAGE.message_testo.text = textt;
	} else {
		that.currentMessaggioTitle = "";
		that.currentMessaggioText = "";
		that.currentMessaggioIcon = "";
		that.currentPerIncoda = 0;
		if (that.state == that.BonusStateEnum.SEQUENCE ) that.state = that.BonusStateEnum.READY;
		createjs.Tween.get(that.MESSAGE).to({alpha:0, visible:false}, 250);
	}
};
//---------