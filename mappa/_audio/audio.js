//v2.0
var audioSprite32bit = [
{
  "src": "audio.ogg",
  "data": {
	"audioSprite": [
      {
        "id": "BonusGame_1_choose",
        "startTime": 0,
        "duration": 835.9183673469388
      },
      {
        "id": "BonusGame_1_click",
        "startTime": 2000,
        "duration": 940.4081632653058
      },
      {
        "id": "BonusGame_1_end",
        "startTime": 4000,
        "duration": 2612.244897959183
      },
      {
        "id": "BonusGame_1_exit",
        "startTime": 8000,
        "duration": 3474.2857142857133
      },
      {
        "id": "BonusGame_1_loop",
        "startTime": 13000,
        "duration": 16039.183673469388
      },
      {
        "id": "BonusGame_1_lose",
        "startTime": 31000,
        "duration": 2168.16326530612
      },
      {
        "id": "BonusGame_1_start",
        "startTime": 35000,
        "duration": 4728.163265306122
      },
      {
        "id": "BonusGame_1_win",
        "startTime": 41000,
        "duration": 1828.5714285714291
      }
    ]
 }
}
];
