BonusImpl.prototype.tempoFadePanel = 1000;
BonusImpl.prototype.tempoPostFadePanel = 10000;

//-------------------------
BonusImpl.prototype.assets = [];
BonusImpl.prototype.stage = null;
BonusImpl.prototype.fonts = [];
BonusImpl.prototype.spriteSheets = [];
BonusImpl.prototype.BonusStateEnum = {
	READY: 0,
	ANIMATING: 1,
	CONCLUDED: 2,
	INTRO: 3,
	SEQUENCE: 4
};
BonusImpl.prototype.state = BonusImpl.prototype.BonusStateEnum.INTRO;
BonusImpl.prototype.clickEnabled = false;
BonusImpl.prototype.localLaunch = false;
BonusImpl.prototype.relocables = [];
BonusImpl.prototype.isRotatedPrev = false;
BonusImpl.prototype.doOutroMessage = false;
//---
BonusImpl.prototype.ontick = null;
BonusImpl.prototype.usecjticker = true;

//-------------------------
BonusImpl.prototype.getDataCallback1 = function (dataFromEngine) {
	this.mapAdditionalData = dataFromEngine;
	this.callInitApi("elettroMappa", this.getDataCallback2.bind(this));
};
BonusImpl.prototype.getDataCallback2 = function (dataFromEngine) {
	this.mapdata = dataFromEngine;
	this.layerZonesIdsSubtract = this.mapdata.tilesets[1].firstgid -1;
	this.loadSO();
	//------------------
	console.log(this.mapAdditionalData["CONFIG"]["app_name"] + " " + this.mapAdditionalData["CONFIG"]["app_version"]);
	//------------------
	this.initSceneAndSprite();
	//------------------
	this.startGame();
};
BonusImpl.prototype.preInit = function () { //PRELOAD ASSETS
	this.localLaunch = true;
	var manifest = [ { "src": "bonusImages.json", "id": "sheet1", "type": "spritesheet" } ];
	if (!window.modelApp.isDesktop) {
		for (var m = 0; m < manifest.length; m++) {
			manifest[m].src = manifest[m].src.replace('.json', '_mobile.json');
		}
	}
	var loader = new createjs.LoadQueue(true, "./");
	loader.on("fileload", this.handleFileLoad.bind(this));
	loader.on("complete", this.handleComplete.bind(this));
	loader.loadManifest(manifest);
};
BonusImpl.prototype.handleFileLoad = function (event) {
	this.assets.push(event);
};
BonusImpl.prototype.handleComplete = function () {
	for (var i = 0; i < this.assets.length; i++) {
		var event = this.assets[i];
		var result = event.result;
		switch (event.item.id) {
			case 'sheet1':
				this.spriteSheets[0] = result;
				break;
			case 'sheet2':
				this.spriteSheets[1] = result;
				break;
		}
	}
	this.loadFonts();
};
BonusImpl.prototype.loadFonts = function () {
	var that = this;
	var fontA = new FontFaceObserver('Minimal5x7');
	var fontB = new FontFaceObserver('Minimal5x5Monospaced');
	var fontC = new FontFaceObserver('Minimal3x5');
	Promise.all([fontA.load(), fontB.load(), fontC.load()]).then(function () {
	  that.handleFontLoadComplete();
	});
};
BonusImpl.prototype.handleFontLoadComplete = function () {
	this.loadAudio();
};
BonusImpl.prototype.loadAudio = function() {
	var that = this;
	var assetsFolder = '_audio/';
	var assetDescriptor = audioSprite32bit;
	//---
	try {
		createjs.Sound.initializeDefaultPlugins();
		createjs.Sound.alternateExtensions = ["ogg", "mp3", "m4a"];
		createjs.Sound.on("progress", function (evt) {
		});
		createjs.Sound.on("fileload", function () {
			//END
			that.init();
		});
		function preloadError() {
			console.log('Audio Preload Error 2.');
			that.init();
		}
		function fileError() {
			console.log('Audio Preload Error 3.');
			that.init();
		}
		createjs.Sound.on("error", preloadError);
		createjs.Sound.on("fileerror", fileError);
		var soundsRegistrationResults = createjs.Sound.registerSounds(assetDescriptor, assetsFolder);
		var soundOk = true;
		if (soundOk && !soundsRegistrationResults)
			soundOk = false;
		if (soundOk && soundsRegistrationResults.length == 0)
			soundOk = false;
		if (soundOk && !soundsRegistrationResults[0])
			soundOk = false;
		//console.log("SOUND OK: ", soundOk);
	} catch (e) {
		console.log('Audio Preload Error 1.');
		that.init();
	}
};
BonusImpl.prototype.init = function () {
	this.callInitApi("elettroConfig", this.getDataCallback1.bind(this));
}

//-------------------------
BonusImpl.prototype.horizontalCentralAlignText = function (textEl, centralX) {
	var elBounds = textEl.getBounds();
	if (elBounds != null) {
		textEl.x = centralX - (Math.floor(elBounds.width / 2.0));
	}
};
BonusImpl.prototype.verticalCentralAlignText = function (textEl, centralY) {
	var elBounds = textEl.getBounds();
	if (elBounds != null) {
		textEl.y = centralY - (Math.floor(elBounds.height / 2.0)) + 30;
	}
};

//-------------------------
BonusImpl.prototype.initCalled = 0;
BonusImpl.prototype.callInitApi = function (filename, callback, errorCallback, selPos) {
	var useUrl = "_json/"+filename+".json";
	if (this.initCalled<2) {
		this.initCalled++;
		var callbackToBonus = callback;
		var errorToBonus = errorCallback;
		$.ajax({
			type: "GET",
			url: useUrl,
			contentType: 'application/json',
			dataType: 'json',
			success: function (data) {
				if (data) {
					if (callbackToBonus) callbackToBonus(data);
				} else {
					if (errorToBonus) errorToBonus("Error starting the bonus!");
				}
			},
			error: function (ex) {
				console.log("CALL INIT ERROR", ex);
				if (errorToBonus) errorToBonus(ex);
			}
		});
	}
};

//-------------------------
BonusImpl.prototype.makeRelocable = function () {
	var that = this;
	//** RIMETTERE? if (!window.modelApp.isDesktop) {
		window.addEventListener('resize', relocateObjects); 
		relocateObjects();
	//** RIMETTERE ?}
	function relocateObjects() {
		var iw = window.innerWidth, ih = window.innerHeight;
		var isRotated = false;
		if (iw < ih) isRotated = true;
		if (that.relocables.length > 0) {
			var reloMax = that.relocables.length;
			if (that.isRotatedPrev != isRotated) {
				var whatGet = "h";
				var oldGet = "v";
				if (isRotated) {
					whatGet = "v";
					oldGet = "h";
				}
				for (var r = 0; r < reloMax; r++) {
					var rspec = that.relocables[r];
					var robj = getItem(rspec["id"]);
					if (rspec[whatGet]["x"] != null) robj.x = rspec[whatGet]["x"];
					if (rspec[whatGet]["y"] != null) robj.y = rspec[whatGet]["y"];
					if (rspec[whatGet]["s"]) {
						if (!window.modelApp.isDesktop && window.config.mobileAssetScale && (rspec.kind == "Sprite")) robj.scaleX = robj.scaleY = (rspec[whatGet]["s"] * window.config.mobileAssetScale)
							else robj.scaleX = robj.scaleY = rspec[whatGet]["s"];
					}
					if (rspec[oldGet]["v"]!= null) {
						var isNowVisible = robj.visible;
						var wasToBeVisible = rspec[oldGet]["v"];
						if (isNowVisible == wasToBeVisible) robj.visible = rspec[whatGet]["v"];
					}
					if (rspec.kind == "Text") {
						var oldTxt = robj.text;
						if (rspec[whatGet]["x"]) robj.cX = rspec[whatGet]["x"];
						if (rspec[whatGet]["x"]) robj.dX = rspec[whatGet]["x"];
						if (robj.textAlign == "center")	{
							if (robj.iniText) robj.text = robj.iniText;
							that.horizontalCentralAlignText(robj, robj.cX);
							robj.text = oldTxt;
						}
					}
				}
			}
		}
		that.isRotatedPrev = isRotated;
		if (that.messageContainer) {
			if (isRotated) {
				that.messageContainer.x = -(280);
				that.messageContainer.y = +(280);
			} else {
				that.messageContainer.x = 0;
				that.messageContainer.y = 0;
			}
		}
		if (!that.localLaunch) {
			if (isRotated) {
				that.stage.x = -360;
				that.stage.y = -640;
			} else {
				that.stage.x = -640;
				that.stage.y = -360;
			}
		}

		function getItem(idStr) {
			function fetchFromObject(obj, prop) {
				if (typeof obj === 'undefined') {
					return false;
				}
				var _index = prop.indexOf('.')
				if (_index > -1) {
					return fetchFromObject(obj[prop.substring(0, _index)], prop.substr(_index + 1));
				}
				return obj[prop];
			}

			var item = fetchFromObject(that, idStr.replace('this.', ''));
			return item;
		}
	}
}
//-------------------------