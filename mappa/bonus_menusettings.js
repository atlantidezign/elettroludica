BonusImpl.prototype.settingsOpened = false;
BonusImpl.prototype.setupMenuSettings = function () {
	var that = this;
	that.SETTINGS = new createjs.Container();
	that.SETTINGS.name = "SETTINGS";
	that.SETTINGS.x = 710;
	that.SETTINGS.y = 1270;
	that.SETTINGS.that = that;
	that.stage.addChild(that.SETTINGS);

	var button_settings = new createjs.Sprite(that.spriteSheets[0], "button_settings");
	if (!window.modelApp.isDesktop && window.config.mobileAssetScale) button_settings.scaleX = button_settings.scaleY = window.config.mobileAssetScale;
	button_settings.gotoAndStop("button_settings");
	button_settings.name = "button_settings";
	button_settings.x = 480;
	button_settings.y = -725;
	button_settings.that = that;
	//button_settings.width = 81;
	//button_settings.height = 82;
	button_settings.cursor = "pointer";
	that.SETTINGS.addChild(button_settings);
	that.SETTINGS.button_settings = button_settings;

	var button_settings_active = new createjs.Sprite(that.spriteSheets[0], "button_settings_active");
	if (!window.modelApp.isDesktop && window.config.mobileAssetScale) button_settings_active.scaleX = button_settings_active.scaleY = window.config.mobileAssetScale;
	button_settings_active.gotoAndStop("button_settings_active");
	button_settings_active.name = "button_settings_active";
	button_settings_active.x = 480;
	button_settings_active.y = -725;
	button_settings_active.that = that;
	button_settings_active.visible = false;
	//button_settings_active.width = 81;
	//button_settings_active.height = 82;
	button_settings_active.cursor = "pointer";
	that.SETTINGS.addChild(button_settings_active);
	that.SETTINGS.button_settings_active = button_settings_active;

	var button_audio_off = new createjs.Sprite(that.spriteSheets[0], "button_audio_off");
	if (!window.modelApp.isDesktop && window.config.mobileAssetScale) button_audio_off.scaleX = button_audio_off.scaleY = window.config.mobileAssetScale;
	button_audio_off.gotoAndStop("button_audio_off");
	button_audio_off.name = "button_audio_off";
	button_audio_off.x = 480;
	button_audio_off.y = -813;
	button_audio_off.that = that;
	button_audio_off.visible = false;
	//button_audio_off.width = 81;
	//button_audio_off.height = 82;
	button_audio_off.cursor = "pointer";
	that.SETTINGS.addChild(button_audio_off);
	that.SETTINGS.button_audio_off = button_audio_off;

	var button_audio_on = new createjs.Sprite(that.spriteSheets[0], "button_audio_on");
	if (!window.modelApp.isDesktop && window.config.mobileAssetScale) button_audio_on.scaleX = button_audio_on.scaleY = window.config.mobileAssetScale;
	button_audio_on.gotoAndStop("button_audio_on");
	button_audio_on.name = "button_audio_on";
	button_audio_on.x = 480;
	button_audio_on.y = -813;
	button_audio_on.that = that;
	button_audio_on.visible = false;
	//button_audio_on.width = 81;
	//button_audio_on.height = 82;
	button_audio_on.cursor = "pointer";
	that.SETTINGS.addChild(button_audio_on);
	that.SETTINGS.button_audio_on = button_audio_on;

	var button_lang_en = new createjs.Sprite(that.spriteSheets[0], "button_lang_en");
	if (!window.modelApp.isDesktop && window.config.mobileAssetScale) button_lang_en.scaleX = button_lang_en.scaleY = window.config.mobileAssetScale;
	button_lang_en.gotoAndStop("button_lang_en");
	button_lang_en.name = "button_lang_en";
	button_lang_en.x = 480;
	button_lang_en.y = -900;
	button_lang_en.that = that;
	button_lang_en.visible = false;
	//button_lang_en.width = 81;
	//button_lang_en.height = 81;
	button_lang_en.cursor = "pointer";
	that.SETTINGS.addChild(button_lang_en);
	that.SETTINGS.button_lang_en = button_lang_en;

	var button_lang_it = new createjs.Sprite(that.spriteSheets[0], "button_lang_it");
	if (!window.modelApp.isDesktop && window.config.mobileAssetScale) button_lang_it.scaleX = button_lang_it.scaleY = window.config.mobileAssetScale;
	button_lang_it.gotoAndStop("button_lang_it");
	button_lang_it.name = "button_lang_it";
	button_lang_it.x = 480;
	button_lang_it.y = -900;
	button_lang_it.that = that;
	button_lang_it.visible = false;
	//button_lang_it.width = 81;
	//button_lang_it.height = 81;
	button_lang_it.cursor = "pointer";
	that.SETTINGS.addChild(button_lang_it);
	that.SETTINGS.button_lang_it = button_lang_it;

	var button_help = new createjs.Sprite(that.spriteSheets[0], "button_help");
	if (!window.modelApp.isDesktop && window.config.mobileAssetScale) button_help.scaleX = button_help.scaleY = window.config.mobileAssetScale;
	button_help.gotoAndStop("button_help");
	button_help.name = "button_help";
	button_help.x = 480;
	button_help.y = -988;
	button_help.that = that;
	button_help.visible = false;
	//button_help.width = 81;
	//button_help.height = 82;
	button_help.cursor = "pointer";
	that.SETTINGS.addChild(button_help);
	that.SETTINGS.button_help = button_help;

	var button_delete = new createjs.Sprite(that.spriteSheets[0], "button_delete");
	if (!window.modelApp.isDesktop && window.config.mobileAssetScale) button_delete.scaleX = button_delete.scaleY = window.config.mobileAssetScale;
	button_delete.gotoAndStop("button_delete");
	button_delete.name = "button_delete";
	button_delete.x = 480;
	button_delete.y = -1076;
	button_delete.that = that;
	button_delete.visible = false;
	//button_delete.width = 81;
	//button_delete.height = 82;
	button_delete.cursor = "pointer";
	that.SETTINGS.addChild(button_delete);
	that.SETTINGS.button_delete = button_delete;
};
BonusImpl.prototype.addEventsMenuSettings = function () {
	var that = this;
	that.SETTINGS.button_settings.addEventListener("click", that.clickFunctionSettings);
	that.SETTINGS.button_settings_active.addEventListener("click", that.clickFunctionSettings);
	that.SETTINGS.button_audio_off.addEventListener("click", that.clickFunctionSettings);
	that.SETTINGS.button_audio_on.addEventListener("click", that.clickFunctionSettings);
	that.SETTINGS.button_lang_en.addEventListener("click", that.clickFunctionSettings);
	that.SETTINGS.button_lang_it.addEventListener("click", that.clickFunctionSettings);
	that.SETTINGS.button_help.addEventListener("click", that.clickFunctionSettings);
	that.SETTINGS.button_delete.addEventListener("click", that.clickFunctionSettings);
};
BonusImpl.prototype.clickFunctionSettings = function (evt) {
	var itemClicked = evt.currentTarget;
	var that = itemClicked.that;
	if (that.state == BonusImpl.prototype.BonusStateEnum.READY) if (that.audioInplay) that.bgmLoop = window.timeliner.fx.playBonusExternalSound("click", that.audioGameId);
	switch (itemClicked.name) {
		case "button_settings":
			if (!that.state == BonusImpl.prototype.BonusStateEnum.READY) if (that.audioInplay) that.bgmLoop = window.timeliner.fx.playBonusExternalSound("click", that.audioGameId);
			that.settingsOpened=true;
			that.SETTINGS.button_settings.visible = false;
			that.SETTINGS.button_settings_active.visible = true;
			that.SETTINGS.button_help.visible = true;
			that.SETTINGS.button_delete.visible = true;
			if (that.lang == "en") that.SETTINGS.button_lang_en.visible = true
			else that.SETTINGS.button_lang_it.visible = true;
			if (!that.audioInPlay) that.SETTINGS.button_audio_off.visible = true
			else that.SETTINGS.button_audio_on.visible = true;
			break;
		case "button_settings_active":
			if (!that.state == BonusImpl.prototype.BonusStateEnum.READY) if (that.audioInplay) that.bgmLoop = window.timeliner.fx.playBonusExternalSound("click", that.audioGameId);
			that.settingsOpened=false;
			that.SETTINGS.button_settings.visible = true;
			that.SETTINGS.button_settings_active.visible = false;
			that.SETTINGS.button_help.visible = false;
			that.SETTINGS.button_delete.visible = false;
			that.SETTINGS.button_audio_off.visible = false;
			that.SETTINGS.button_audio_on.visible = false;
			that.SETTINGS.button_lang_en.visible = false;
			that.SETTINGS.button_lang_it.visible = false;
			break;
		case "button_audio_off":
			if (!that.state == BonusImpl.prototype.BonusStateEnum.READY) if (that.audioInplay) that.bgmLoop = window.timeliner.fx.playBonusExternalSound("click", that.audioGameId);
			if (that.bgmLoop) {
				that.bgmLoop.play()
			} else {
				that.bgmLoop = window.timeliner.fx.playBonusExternalSound("loop", that.audioGameId);
			}
			if (that.bgmLoop) that.audioInPlay = true;
			that.SETTINGS.button_audio_off.visible = false;
			that.SETTINGS.button_audio_on.visible = true;
			break;
		case "button_audio_on":
			if (!that.state == BonusImpl.prototype.BonusStateEnum.READY) if (that.audioInplay) that.bgmLoop = window.timeliner.fx.playBonusExternalSound("click", that.audioGameId);
			if (that.bgmLoop) {
				that.bgmLoop.stop()
			}
			that.audioInPlay = false;
			that.SETTINGS.button_audio_on.visible = false;
			that.SETTINGS.button_audio_off.visible = true;
			break;
		case "button_lang_it":
			if (!that.state == BonusImpl.prototype.BonusStateEnum.READY) if (that.audioInplay) that.bgmLoop = window.timeliner.fx.playBonusExternalSound("click", that.audioGameId);
			that.lang = "en";
			that.SETTINGS.button_lang_it.visible = false;
			that.SETTINGS.button_lang_en.visible = true;
			that.localizeLabels();
			that.updateHud();
			that.closeMessaggio();
			that.idiotClicks = 0;
			break;
		case "button_lang_en":
			if (!that.state == BonusImpl.prototype.BonusStateEnum.READY) if (that.audioInplay) that.bgmLoop = window.timeliner.fx.playBonusExternalSound("click", that.audioGameId);
			that.lang = "it";
			that.SETTINGS.button_lang_en.visible = false;
			that.SETTINGS.button_lang_it.visible = true;
			that.localizeLabels();
			that.updateHud();
			that.closeMessaggio();
			that.idiotClicks = 0;
			break;
		case "button_help":
			if (!that.state == BonusImpl.prototype.BonusStateEnum.READY) if (that.audioInplay) that.bgmLoop = window.timeliner.fx.playBonusExternalSound("click", that.audioGameId);
			var titoli = that.mapAdditionalData["TEXTS"][that.lang]["HELPTITOLO"];
			var testi = that.mapAdditionalData["TEXTS"][that.lang]["HELPTESTO"];
			for (var t=titoli.length-1; t>-1; t--) {
				that.doMessaggio(titoli[t], testi[t]);
			}
			break;
		case "button_delete":
			if (confirm( that.mapAdditionalData["TEXTS"][that.lang]["CONFIRM_DELETE_SETTINGS"] )) {
				that.clearSO();
			}
			break;
	}
};
//-------------------------