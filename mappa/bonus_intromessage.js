BonusImpl.prototype.setupIntroMessageContainer = function () {
	var that = this;
	var settingsHelpPanel = { "introLineHeight": "22", "introFont": "bold 34px 'Minimal5x7'", "introColor": "#ffffff", "introShadow": { "color": "#000000", "offsetX": "0", "offsetY": "3", "blur": "5" }, "outroLineHeight": "22", "outroFont": "bold 34px 'Minimal5x7'", "outroColor": "#ffffff", "outroShadow": { "color": "#000000", "offsetX": "0", "offsetY": "3", "blur": "5" }, "timeout": 10000, "fade": 500, "bg": "messageBg" };
	//-------------------------------------------------------------------------------------------------------------------------------------------
	//MESSAGE CONTAINER
	var introMsg = "ELETTRO\nLUDICA";
	var outroMsg = "";
	if (settingsHelpPanel.fade) this.tempoFadePanel = settingsHelpPanel.fade;
	if (settingsHelpPanel.timeout) this.tempoPostFadePanel = settingsHelpPanel.timeout;
	introMsg = that.mapAdditionalData["TEXTS"][that.lang]["WELCOME"];
	outroMsg = that.mapAdditionalData["TEXTS"][that.lang]["GOODBYE"];
	this.messageContainer = new createjs.Container();
	if (settingsHelpPanel.bg === null) {
		//non lo fa
	} else if (settingsHelpPanel.bg !== "") {
		var sprite = new createjs.Sprite(this.spriteSheets[0], settingsHelpPanel.bg);
		var b = sprite.getBounds();
		var bX = b.width/2;
		var bY = b.height/2;
		if (!window.modelApp.isDesktop && window.config.mobileAssetScale) {
			sprite.scaleX = sprite.scaleY = window.config.mobileAssetScale;
			bX*=window.config.mobileAssetScale;
			bY*=window.config.mobileAssetScale;
		}
		sprite.x = 640 - (bX);
		sprite.y = 360 - (bY);
		this.messageContainer.addChild(sprite);
	} else {
		var shape = new createjs.Shape();
		shape.graphics.beginFill("rgba(0,0,0,0.8)").drawRect(0, 0, 640, 320);
		shape.cache(0, 0, 640, 320);
		shape.x = 320;
		shape.y = 180;
		this.messageContainer.addChild(shape);
	}
	this.introText = new createjs.Text(introMsg, settingsHelpPanel.introFont, settingsHelpPanel.introColor);
	this.introText.textBaseline = "alphabetic";
	this.introText.textAlign = "center";
	this.introText.shadow = new createjs.Shadow(settingsHelpPanel.introShadow.color, settingsHelpPanel.introShadow.offsetX, settingsHelpPanel.introShadow.offsetY, settingsHelpPanel.introShadow.blur);
	this.messageContainer.addChild(this.introText);
	this.introText.x = 640;
	this.introText.lineHeight = settingsHelpPanel.introLineHeight;
	this.verticalCentralAlignText(this.introText, 360);
	//box outro
	this.outroText = new createjs.Text(outroMsg, settingsHelpPanel.outroFont, settingsHelpPanel.outroColor);
	this.outroText.textBaseline = "alphabetic";
	this.outroText.shadow = new createjs.Shadow(settingsHelpPanel.outroShadow.color, settingsHelpPanel.outroShadow.offsetX, settingsHelpPanel.outroShadow.offsetY, settingsHelpPanel.outroShadow.blur);
	this.outroText.visible = false;
	this.outroText.textAlign = "center";
	this.outroText.x = 640;
	this.introText.lineHeight = settingsHelpPanel.outroLineHeight;
	this.messageContainer.addChild(this.outroText);
	this.verticalCentralAlignText(this.outroText, 360);
	this.messageContainer.x = 0;
	this.messageContainer.y = 0;
	if (introMsg.length == 0) {
		this.messageContainer.visible = false;
	}
	this.introMsg = introMsg;
	this.stage.addChild(this.messageContainer);
	//------------------
};
BonusImpl.prototype.closeMessageContainer = function(e) {
	var messCont = e.currentTarget;
	var that = messCont.that;
	if (messCont.visible) {
		createjs.Tween.get(messCont, {override:true}).to({alpha: 0, visible: false}, 1000).call(function () {
			that.introText.visible = false;
			messCont.visible = false;
			that.state = that.BonusStateEnum.READY;
		});
	}
	if (!that.game_logo.visible) {
		that.game_logo.alpha = 0;
		that.game_logo.visible = true;
	}
	createjs.Tween.get(that.game_logo).to({alpha: 1}, 1000)
	if (that.audioInplay) that.bgmLoop = window.timeliner.fx.playBonusExternalSound("start", that.audioGameId);
};
//-------------------------