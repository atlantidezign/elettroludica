// ELETTROLUDICA MUSEUM
// MAP+GAME 
// v0.9c
// (C)2020 ALESSANDRO DI MICHELE
// www.atlantide-design.it
// -------------------------
// thanks to opengameart.org, digennaro.it
 
var BonusImpl = function () {
	//OPTIONS
	var collisionDoorId = 2; //non implementata
	var forceOverIds = [4,6,27,28,31,34,36,37,51]; //configurare blocchi forzati in over
	
	var charactersScale = 1; //scalatura assets omini
	
	var centroAddX = -96; //posizione inizizle player
	var centroAddY = 830; //posizione inizizle player
	
	var lightTilesActive = false; //non implementata
	var enemyActive = true; //true - se mettere nemici
	var guruActive = true; //true - se mettere guru
	var leggioActive = true;  //true - se mettere leggii
	var toponomasticaActive = true;  //true - se mettere toponomastica
	var hiddenActive = false; //** hiddens, non implementata, fare?
	var lockedActive = false; //** lockeds + inventario, non implementata, fare?

	var virtualJoystickActive = true; //true - se far vedere virtual joystick
	
	var searchActive = false; //** ricerca non implementata
	var shareActive = false; //** share non implementata
	var exitActive = false; //** exit non implementata
	
	var faiAvvisiPrimaVolta = false; //false - se fare avvisi al primo ingresso in una nuova area
	var tempoHighlightPrimaVolta = 2000; //tempi avvisi di cui sopra
	var tempoHighlightSuccessivo = 500; //tempi avvisi di cui sopra

	var preloadCloseTime = 4000; //tempo chiusura preload
	//---
	var useGamepadsInput = true; //if use gamepad, if detected
	var gamepadMapping = {
		"buttons":{
			"A": 0, //X
			"B": 1, //O
			"X": 2, //Q
			"Y": 3, //T

			"LB": 4,
			"RB": 5,
			"LT": 6, //analog
			"RT": 7, //analog

			"VIEW": 8, //SELECT
			"MENU": 9, //START

			"LJB": 10,
			"RJB": 11,

			"UP": 12,
			"DOWN": 13,
			"LEFT": 14,
			"RIGHT": 15,
		},
		"axes":{
			"LJX": 0,
			"LJY": 1,
			"RJX": 2,
			"RJY": 2
		},
		"dir": {
			"LEFT": -1,
			"RIGHT": 1,
			"UP": -1,
			"DOWN": 1
		},
		"thresholds": {
			"axes": 0.5, //for axes 0.75
			"buttons": 0.5 //for analog buttons
		}
	};
	//----------------------
	//--- MC
	this.logo; //Sprite
	this.testo_label; //Text
	this.testo_value; //Text
	
	this.toponomastica; //Container
	//this.toponomastica.*KEY*; //Text
	
	this.miniMap; //Container
	this.miniMap_bg;  //Sprite
	this.miniMap_player;  //Sprite
	
	this.TOOLBAR; //Container
	//this.TOOLBAR.toolbar_bg; //Sprite
	//this.TOOLBAR.toolbar_barra__1; //Sprite
	//this.TOOLBAR.toolbar_barra__2; //Sprite
	//this.TOOLBAR.toolbar_barra__3; //Sprite
	//this.TOOLBAR.toolbar_barra__4; //Sprite
	//this.TOOLBAR.toolbar_barra__5; //Sprite
	//this.TOOLBAR.toolbar_punti; //Text
	//this.TOOLBAR.toolbar_location; //Text
	//this.TOOLBAR.player_bg; //Sprite
	//this.TOOLBAR.player_ico; //Sprite
	//this.TOOLBAR.player_name; //Text
	this.game_logo; //Sprite
	this.button_exit; //Sprite CLICKABLE(LEV.0)
	this.button_search; //Sprite CLICKABLE(LEV.1)
	this.button_share; //Sprite CLICKABLE(LEV.2)
	this.INVENTARIO; //Container HIDDEN
	//this.INVENTARIO.button_inventario; //Sprite HIDDEN
	this.SETTINGS = {
		button_settings : {},
		button_settings_active : {},
		button_audio_off : {},
		button_audio_on : {},
		button_lang_en : {},
		button_lang_it : {},
		button_help : {},
		button_delete : {},
	}; //Container
	//this.SETTINGS.button_settings; //Sprite CLICKABLE(LEV.3)
	//this.SETTINGS.button_settings_active; //Sprite CLICKABLE(LEV.3)
	//this.SETTINGS.button_audio_off; //Sprite CLICKABLE(LEV.4)
	//this.SETTINGS.button_audio_on; //Sprite CLICKABLE(LEV.4)
	//this.SETTINGS.button_lang_en; //Sprite CLICKABLE(LEV.5)
	//this.SETTINGS.button_lang_it; //Sprite CLICKABLE(LEV.5)
	//this.SETTINGS.button_help; //Sprite CLICKABLE(LEV.6)
	//this.SETTINGS.button_delete; //Sprite CLICKABLE(LEV.6)
	this.MESSAGE = {}; //Container
	//this.MESSAGE.message_ico_bg; //Sprite HIDDEN
	//this.MESSAGE.message_ico; //Sprite HIDDEN
	//this.MESSAGE.message_leggio_ico; //Sprite HIDDEN
	//this.MESSAGE.message_bg; //Sprite HIDDEN
	//this.MESSAGE.button_exit_message; //Sprite HIDDEN CLICKABLE(LEV.7)
	//this.MESSAGE.message_titolo; //Text HIDDEN
	//this.MESSAGE.message_testo; //Text HIDDEN
	this.messageContainer = {};
	//--- GAME VARS
	var that = this;
	this.relocables = null;
	this.introMsg = "";
	var canvasW = 1280;
    var canvasH = 720;
	var mapTiles, player,  firstKey,
	allEnemies = [],
	enemies = [], directions = [0, 90, 180, 270],
	mapEnemies = [], mapGurus = [], mapLeggios = [], mapToponomastic = [],
	keysPressed = {
		38: 0,
		40: 0,
		37: 0,
		39: 0
	};
	this.gurus = [];
	this.leggios = [];
	this.allGurus = [];
	this.allLeggios = [];
	this.mapdata = null;
	this.mapAdditionalData = null;
	this.layerZonesIdsSubtract = 0;
	var w,h ,bW,bH;
	this.layerz = [];
	this.playerLayerId = -1;
	this.miniMap_scale = 8;
	this.animSpritesFramerate = 24;
	this.enemyGoal = null;
	this.audioGameId = 1;
	this.audioInPlay = false;
	this.bgmLoop = false;
	//--
	this.gamepadActive = false;
	this.gamepads = [];
	//---
	this.actualAreas = null;
	this.actualVetrinas = null;
	this.actualModules = null;
	
	//--- SALVATE NEL LOCALSTORAGE
	this.lang = "it";
	this.visitedAreas = [];
	this.visitedVetrinas = [];
	this.visitedModules = [];
	this.visitedGurus = [];
	this.visitedLeggios = [];
	this.visitedHiddens = [];
	this.playerUnlocked = [];
	this.playerSkin = 2;
	this.playerPoints = 0;
	//---------
	this.initSceneAndSprite = function () {
		if (!this.stage) {
			this.stage = new createjs.Stage(document.getElementById("testCanvas"));
			this.stage.enableMouseOver(30);
			createjs.Touch.enable(this.stage);
			this.stage.snapToPixelEnabled = true;
			createjs.Ticker.framerate = 60;
			createjs.Ticker.timingMode = createjs.Ticker.RAF_SYNCHED;
		}
		//------------------------------------------------------------------------------------------------------------------------------------
		var defShadow_dark = { color: "#000000", offsetX: 1, offsetY: 1, blur: 3 };
		var defShadow_light = { color: "#ffffff", offsetX: 1, offsetY: 1, blur: 3 };

		var mapContainer = new createjs.Container();
		mapContainer.name = "mapContainer";
		this.stage.addChild(mapContainer);
		this.stage.mapContainer = mapContainer;
					
		this.testo_label = new createjs.Text("999", "bold 30px 'Minimal5x7'", "#ffffff") ;
		this.testo_label.iniText = "999";
		this.testo_label.name = "testo_label";
		this.testo_label.textBaseline = "alphabetic";
		this.testo_label.cX = 1185;
		this.testo_label.dX = 1185;
		this.testo_label.textAlign = "center";
		this.testo_label.y = 200;
		this.horizontalCentralAlignText(this.testo_label, this.testo_label.cX);
		this.testo_label.shadow = new createjs.Shadow(defShadow_dark.color, defShadow_dark.offsetX, defShadow_dark.offsetY, defShadow_dark.blur);
		this.testo_label.mouseEnabled = false;
		this.stage.addChild(this.testo_label);

		this.testo_value = new createjs.Text("999", "bold 30px 'Minimal5x7'", "#ffffff") ;
		this.testo_value.iniText = "999";
		this.testo_value.name = "testo_value";
		this.testo_value.textBaseline = "alphabetic";
		this.testo_value.cX = 1232;
		this.testo_value.dX = 1232;
		this.testo_value.textAlign = "center";
		this.testo_value.y = 230;
		this.horizontalCentralAlignText(this.testo_value, this.testo_value.cX);
		this.testo_value.shadow = new createjs.Shadow(defShadow_dark.color, defShadow_dark.offsetX, defShadow_dark.offsetY, defShadow_dark.blur);
		this.testo_value.mouseEnabled = false;
		this.stage.addChild(this.testo_value);
		//------------------

		this.setupMap();

		this.setupToponomastica();

		//------------------

		this.miniMap = new createjs.Container();
		this.miniMap.name = "miniMap";
		this.miniMap.x = 1092;
		this.miniMap.y = 10;
		this.miniMap.scaleX = this.miniMap.scaleY = 0.75;
		this.stage.addChild(this.miniMap);
		this.stage.miniMap = this.miniMap;
		
		var miniMap_bg = new createjs.Sprite(this.spriteSheets[0], "mappa_bg");
		if (!window.modelApp.isDesktop && window.config.mobileAssetScale) miniMap_bg.scaleX = miniMap_bg.scaleY = window.config.mobileAssetScale;
		miniMap_bg.gotoAndStop("mappa_bg");
		miniMap_bg.name = "miniMap_bg";
		miniMap_bg.x = -8;
		miniMap_bg.y = -8;
		this.miniMap.addChild(miniMap_bg);
		this.miniMap.bg = miniMap_bg;

		var miniMap_mappa = new createjs.Sprite(this.spriteSheets[0], "mappa");
		if (!window.modelApp.isDesktop && window.config.mobileAssetScale) miniMap_mappa.scaleX = miniMap_mappa.scaleY = window.config.mobileAssetScale;
		miniMap_mappa.gotoAndStop("mappa");
		miniMap_mappa.name = "miniMap_mappa";
		miniMap_mappa.x = 0;
		miniMap_mappa.y = 0;
		this.miniMap.addChild(miniMap_mappa);
		this.miniMap.mappa = miniMap_mappa;

		var miniMap_player = new createjs.Shape();
		miniMap_player.graphics.beginFill("rgba(255,0,0,1)").drawRect(0, 0, 6, 6);
		miniMap_player.cache(0, 0, 6, 6);
		miniMap_player.regX = 3;
		miniMap_player.regY = 3;
		miniMap_player.x = 80 / this.miniMap_scale;
		miniMap_player.y = 80 / this.miniMap_scale;
		this.miniMap.addChild(miniMap_player);
		this.miniMap.player = miniMap_player;

		var miniMapShadow= {  "color": "#000000", "offsetX": "0", "offsetY": "3", "blur": "5"  };
		this.miniMap.shadow = new createjs.Shadow(miniMapShadow.color, miniMapShadow.offsetX, miniMapShadow.offsetY, miniMapShadow.blur);
		
		this.logo = new createjs.Sprite(this.spriteSheets[0], "logo");
		if (!window.modelApp.isDesktop && window.config.mobileAssetScale) this.logo.scaleX = this.logo.scaleY = window.config.mobileAssetScale * 0.25
		else this.logo.scaleX = this.logo.scaleY = 0.25;
		this.logo.gotoAndStop("logo");
		this.logo.name = "logo";
		this.logo.x = 1230;
		this.logo.y = 8;
		this.stage.addChild(this.logo);

		//------------------
		
		this.TOOLBAR = new createjs.Container();
		this.TOOLBAR.name = "TOOLBAR";
		this.TOOLBAR.x = 710;
		this.TOOLBAR.y = 1270;
		this.stage.addChild(this.TOOLBAR);

		var toolbar_bg = new createjs.Sprite(this.spriteSheets[0], "toolbar_bg");
		if (!window.modelApp.isDesktop && window.config.mobileAssetScale) toolbar_bg.scaleX = toolbar_bg.scaleY = window.config.mobileAssetScale;
		toolbar_bg.gotoAndStop("toolbar_bg");
		toolbar_bg.name = "toolbar_bg";
		toolbar_bg.x = 41;
		toolbar_bg.y = -637;
		//toolbar_bg.width = 435;
		//toolbar_bg.height = 80;
		this.TOOLBAR.addChild(toolbar_bg);
		this.TOOLBAR.toolbar_bg = toolbar_bg;

		var toolbar_barra__1 = new createjs.Sprite(this.spriteSheets[0], "toolbar_barra");
		if (!window.modelApp.isDesktop && window.config.mobileAssetScale) toolbar_barra__1.scaleX = toolbar_barra__1.scaleY = window.config.mobileAssetScale;
		toolbar_barra__1.gotoAndStop("toolbar_barra");
		toolbar_barra__1.name = "toolbar_barra__1";
		toolbar_barra__1.x = 195;
		toolbar_barra__1.y = -595+24;
		toolbar_barra__1.regY = 24;
		if (!window.modelApp.isDesktop && window.config.mobileAssetScale) toolbar_barra__1.regY = toolbar_barra__1.regY / window.config.mobileAssetScale;
		toolbar_barra__1.scaleY = 0;
		//toolbar_barra__1.width = 30;
		//toolbar_barra__1.height = 24;
		this.TOOLBAR.addChild(toolbar_barra__1);
		this.TOOLBAR.toolbar_barra__1 = toolbar_barra__1;

		var toolbar_barra__2 = new createjs.Sprite(this.spriteSheets[0], "toolbar_barra");
		if (!window.modelApp.isDesktop && window.config.mobileAssetScale) toolbar_barra__2.scaleX = toolbar_barra__2.scaleY = window.config.mobileAssetScale;
		toolbar_barra__2.gotoAndStop("toolbar_barra");
		toolbar_barra__2.name = "toolbar_barra__2";
		toolbar_barra__2.x = 254;
		toolbar_barra__2.y = -595+24;
		toolbar_barra__2.regY = 24;
		if (!window.modelApp.isDesktop && window.config.mobileAssetScale) toolbar_barra__2.regY = toolbar_barra__2.regY / window.config.mobileAssetScale;
		toolbar_barra__2.scaleY = 0;
		//toolbar_barra__2.width = 30;
		//toolbar_barra__2.height = 24;
		this.TOOLBAR.addChild(toolbar_barra__2);
		this.TOOLBAR.toolbar_barra__2 = toolbar_barra__2;

		var toolbar_barra__3 = new createjs.Sprite(this.spriteSheets[0], "toolbar_barra");
		if (!window.modelApp.isDesktop && window.config.mobileAssetScale) toolbar_barra__3.scaleX = toolbar_barra__3.scaleY = window.config.mobileAssetScale;
		toolbar_barra__3.gotoAndStop("toolbar_barra");
		toolbar_barra__3.name = "toolbar_barra__3";
		toolbar_barra__3.x = 313;
		toolbar_barra__3.y = -595+24;
		toolbar_barra__3.regY = 24;
		if (!window.modelApp.isDesktop && window.config.mobileAssetScale) toolbar_barra__3.regY = toolbar_barra__3.regY / window.config.mobileAssetScale;
		toolbar_barra__3.scaleY = 0;
		//toolbar_barra__3.width = 30;
		//toolbar_barra__3.height = 24;
		this.TOOLBAR.addChild(toolbar_barra__3);
		this.TOOLBAR.toolbar_barra__3 = toolbar_barra__3;

		var toolbar_barra__4 = new createjs.Sprite(this.spriteSheets[0], "toolbar_barra");
		if (!window.modelApp.isDesktop && window.config.mobileAssetScale) toolbar_barra__4.scaleX = toolbar_barra__4.scaleY = window.config.mobileAssetScale;
		toolbar_barra__4.gotoAndStop("toolbar_barra");
		toolbar_barra__4.name = "toolbar_barra__4";
		toolbar_barra__4.x = 373;
		toolbar_barra__4.y = -595+24;
		toolbar_barra__4.regY = 24;
		if (!window.modelApp.isDesktop && window.config.mobileAssetScale) toolbar_barra__4.regY = toolbar_barra__4.regY / window.config.mobileAssetScale;
		toolbar_barra__4.scaleY = 0;
		//toolbar_barra__4.width = 30;
		//toolbar_barra__4.height = 24;
		this.TOOLBAR.addChild(toolbar_barra__4);
		this.TOOLBAR.toolbar_barra__4 = toolbar_barra__4;

		var toolbar_barra__5 = new createjs.Sprite(this.spriteSheets[0], "toolbar_barra");
		if (!window.modelApp.isDesktop && window.config.mobileAssetScale) toolbar_barra__5.scaleX = toolbar_barra__5.scaleY = window.config.mobileAssetScale;
		toolbar_barra__5.gotoAndStop("toolbar_barra");
		toolbar_barra__5.name = "toolbar_barra__5";
		toolbar_barra__5.x = 432;
		toolbar_barra__5.y = -595+24;
		toolbar_barra__5.regY = 24;
		if (!window.modelApp.isDesktop && window.config.mobileAssetScale) toolbar_barra__5.regY = toolbar_barra__5.regY / window.config.mobileAssetScale;
		toolbar_barra__5.scaleY = 0;
		//toolbar_barra__5.width = 30;
		//toolbar_barra__5.height = 24;
		this.TOOLBAR.addChild(toolbar_barra__5);
		this.TOOLBAR.toolbar_barra__5 = toolbar_barra__5;

		var toolbar_punti = new createjs.Text("999999999", "16px 'Minimal3x5'", "#FFFFFF") ;
		toolbar_punti.iniText = "999999999";
		toolbar_punti.name = "toolbar_punti";
		toolbar_punti.textBaseline = "alphabetic";
		toolbar_punti.cX = 161;
		toolbar_punti.dX = 161;
		toolbar_punti.textAlign = "center";
		toolbar_punti.y = -577;
		this.horizontalCentralAlignText(toolbar_punti, toolbar_punti.cX);
		toolbar_punti.shadow = new createjs.Shadow(defShadow_dark.color, defShadow_dark.offsetX, defShadow_dark.offsetY, defShadow_dark.blur);
		toolbar_punti.mouseEnabled = false;
		//toolbar_punti.width = 76;
		//toolbar_punti.height = 16;
		this.TOOLBAR.addChild(toolbar_punti);
		this.TOOLBAR.toolbar_punti = toolbar_punti;

		var toolbar_location = new createjs.Text("SEI NELL’AREA SALA GIOCHI, VETRINA 01, MODULO V1AA", "16px 'Minimal3x5'", "#FFFFFF") ;
		toolbar_location.iniText = "SEI NELL’AREA SALA GIOCHI, VETRINA 01, MODULO V1AA";
		toolbar_location.name = "toolbar_location";
		toolbar_location.textBaseline = "alphabetic";
		toolbar_location.cX = 441;
		toolbar_location.dX = 441;
		toolbar_location.textAlign = "center";
		toolbar_location.y = -610;
		this.horizontalCentralAlignText(toolbar_location, toolbar_location.cX);
		toolbar_location.shadow = new createjs.Shadow(defShadow_dark.color, defShadow_dark.offsetX, defShadow_dark.offsetY, defShadow_dark.blur);
		toolbar_location.mouseEnabled = false;
		//toolbar_location.width = 358;
		//toolbar_location.height = 18;
		this.TOOLBAR.addChild(toolbar_location);
		this.TOOLBAR.toolbar_location = toolbar_location;

		var player_bg = new createjs.Sprite(this.spriteSheets[0], "player_bg");
		if (!window.modelApp.isDesktop && window.config.mobileAssetScale) player_bg.scaleX = player_bg.scaleY = window.config.mobileAssetScale;
		player_bg.gotoAndStop("player_bg");
		player_bg.name = "player_bg";
		player_bg.x = 480;
		player_bg.y = -637;
		//player_bg.width = 81;
		//player_bg.height = 82;
		this.TOOLBAR.addChild(player_bg);
		this.TOOLBAR.player_bg = player_bg;

		var player_ico = new createjs.Sprite(this.spriteSheets[0], "player_ico");
		if (!window.modelApp.isDesktop && window.config.mobileAssetScale) player_ico.scaleX = player_ico.scaleY = window.config.mobileAssetScale * 0.35
		else player_ico.scaleX = player_ico.scaleY = 0.35;
		player_ico.gotoAndStop("player_ico");
		player_ico.name = "player_ico";
		player_ico.x = 507;
		player_ico.y = -628;
		player_ico.mouseEnabled = false;
		//player_ico.width = 27;
		//player_ico.height = 47;
		this.TOOLBAR.addChild(player_ico);
		this.TOOLBAR.player_ico = player_ico;

		var player_name = new createjs.Text("YOU", "16px 'Minimal3x5'", "#FFFFFF") ;
		player_name.iniText = "YOU";
		player_name.name = "player_name";
		player_name.textBaseline = "alphabetic";
		player_name.cX = 533;
		player_name.dX = 533;
		player_name.textAlign = "center";
		player_name.y = -568;
		player_name.mouseEnabled = false;
		this.horizontalCentralAlignText(player_name, player_name.cX);
		player_name.shadow = new createjs.Shadow(defShadow_dark.color, defShadow_dark.offsetX, defShadow_dark.offsetY, defShadow_dark.blur);
		player_name.mouseEnabled = false;
		//player_name.width = 28;
		//player_name.height = 17;
		this.TOOLBAR.addChild(player_name);
		this.TOOLBAR.player_name = player_name;

		this.game_logo = new createjs.Sprite(this.spriteSheets[0], "game_logo");
		if (!window.modelApp.isDesktop && window.config.mobileAssetScale) this.game_logo.scaleX = this.game_logo.scaleY = window.config.mobileAssetScale;
		this.game_logo.gotoAndStop("game_logo");
		this.game_logo.name = "game_logo";
		this.game_logo.x = 440;
		this.game_logo.y = 9;
		this.game_logo.visible = false;
		//this.game_logo.width = 400;
		//this.game_logo.height = 100;
		this.stage.addChild(this.game_logo);

		this.button_exit = new createjs.Sprite(this.spriteSheets[0], "button_exit");
		if (!window.modelApp.isDesktop && window.config.mobileAssetScale) this.button_exit.scaleX = this.button_exit.scaleY = window.config.mobileAssetScale;
		this.button_exit.gotoAndStop("button_exit");
		this.button_exit.name = "button_exit";
		this.button_exit.x = 10;
		this.button_exit.y = 10;
		//this.button_exit.width = 81;
		//this.button_exit.height = 82;
		this.button_exit.cursor = "pointer";
		this.stage.addChild(this.button_exit);

		this.button_search = new createjs.Sprite(this.spriteSheets[0], "button_search");
		if (!window.modelApp.isDesktop && window.config.mobileAssetScale) this.button_search.scaleX = this.button_search.scaleY = window.config.mobileAssetScale;
		this.button_search.gotoAndStop("button_search");
		this.button_search.name = "button_search";
		this.button_search.x = 10;
		this.button_search.y = 633;
		this.button_search.visible = false;
		this.button_search.cursor = "pointer";
		this.stage.addChild(this.button_search);

		this.button_share = new createjs.Sprite(this.spriteSheets[0], "button_share");
		if (!window.modelApp.isDesktop && window.config.mobileAssetScale) this.button_share.scaleX = this.button_share.scaleY = window.config.mobileAssetScale;
		this.button_share.gotoAndStop("button_share");
		this.button_share.name = "button_share";
		this.button_share.x = 102;
		this.button_share.y = 633;
		this.button_share.visible = false;
		this.button_share.cursor = "pointer";
		this.stage.addChild(this.button_share);
		
		this.INVENTARIO = new createjs.Container();
		this.INVENTARIO.name = "INVENTARIO";
		this.INVENTARIO.x = 710;
		this.INVENTARIO.y = 1270;
		this.INVENTARIO.visible = false;
		this.stage.addChild(this.INVENTARIO);

		var button_inventario = new createjs.Sprite(this.spriteSheets[0], "button_inventario");
		if (!window.modelApp.isDesktop && window.config.mobileAssetScale) button_inventario.scaleX = button_inventario.scaleY = window.config.mobileAssetScale;
		button_inventario.gotoAndStop("button_inventario");
		button_inventario.name = "button_inventario";
		button_inventario.x = -44;
		button_inventario.y = -638;
		//button_inventario.width = 81;
		//button_inventario.height = 82;
		button_inventario.visible = false;
		this.INVENTARIO.addChild(button_inventario);
		this.INVENTARIO.button_inventario = button_inventario;

		this.setupMenuSettings();
		//-----------------------------

		this.MESSAGE = new createjs.Container();
		this.MESSAGE.name = "MESSAGE";
		this.MESSAGE.x = 1270;
		this.MESSAGE.y = 710;
		this.MESSAGE.visible = false;
		this.stage.addChild(this.MESSAGE);

		var message_ico_bg = new createjs.Sprite(this.spriteSheets[0], "message_ico_bg");
		if (!window.modelApp.isDesktop && window.config.mobileAssetScale) message_ico_bg.scaleX = message_ico_bg.scaleY = window.config.mobileAssetScale;
		message_ico_bg.gotoAndStop("message_ico_bg");
		message_ico_bg.name = "message_ico_bg";
		message_ico_bg.x = -614;
		message_ico_bg.y = -237;
		//message_ico_bg.width = 127;
		//message_ico_bg.height = 142;
		message_ico_bg.visible = false;
		this.MESSAGE.addChild(message_ico_bg);
		this.MESSAGE.message_ico_bg = message_ico_bg;

		var message_leggio_ico = new createjs.Sprite(this.spriteSheets[0], "leggio_ico");
		if (!window.modelApp.isDesktop && window.config.mobileAssetScale) message_leggio_ico.scaleX = message_leggio_ico.scaleY = window.config.mobileAssetScale;
		message_leggio_ico.gotoAndStop("leggio_ico");
		message_leggio_ico.name = "message_leggio_ico";
		message_leggio_ico.x = -602;
		message_leggio_ico.y = -235;
		//message_leggio_ico.width = 82;
		//message_leggio_ico.height = 126;
		message_leggio_ico.visible = false;
		this.MESSAGE.addChild(message_leggio_ico);
		this.MESSAGE.message_leggio_ico = message_leggio_ico;
		
		var message_ico = new createjs.Sprite(this.spriteSheets[0], "message_ico");
		if (!window.modelApp.isDesktop && window.config.mobileAssetScale) message_ico.scaleX = message_ico.scaleY = window.config.mobileAssetScale;
		message_ico.gotoAndStop("message_ico");
		message_ico.name = "message_ico";
		message_ico.x = -602;
		message_ico.y = -235;
		//message_ico.width = 82;
		//message_ico.height = 126;
		message_ico.visible = false;
		this.MESSAGE.addChild(message_ico);
		this.MESSAGE.message_ico = message_ico;

		var message_bg = new createjs.Sprite(this.spriteSheets[0], "message_bg");
		if (!window.modelApp.isDesktop && window.config.mobileAssetScale) message_bg.scaleX = message_bg.scaleY = window.config.mobileAssetScale;
		message_bg.gotoAndStop("message_bg");
		message_bg.name = "message_bg";
		message_bg.x = -519;
		message_bg.y = -253;
		//message_bg.width = 435;
		//message_bg.height = 170;
		this.MESSAGE.addChild(message_bg);
		this.MESSAGE.message_bg = message_bg;

		var button_exit_message = new createjs.Sprite(this.spriteSheets[0], "button_exit_message");
		if (!window.modelApp.isDesktop && window.config.mobileAssetScale) button_exit_message.scaleX = button_exit_message.scaleY = window.config.mobileAssetScale;
		button_exit_message.gotoAndStop("button_exit_message");
		button_exit_message.name = "button_exit_message";
		button_exit_message.x = -165;
		button_exit_message.y = -288;
		//button_exit_message.width = 81;
		//button_exit_message.height = 82;
		button_exit_message.cursor = "pointer";
		this.MESSAGE.addChild(button_exit_message);
		this.MESSAGE.button_exit_message = button_exit_message;

		var message_titolo = new createjs.Text("Lorem ipsum sator arepo", "bold 28px 'Minimal5x7'", "#B9EA5C") ;
		message_titolo.iniText = "Lorem ipsum sator arepo";
		message_titolo.name = "message_titolo";
		message_titolo.textBaseline = "alphabetic";
		message_titolo.lineHeight = 24;
		message_titolo.cX = -269;
		message_titolo.dX = -269;
		message_titolo.x = -498;
		message_titolo.y = -223;
		message_titolo.shadow = new createjs.Shadow(defShadow_dark.color, defShadow_dark.offsetX, defShadow_dark.offsetY, defShadow_dark.blur);
		message_titolo.mouseEnabled = false;
		//message_titolo.width = 229;
		//message_titolo.height = 17;
		this.MESSAGE.addChild(message_titolo);
		this.MESSAGE.message_titolo = message_titolo;

		var message_testo = new createjs.Text("Lorem ipsum sator arepo", "bold 22px 'Minimal5x7'", "#FFFFFF") ;
		message_testo.iniText = "Lorem ipsum sator arepo";
		message_testo.name = "message_testo";
		message_testo.textBaseline = "alphabetic";
		message_testo.lineHeight = 18;
		message_testo.cX = -269;
		message_testo.dX = -269;
		message_testo.x = -498;
		message_testo.y = -187;
		message_testo.shadow = new createjs.Shadow(defShadow_dark.color, defShadow_dark.offsetX, defShadow_dark.offsetY, defShadow_dark.blur);
		message_testo.mouseEnabled = false;
		//message_testo.width = 229;
		//message_testo.height = 17;
		this.MESSAGE.addChild(message_testo);
		this.MESSAGE.message_testo = message_testo;
		//------------------

		this.localizeLabels();
		//** this.initHtmlGui();
		this.setupIntroMessageContainer();

		if (virtualJoystickActive) this.virtualJoystickInit();
	};
	//---------
	this.startGame = function () {
		this.relocables = [
			{ "id": "this.testo_label", "kind": "Text", "h": { "x": 1185, "y": 200, "v": true }, "v": { "x": 625, "y": 200, "v": true } },
			{ "id": "this.testo_value", "kind": "Text", "h": { "x": 1232, "y": 230, "v": true }, "v": { "x": 672, "y": 230, "v": true }} ,
			{ "id": "this.stage.mapContainer", "kind": "Container", "h": { "x": 0, "y": 0, "v": true }, "v": { "x": -280, "y": 280, "v": true }},
			{ "id": "this.miniMap", "kind": "Container", "h": { "x": 1092 }, "v": { "x": 532 } },
			{ "id": "this.logo", "kind": "Sprite", "h": { "x": 1230 , "y": 8}, "v": { "x": 670, "y": 8 } },

			{ "id": "this.TOOLBAR.toolbar_bg", "kind": "Sprite", "h": { "x": "41", "y": "-637", "v": true }, "v": { "x": "-519", "y": "-77", "v": true } },
			{ "id": "this.TOOLBAR.toolbar_barra__1", "kind": "Sprite", "h": { "x": "195", "y": "-571", "v": true }, "v": { "x": "-365", "y": "-11", "v": true } },
			{ "id": "this.TOOLBAR.toolbar_barra__2", "kind": "Sprite", "h": { "x": "254", "y": "-571", "v": true }, "v": { "x": "-306", "y": "-11", "v": true } },
			{ "id": "this.TOOLBAR.toolbar_barra__3", "kind": "Sprite", "h": { "x": "313", "y": "-571", "v": true }, "v": { "x": "-247", "y": "-11", "v": true } },
			{ "id": "this.TOOLBAR.toolbar_barra__4", "kind": "Sprite", "h": { "x": "373", "y": "-571", "v": true }, "v": { "x": "-187", "y": "-11", "v": true } },
			{ "id": "this.TOOLBAR.toolbar_barra__5", "kind": "Sprite", "h": { "x": "432", "y": "-571", "v": true }, "v": { "x": "-128", "y": "-11", "v": true } },
			{ "id": "this.TOOLBAR.toolbar_punti", "kind": "Text", "h": { "x": 161, "y": -577, "v": true }, "v": { "x": -399, "y": -17, "v": true } },
			{ "id": "this.TOOLBAR.toolbar_location", "kind": "Text", "h": { "x": 441, "y": -610, "v": true }, "v": { "x": -119, "y": -49, "v": true } },
			{ "id": "this.TOOLBAR.player_bg", "kind": "Sprite", "h": { "x": "480", "y": "-637", "v": true }, "v": { "x": "-80", "y": "-77", "v": true } },
			{ "id": "this.TOOLBAR.player_ico", "kind": "Sprite", "h": { "x": "507", "y": "-628", "v": true }, "v": { "x": "-53", "y": "-68", "v": true } },
			{ "id": "this.TOOLBAR.player_name", "kind": "Text", "h": { "x": 533, "y": -568, "v": true }, "v": { "x": -27, "y": -8, "v": true } },
			{ "id": "this.game_logo", "kind": "Sprite", "h": { "x": "440", "y": "9", "v": true }, "v": { "x": "109", "y": "9", "v": true } },
			{ "id": "this.button_search", "kind": "Sprite", "h": { "x": "10", "y": "633", "v": true }, "v": { "x": "10", "y": "1191", "v": true } },
			{ "id": "this.button_share", "kind": "Sprite", "h": { "x": "102", "y": "633", "v": true }, "v": { "x": "102", "y": "1191", "v": true } },
			{ "id": "this.INVENTARIO.button_inventario", "kind": "Sprite", "h": { "x": "-44", "y": "-638", "v": false }, "v": { "x": "-168", "y": "-165", "v": false } },
			{ "id": "this.SETTINGS.button_settings", "kind": "Sprite", "h": { "x": "480", "y": "-725"  }, "v": { "x": "-80", "y": "-165" } },
			{ "id": "this.SETTINGS.button_settings_active", "kind": "Sprite", "h": { "x": "480", "y": "-725"  }, "v": { "x": "-80", "y": "-165" } },
			{ "id": "this.SETTINGS.button_audio_off", "kind": "Sprite", "h": { "x": "480", "y": "-813" }, "v": { "x": "-80", "y": "-253" } },
			{ "id": "this.SETTINGS.button_audio_on", "kind": "Sprite", "h": { "x": "480", "y": "-813" }, "v": { "x": "-80", "y": "-253" } },
			{ "id": "this.SETTINGS.button_lang_en", "kind": "Sprite", "h": { "x": "480", "y": "-900"}, "v": { "x": "-80", "y": "-340" } },
			{ "id": "this.SETTINGS.button_lang_it", "kind": "Sprite", "h": { "x": "480", "y": "-900" }, "v": { "x": "-80", "y": "-340" } },
			{ "id": "this.SETTINGS.button_help", "kind": "Sprite", "h": { "x": "480", "y": "-988" }, "v": { "x": "-80", "y": "-428" } },
			{ "id": "this.SETTINGS.button_delete", "kind": "Sprite", "h": { "x": "480", "y": "-1076" }, "v": { "x": "-80", "y": "-516" } },
			{ "id": "this.MESSAGE", "kind": "Container", "h": { "x":1270, "y":710 }, "v": {"x":710, "y":1270 } },
		];
		//------------------
		this.setupCharacters();
		this.setupControls();
		//------------------
		this.updateBySO();
		//------------------
		//REAL START
		//tick
		this.ontick = this.tick.bind(this);
		createjs.Ticker.on("tick", this.ontick);
		createjs.Ticker.addEventListener("tick", this.stage);
		//resize
		if (this.localLaunch) makeResponsive(1280, 720, document.getElementById("testCanvas"), this.stage);
		if (this.relocables !== undefined && this.relocables !== null && this.relocables.length > 0) this.makeRelocable();
		//preload end
		this.endPreload();
		//------------------
		// LAUNCH
		this.messageContainer.that = this;
		this.messageContainer.addEventListener("click", this.closeMessageContainer);
		if (this.introMsg.length > 0) {
			setTimeout(function () {
				that.closeMessageContainer({currentTarget: that.messageContainer});
			}, this.tempoPostFadePanel);
		} else {
			this.introText.visible = false;
			this.messageContainer.visible = false;
			this.game_logo.alpha = 0;
			this.game_logo.visible = true;
			createjs.Tween.get(this.game_logo).to({alpha: 1}, 1000);
			this.state = this.BonusStateEnum.READY;
		}
	};
	//---------
	this.initHtmlGui = function() {
		//**
	};
	this.endPreload = function() {
		var wrapper = document.querySelector('#_preload_div_ svg')
		wrapper.classList.remove('active')
		setTimeout(function anprel() {
			var canvas = document.getElementById("testCanvas");
			canvas.style.display = 'block';
			var preloaderDiv = document.getElementById("_preload_div_");
			preloaderDiv.style.display = 'none';
		}, preloadCloseTime)
	};
	//---------
	this.tick = function (e) {
		if (this.state == this.BonusStateEnum.CONCLUDED) {
		}
		if (this.state == this.BonusStateEnum.READY || this.state == this.BonusStateEnum.SEQUENCE ) {
			if (enemyActive) enemyBrain();
		}
		if (this.state == this.BonusStateEnum.READY) {
			if (!detectGamepadsMovement()) detectKeys();
			if (lightTilesActive) lightTiles();
			if (guruActive) guruBrain();
			if (leggioActive) leggioBrain();
			updateMiniMap();
			updatePlayerZone();
		}
		detectGamepadsButtons();
	};
	function updateMiniMap() {
		that.miniMap.player.x = player.x / that.miniMap_scale;
		that.miniMap.player.y = player.y / that.miniMap_scale;
	};
	function updatePlayerZone() {
		var zx = Math.floor(player.x/bW);
		var zy = Math.floor(player.y/bH);
		var actualAreas_id = that.getBlock(zx,zy,3) - that.layerZonesIdsSubtract;
		if (actualAreas_id<0) actualAreas_id = 0;
		
		var actualVetrinas_id = that.getBlock(zx,zy,4) - that.layerZonesIdsSubtract;
		var actualModules_id = that.getBlock(zx,zy,5) - that.layerZonesIdsSubtract;
		if (actualVetrinas_id<0) actualVetrinas_id = 0;
		if (actualModules_id<0) actualModules_id = 0;
		//vedo anche intorno
		var rots = [ {x:-1, y:0}, {x:1, y:0}, {x:0, y:-1}, {x:0, y:1},
			{x:1, y:-1}, {x:-1, y:-1}, {x:1, y:1}, {x:-1, y:1} , {x:0, y:-2}];
		var rid = 0;
		while (actualVetrinas_id == 0 && rid < rots.length) {
			var rot = rots[rid];
			var nx = zx + rot.x;
			var ny = zy + rot.y;
			var avid = that.getBlock(nx,ny,4) - that.layerZonesIdsSubtract;
			if (avid>0) actualVetrinas_id = avid;
			rid++;
		}	
		rid = 0
		while (actualModules_id == 0 && rid < rots.length) {
			var rot = rots[rid];
			var nx = zx + rot.x;
			var ny = zy + rot.y;
			var amid = that.getBlock(nx,ny,5) - that.layerZonesIdsSubtract;
			if (amid>0) actualModules_id = amid;
			rid++;
		}		
		
		if (actualVetrinas_id<0) actualVetrinas_id = 0;
		if (actualModules_id<0) actualModules_id = 0;
		
		var actualAreas = null;
		if (actualAreas_id > 0) actualAreas = that.mapAdditionalData["AREAS"][(actualAreas_id<10? "0"+actualAreas_id:actualAreas_id)];
		var actualVetrinas = null;
		if (actualVetrinas_id > 0) actualVetrinas = that.mapAdditionalData["VETRINAS"][(actualVetrinas_id<10? "0"+actualVetrinas_id:actualVetrinas_id)];
		var actualModules = null; 
		if (actualModules_id > 0) actualModules = that.mapAdditionalData["MODULES"][(actualModules_id<10? "0"+actualModules_id:actualModules_id)];
		var cambiatoQualcosa = false;
		if ( actualAreas != that.actualAreas) {
			cambiatoQualcosa = true;
		}		
		if ( actualVetrinas != that.actualVetrinas) {
			cambiatoQualcosa = true;
		}		
		if ( actualModules != that.actualModules) {
			cambiatoQualcosa = true;
		}
		that.actualAreas = actualAreas;
		that.actualVetrinas = actualVetrinas;
		that.actualModules = actualModules;
		
		if (cambiatoQualcosa) {
			//** valutare per moduli che fare, se accendere solo vetrina con prossimità
			//** e se vetrina tenerla accesa, non solo prima volta
			if (actualAreas_id > 0) {
				if (that.visitedAreas.indexOf(actualAreas) < 0) {
					that.visitedAreas.push(actualAreas);
					that.playerPoints += that.mapAdditionalData["POINTS"]["AREAS"];
					doPrimaVoltaMessageArea();
				}
			}
			if (actualVetrinas_id > 0) {
				if (that.visitedVetrinas.indexOf(actualVetrinas) < 0) {
					that.visitedVetrinas.push(actualVetrinas);
					that.playerPoints += that.mapAdditionalData["POINTS"]["VETRINAS"];
					doPrimaVoltaMessageVetrina();
				}
			}
			if (actualModules_id > 0){
				if (that.visitedModules.indexOf(actualModules) < 0) {
					that.visitedModules.push(actualModules);
					that.playerPoints += that.mapAdditionalData["POINTS"]["MODULES"];
					doPrimaVoltaMessageModulo();
				}
				var modules = that.layerz[that.layerz.length-2];
				createjs.Tween.removeTweens(modules);
				that.showArea("MODULES", that.actualModules);
				createjs.Tween.get(modules).wait(tempoHighlightSuccessivo).call(that.hideArea);
			}
			that.updateHud();
			that.checkEndGame();
		}
	};
	function doPrimaVoltaMessageArea() {
		var areas = that.layerz[that.layerz.length-4];
		if (faiAvvisiPrimaVolta) {
			createjs.Tween.removeTweens(areas);
			that.showArea("AREAS", that.actualAreas);
			createjs.Tween.get(areas).wait(tempoHighlightPrimaVolta).call(that.hideArea);
		}
		var completateTutte = false;
		if (that.visitedAreas.length >= that.mapAdditionalData["COUNTS"]["AREAS"]) completateTutte = true;
		if (faiAvvisiPrimaVolta) that.doMessaggio(that.mapAdditionalData["TEXTS"][that.lang]["AREA"] + ": " +that.mapAdditionalData["LABELS"][that.lang][that.actualAreas].replace("\n", " "	), that.mapAdditionalData["TEXTS"][that.lang]["PRIMAVOLTA"] + "\n" +that.mapAdditionalData["TEXTS"][that.lang][that.actualAreas]);
		if (completateTutte) {
			if (that.audioInplay) that.bgmLoop = window.timeliner.fx.playBonusExternalSound("win", that.audioGameId);
			that.doMessaggio(that.mapAdditionalData["TEXTS"][that.lang]["HAICOMPLETATO_TITLE"].replace("\n", " "), that.mapAdditionalData["TEXTS"][that.lang]["HAICOMPLETATO_TEXT"] + "\n" + that.mapAdditionalData["TEXTS"][that.lang]["AREA"]);
		}
	};
	function doPrimaVoltaMessageVetrina() {
		var vetrinas = that.layerz[that.layerz.length-3];
		if (faiAvvisiPrimaVolta) {
			createjs.Tween.removeTweens(vetrinas);
			that.showArea("VETRINAS", that.actualVetrinas);
			createjs.Tween.get(vetrinas).wait(tempoHighlightPrimaVolta).call(that.hideArea);
		}
		var completateTutte = false;
		if (that.visitedVetrinas.length >= that.mapAdditionalData["COUNTS"]["VETRINAS"]) completateTutte = true;
		if (faiAvvisiPrimaVolta) that.doMessaggio(that.mapAdditionalData["LABELS"][that.lang][that.actualVetrinas].replace("\n", " "), that.mapAdditionalData["TEXTS"][that.lang]["PRIMAVOLTA"] + "\n" + that.mapAdditionalData["TEXTS"][that.lang][that.actualVetrinas]); //that.mapAdditionalData["TEXTS"][that.lang]["VETRINA"] + ": " +
		if (completateTutte) {
			if (that.audioInplay) that.bgmLoop = window.timeliner.fx.playBonusExternalSound("win", that.audioGameId);
			that.doMessaggio(that.mapAdditionalData["TEXTS"][that.lang]["HAICOMPLETATO_TITLE"].replace("\n", " "), that.mapAdditionalData["TEXTS"][that.lang]["HAICOMPLETATO_TEXT"] + "\n" + that.mapAdditionalData["TEXTS"][that.lang]["VETRINA"]);
		}
	};
	function doPrimaVoltaMessageModulo() {
		var modules = that.layerz[that.layerz.length-2];
		if (faiAvvisiPrimaVolta) {
			createjs.Tween.removeTweens(modules);
			that.showArea("MODULES", that.actualModules);
			createjs.Tween.get(modules).wait(tempoHighlightPrimaVolta).call(that.hideArea);
		}
		var completateTutte = false;
		if (that.visitedModules.length >= that.mapAdditionalData["COUNTS"]["MODULES"]) completateTutte = true;
		//if (faiAvvisiPrimaVolta)  that.doMessaggio(that.mapAdditionalData["TEXTS"][that.lang]["MODULE"] + ": " +that.actualModules, that.mapAdditionalData["TEXTS"][that.lang]["PRIMAVOLTA"] + "\n",null, 0 ); //moduli non hanno testi descrittivi
		if (completateTutte) {
			if (that.audioInplay) that.bgmLoop = window.timeliner.fx.playBonusExternalSound("win", that.audioGameId);
			that.doMessaggio(that.mapAdditionalData["TEXTS"][that.lang]["HAICOMPLETATO_TITLE"].replace("\n", " "), that.mapAdditionalData["TEXTS"][that.lang]["HAICOMPLETATO_TEXT"] + "\n" + that.mapAdditionalData["TEXTS"][that.lang]["MODULE"]);
		}
	};
	this.updateHud = function() {
		var stringLocation = "";
		if (that.actualAreas) {
			stringLocation = that.mapAdditionalData["TEXTS"][that.lang]["SEIAREA"] + " " + that.mapAdditionalData["LABELS"][that.lang][that.actualAreas].replace("\n", " ");
			if (that.actualVetrinas != null) stringLocation += ", " + that.mapAdditionalData["LABELS"][that.lang][that.actualVetrinas].replace("\n", " ");// that.mapAdditionalData["TEXTS"][that.lang]["VETRINA"] + " " +
			if (that.actualModules != null) stringLocation += ", " + that.mapAdditionalData["TEXTS"][that.lang]["MODULE"] + " " + that.actualModules.replace("\n", " ");
		}
		if (stringLocation!= "") that.TOOLBAR.toolbar_location.text = stringLocation;
		that.TOOLBAR.toolbar_punti.text = that.playerPoints; //that.mapAdditionalData["TEXTS"][that.lang]["POINTS"]
		
		var scalaAreas = 0;
		if (that.mapAdditionalData["COUNTS"]["AREAS"]>0) scalaAreas = that.visitedAreas.length / that.mapAdditionalData["COUNTS"]["AREAS"];
		if (!window.modelApp.isDesktop && window.config.mobileAssetScale) scalaAreas *= window.config.mobileAssetScale;
		that.TOOLBAR.toolbar_barra__1.scaleY = scalaAreas;
		var scalaVetrinas = 0;
		if (that.mapAdditionalData["COUNTS"]["VETRINAS"]>0) scalaVetrinas = that.visitedVetrinas.length / that.mapAdditionalData["COUNTS"]["VETRINAS"];
		if (!window.modelApp.isDesktop && window.config.mobileAssetScale) scalaVetrinas *= window.config.mobileAssetScale;
		that.TOOLBAR.toolbar_barra__2.scaleY = scalaVetrinas;
		var scalaModules = 0;
		if (that.mapAdditionalData["COUNTS"]["MODULES"]>0) scalaModules = that.visitedModules.length / that.mapAdditionalData["COUNTS"]["MODULES"];
		if (!window.modelApp.isDesktop && window.config.mobileAssetScale) scalaModules *= window.config.mobileAssetScale;
		that.TOOLBAR.toolbar_barra__3.scaleY = scalaModules;
		var scalaGurus = 0;
		if (that.mapAdditionalData["COUNTS"]["GURUS"]>0) scalaGurus = that.visitedGurus.length / that.mapAdditionalData["COUNTS"]["GURUS"];
		if (!window.modelApp.isDesktop && window.config.mobileAssetScale) scalaGurus *= window.config.mobileAssetScale;
		that.TOOLBAR.toolbar_barra__4.scaleY = scalaGurus;
		var scalaLeggios = 0;
		if (that.mapAdditionalData["COUNTS"]["LEGGIOS"]>0) scalaLeggios = that.visitedLeggios.length / that.mapAdditionalData["COUNTS"]["LEGGIOS"];
		if (!window.modelApp.isDesktop && window.config.mobileAssetScale) scalaLeggios *= window.config.mobileAssetScale;
		that.TOOLBAR.toolbar_barra__5.scaleY = scalaLeggios;
		that.saveSO();
	};
	//---------
	this.setupToponomastica = function() {
		this.toponomastica = new createjs.Container();
		this.toponomastica.name = "toponomastica";
		this.toponomastica.x = 0;
		this.toponomastica.y = 0;
		this.stage.mapContainer.addChild(this.toponomastica);
		this.stage.mapContainer.topo = this.toponomastica;
		this.layerz.push(this.toponomastica);
		//--
		if (toponomasticaActive) {
			var obXadd = 0;
			var obYadd = 0;
			var arr = mapToponomastic;
			var len = arr.length;
			//---
			for (var n = 0; n < len; n++) {
				var ob = arr[n];
				var val = ob.bid - 1;
				var useKey = this.mapAdditionalData["KEYS"][val];
				addToponomastica(ob.x + obXadd, ob.y + obYadd, useKey);
			}
		}
	};
	this.setupMap = function () {
		w = this.mapdata.width;
		h = this.mapdata.height;
		bW = this.mapdata.tilewidth;
		bH = this.mapdata.tileheight;
		this.layerz = [];
		mapTiles = {};
		for (var l=0; l<this.mapdata.layers.length; l++) {
			var lad = this.mapdata.layers[l];
			if (lad.name != "walk" && lad.name != "enemies" && lad.name != "gurus" && lad.name != "leggios" && lad.name != "toponomastic" ) {
				var layo = new createjs.Container();
				var namo = lad.name;
				layo.name = namo;
				this.stage.mapContainer.addChild(layo);
				this.stage.mapContainer[namo] = layo;
				this.layerz.push(layo);
				if (lad.name != "modules") { //tolto lad.name != "enemies"
					layo.mouseEnabled = false;
					layo.mouseChildren = false;
				}
			}
			var matrix = lad.data;
			lad.matr = [];
			var index = 0;
			for (var y = 0; y<h; y++) {
				var row = [];
				for (var x = 0; x<w; x++) {
					var bid = matrix[index];
					if (lad.name == "enemies" || lad.name == "gurus" || lad.name == "leggios" || lad.name == "toponomastic" ) {
						if (bid > 0) {
							var ob = {
								x: x,
								y: y,
								bid: bid - this.layerZonesIdsSubtract
							};
							if (lad.name == "enemies") {
								mapEnemies.push(ob);
							} else if (lad.name == "gurus") {
								mapGurus.push(ob);
							} else if (lad.name == "leggios") {
								mapLeggios.push(ob);
							} else if (lad.name == "toponomastic") {
								mapToponomastic.push(ob);
							}
						}
					} else {
						var flid = this.mapdata.layers[0].data[index];
						if (lad.name == "walk") {
							mapTiles["b-" + y + "-" + x] = {
								index: index,
								walkable: (bid > 0),
								door: (bid == collisionDoorId)
							};
						}
						if (bid > 0 && lad.name != "walk") {
							var useSprite = "b"+bid+"b";
							var useName = "b-"+y+"-"+x;
							var useAlpha = 1;
							var useVisible = true;
							if ( lad.name == "areas" || lad.name == "vetrinas" || lad.name == "modules" ) {
								var newCid = bid - this.layerZonesIdsSubtract;
								var vid = this.mapAdditionalData[lad.name.toUpperCase()][(newCid<10? "0"+newCid:newCid)];
								useSprite = "b"+ 0 +"b";
								useName = vid;
								useAlpha = 0.1;
								if (lad.name != "modules") {
									useAlpha = 0.5;
									useVisible = false;
								}
							}
							var tile = new createjs.Sprite(this.spriteSheets[0], useSprite);
							if (!window.modelApp.isDesktop && window.config.mobileAssetScale) tile.scaleX = tile.scaleY = window.config.mobileAssetScale;
							tile.gotoAndStop(useSprite);
							tile.name = useName;
							tile.alpha = useAlpha;
							tile.visible = useVisible;
							tile.x = x*bW;
							tile.y = y*bH;
							if (lad.name == "modules") {
								/* //** TODO RIACCENDERE tile.cursor = "pointer";
								tile.addEventListener("click", that.clickFunctionModules);
								tile.addEventListener("mouseover", function tileover(e){that.showArea("MODULES", e.currentTarget.name);});
								tile.addEventListener("mouseout", function tileout(e) {that.hideArea()});
								 */
							}
							layo.addChild(tile);
						}
						if (lad.name == "over" && flid!=bid && forceOverIds.indexOf(flid) >= 0) {
							var tileO = new createjs.Sprite(this.spriteSheets[0], "b"+flid+"b");
							if (!window.modelApp.isDesktop && window.config.mobileAssetScale) tileO.scaleX = tileO.scaleY = window.config.mobileAssetScale;
							tileO.gotoAndStop("b"+flid+"b");
							tileO.name = "b-"+y+"-"+x;
							tileO.x = x*bW;
							tileO.y = y*bH;
							layo.addChild(tileO);
						}
					}
					row.push(bid);
					index++;
				}
				lad.matr.push(row);
			}
			
			if (lad.name == "walk" ) {
				this.playerLayerId = l+1;
				var layoE = new createjs.Container();
				layoE.name = "enemies";
				this.stage.mapContainer.addChild(layoE);
				this.stage.mapContainer["enemies"] = layoE;
				this.layerz.push(layoE);
				var layoP = new createjs.Container();
				layoP.name = "player";
				this.stage.mapContainer.addChild(layoP);
				this.stage.mapContainer["player"] = layoP;
				this.layerz.push(layoP);
			} else {
				if (lad.name != "enemies" && lad.name != "gurus" && lad.name != "leggios" && lad.name != "toponomastic") layo.cache(0, 0, w*bW, h*bH);
			}
		}
	};
	this.setupCharacters = function () {
		var obXadd = 0;
		var obYadd = 0;

		var len = 0;
		var n = 0;
		var arr = [];
		//---
		addPlayer(this.playerSkin); //** id 2 in base a grafica
		if (enemyActive) {
			arr = mapEnemies;
			n = 0;
			len = arr.length;
			for (n = 0; n< len; n++ ) {
				var ob = arr[n];
				var useBid = ob.bid;
				if (useBid == 2) useBid = 3;//** in base a grafica 2 è user
				if (useBid <= 1) useBid = 1;//** in base a grafica 0 è guru
				if (useBid > 11) useBid = 11;//** in base a grafica 12 è guru e > 12 non esistono
				addEnemy(ob.x+obXadd, ob.y+obYadd, useBid);
			}
		}
		if (guruActive) {
			var useKind = 0; //** id 0 o id 12 in base a grafica
			arr = mapGurus;
			n = 0;
			len = arr.length;
			for (n = 0; n< len; n++ ) {
				var ob = arr[n];
				var val = ob.bid;
				var useName = this.mapAdditionalData["AREAS"][(val<10? "0":"") + val];
				addGuru(ob.x+obXadd, ob.y+obYadd, useKind, useName );
			}
		}
		if (leggioActive) {
			var useKind = 1; //**
			arr = mapLeggios;
			n = 0;
			len = arr.length;
			for (n = 0; n< len; n++ ) {
				var ob = arr[n];
				var val = ob.bid;
				var useName = this.mapAdditionalData["VETRINAS"][(val<10? "0":"") + val];
				addLeggio(ob.x+obXadd, ob.y+obYadd, useKind, useName );
			}
		}
	};
	//---------
	this.stageMouse = null;
	this.stageFirst = null;
	this.stagePress = false;
	this.setupControls = function () {
		var that = this;
		window.addEventListener("gamepadconnected", function (event)  {
			that.gamepadActive = true;
			console.log("A gamepad connected:", event.gamepad);
		});
		window.addEventListener("gamepaddisconnected", function (event) {
			that.gamepadActive = false;
			console.log("A gamepad disconnected:", event.gamepad);
		});
		document.addEventListener("keydown", function (e) {
        keysPressed[e.keyCode] = 1;
        if (!firstKey) { firstKey = e.keyCode; }
		});
		document.addEventListener("keyup", function (e) {
			keysPressed[e.keyCode] = 0;
			if (firstKey === e.keyCode) { firstKey = null; }
			if (player) { player.gotoAndStop(player.baseAsset + "idle"); }
		});

		this.stage.addEventListener("stagemouseup", function (evt) {
			that.stagePress = false;
			that.stageMouse = null;
			that.stageFirst = null;
			if (player) { player.gotoAndStop(player.baseAsset + "idle"); }
			if (virtualJoystickActive) that.virtualJoystickHide();
		});
		this.stage.addEventListener("stagemousedown", function (evt) {
			requestFullScreen();
			that.stagePress = true;
			that.stageMouse = {x:evt.stageX,y:evt.stageY};
			that.stageFirst = {x:evt.stageX,y:evt.stageY};
			if (virtualJoystickActive) that.virtualJoystickShow(evt.stageX, evt.stageY);
		});
		this.stage.addEventListener("stagemousemove", function(evt) {
			if (that.stagePress) that.stageMouse = {x:evt.stageX, y:evt.stageY};
			if (virtualJoystickActive) that.virtualJoystickUpdate(evt.stageX, evt.stageY);
		});
		
		this.addEventsMenuSettings();

		this.MESSAGE.button_exit_message.addEventListener("click", this.clickFunction);
		this.TOOLBAR.player_bg.addEventListener("click", this.clickFunction);
		
		this.button_search.visible = searchActive;
		this.button_exit.visible = exitActive;
		this.button_share.visible = shareActive;
		this.button_exit.addEventListener("click", this.clickFunction);
		this.button_search.addEventListener("click", this.clickFunction);
		this.button_share.addEventListener("click", this.clickFunction);
	};
	//---
	this.localizeLabels = function() {
		this.testo_label.text = " ";
		this.testo_value.text = " ";
		var topoMargin = 2;
		var testi = that.mapAdditionalData["LABELS"][that.lang];
		for (var k = 0; k< that.mapAdditionalData["KEYS"].length; k++) {
			var key = that.mapAdditionalData["KEYS"][k];
			if (that.toponomastica[key]) {
				that.toponomastica[key].text = testi[key];
				var bb = that.toponomastica[key].getBounds();
				that.toponomastica[key].cache(bb.x-topoMargin, bb.y-topoMargin, bb.width+(2*topoMargin), bb.height+(2*topoMargin));
			}
		}
		this.TOOLBAR.player_name.text = that.mapAdditionalData["TEXTS"][that.lang]["TU"];
		this.TOOLBAR.toolbar_location.text = that.mapAdditionalData["TEXTS"][that.lang]["STARTAREA"];
		this.TOOLBAR.toolbar_punti.text = 0;
	};
	this.showArea = function(kind, key) {
		var areas = that.layerz[that.layerz.length-4];
		var vetrinas = that.layerz[that.layerz.length-3];
		var modules = that.layerz[that.layerz.length-2];
		var over = null;
		this.hideArea();
		var eccezionModules = false;
		if (kind.toUpperCase() == "AREAS") {
			over = areas;
		};
		if (kind.toUpperCase() == "VETRINAS") {
			over = vetrinas;
		};
		if (kind.toUpperCase() == "MODULES") {
			over = modules;
			eccezionModules = true;
		}
		if (over) {
			var gotGoal = false;
            for (var i = 0; i < over.numChildren; i++) {
                var item = over.getChildAt(i);
				if (item.name == key) {
					if (!gotGoal) {
						gotGoal = true;
						that.enemyGoal = item;
					}
					if (eccezionModules) item.alpha = 0.5
					else item.visible = true
				}
				else {
					if (eccezionModules) item.alpha = 0.1
					else item.visible = false;
				}
            }
			if (over.cacheCanvas) over.updateCache();
        }
		
	};
	function cleanArea(cont, eccezionModules) {
		for (var i = 0; i < cont.numChildren; i++) {
			var item = cont.getChildAt(i);
				if (eccezionModules) item.alpha = 0.1
				else item.visible = false;
		}
		if (cont.cacheCanvas) cont.updateCache();
		
	};
	this.hideArea = function(){
		that.enemyGoal = null;
		var areas = that.layerz[that.layerz.length-4];
		var vetrinas = that.layerz[that.layerz.length-3];
		var modules = that.layerz[that.layerz.length-2];
		cleanArea(areas, false);
		cleanArea(vetrinas, false);
		cleanArea(modules, true);
	};
	//---------
	function addPlayer(kind) {
		player = new createjs.Sprite(that.spriteSheets[0],"player_"+kind+"_idle");
		if (!window.modelApp.isDesktop && window.config.mobileAssetScale) player.scaleX = player.scaleY = window.config.mobileAssetScale;
		player.scaleX *= charactersScale;
		player.scaleY *= charactersScale;
        player.name = "player";
        player.kind = kind;
        player.regX = bW;
        player.regY = bH+6;
		if (!window.modelApp.isDesktop && window.config.mobileAssetScale) player.regX = player.regX / window.config.mobileAssetScale;
		if (!window.modelApp.isDesktop && window.config.mobileAssetScale) player.regY = player.regY / window.config.mobileAssetScale;
		player.x = canvasW / 2;
        player.y = canvasH / 2;
        player.rotation = 0;
        player.rot = 0;
        player.speed = 8;
        player.height = 42*charactersScale;
        player.width = 30*charactersScale;
		player.heightU = 24*charactersScale;
		player.heightD = 18*charactersScale;
        player.widthL = 15*charactersScale;
        player.widthR = 15*charactersScale;
		player.baseAsset = "player_"+kind+"_";
		player.framerate = that.animSpritesFramerate;
		player.gotoAndStop(player.baseAsset + "idle");
        that.stage.mapContainer["player"].addChild(player);
		//posiziono all'ingresso
		player.x+= centroAddX;
		player.y+= centroAddY;
		moveLayers.x ( -centroAddX );
		moveLayers.y ( -centroAddY );
    };
    function addEnemy(x, y, kind) {
		var enemy = new createjs.Sprite(that.spriteSheets[0],"player_"+kind+"_idle");
		if (!window.modelApp.isDesktop && window.config.mobileAssetScale) enemy.scaleX = enemy.scaleY = window.config.mobileAssetScale;
		enemy.scaleX *= charactersScale;
		enemy.scaleY *= charactersScale;
        enemy.name = "enemy" + enemies.length;
		enemy.kind = kind;
		enemy.regX = bW;
        enemy.regY = bH+6;
		if (!window.modelApp.isDesktop && window.config.mobileAssetScale) enemy.regX = enemy.regX / window.config.mobileAssetScale;
		if (!window.modelApp.isDesktop && window.config.mobileAssetScale) enemy.regY = enemy.regY / window.config.mobileAssetScale;
        enemy.x = x * bW + (bW / 2);
        enemy.y = y * bH + (bH / 2);
        enemy.rotation = 0;
		enemy.rot = directions[Math.floor(Math.random() * directions.length)];
        enemy.speed = 1 + Math.floor(Math.random() * (3-0.00000001));
        enemy.height = 42*charactersScale;
        enemy.width = 30*charactersScale;
		enemy.heightU = 24*charactersScale;
		enemy.heightD = 18*charactersScale;
        enemy.widthL = 15*charactersScale;
        enemy.widthR = 15*charactersScale;
		enemy.baseAsset = "player_"+kind+"_";
		enemy.framerate = that.animSpritesFramerate;
        enemy.gotoAndStop(enemy.baseAsset + "idle");
		enemies.push(enemy);
		allEnemies.push(enemy);
        that.stage.mapContainer["enemies"].addChild(enemy);
    };
	function addGuru(x, y, kind, name) {
		var guruCont = new createjs.Container();
		guruCont.regX = bW;
        guruCont.regY = bH+6;
		if (!window.modelApp.isDesktop && window.config.mobileAssetScale) guruCont.regX = guruCont.regX / window.config.mobileAssetScale;
		if (!window.modelApp.isDesktop && window.config.mobileAssetScale) guruCont.regY = guruCont.regY / window.config.mobileAssetScale;
		guruCont.name = name;
		guruCont.kind = kind;
		guruCont.x = x * bW + (bW / 2);
        guruCont.y = y * bH + (bH / 2);
		guruCont.spoken = false;
		guruCont.cid = that.gurus.length+1;
		
		var guru = new createjs.Sprite(that.spriteSheets[0],"player_"+kind+"_idle");
		if (!window.modelApp.isDesktop && window.config.mobileAssetScale) guru.scaleX = guru.scaleY = window.config.mobileAssetScale;
		guru.name = name;
		guru.scaleX *= charactersScale;
		guru.scaleY *= charactersScale;
        guru.height = 42*charactersScale;
        guru.width = 30*charactersScale;
		guru.baseAsset = "player_"+kind+"_";
		guru.framerate = that.animSpritesFramerate;
		guru.gotoAndStop(guru.baseAsset + "explode")
		createjs.Tween.get(guru).wait( Math.floor(Math.random()*2000) ).call(function(e) {guru.gotoAndPlay(guru.baseAsset + "explode")});
		guruCont.addChild(guru)
		guruCont.guru = guru;
		
		var guru_button = new createjs.Sprite(that.spriteSheets[0],"guru_button");
		if (!window.modelApp.isDesktop && window.config.mobileAssetScale) guru_button.scaleX = guru_button.scaleY = window.config.mobileAssetScale;
		guru_button.scaleX *= charactersScale;
		guru_button.scaleY *= charactersScale;
		guru_button.framerate = that.animSpritesFramerate;
        guru_button.gotoAndPlay("guru_button");
		guru_button.name = name;
		guru_button.kind = kind;
		guru_button.x = 5;
        guru_button.y = -60;
        guru_button.visible = false;
		guruCont.addChild(guru_button)
		guruCont.button = guru_button;
		guru_button.addEventListener("click", that.clickFunctionGuru);
		that.gurus.push(guruCont);
		that.allGurus.push(guruCont);
        that.stage.mapContainer["enemies"].addChild(guruCont);
    };
	function addLeggio(x, y, kind, name) {
		var leggioCont = new createjs.Container();
		leggioCont.regX = 16;
        leggioCont.regY = 16;
		if (!window.modelApp.isDesktop && window.config.mobileAssetScale) leggioCont.regX = leggioCont.regX / window.config.mobileAssetScale;
		if (!window.modelApp.isDesktop && window.config.mobileAssetScale) leggioCont.regY = leggioCont.regY / window.config.mobileAssetScale;
		leggioCont.name = name;
		leggioCont.kind = kind;
		leggioCont.x = x * bW + (bW / 2);
        leggioCont.y = y * bH + (bH / 2);
		leggioCont.spoken = false;
		leggioCont.cid = that.leggios.length+1;
		
		var leggio = new createjs.Sprite(that.spriteSheets[0],"leggio");
		if (!window.modelApp.isDesktop && window.config.mobileAssetScale) leggio.scaleX = leggio.scaleY = window.config.mobileAssetScale;
		leggio.name = name;
        leggio.height = bW;
        leggio.width = bH;
		leggio.baseAsset = "leggio";
		leggio.framerate = that.animSpritesFramerate;
		leggio.gotoAndStop(leggio.baseAsset + "explode")
		createjs.Tween.get(leggio).wait( Math.floor(Math.random()*2000) ).call(function(e) {leggio.gotoAndPlay(leggio.baseAsset + "explode")});
		leggioCont.addChild(leggio)
		leggioCont.leggio = leggio;
		
		var leggio_button = new createjs.Sprite(that.spriteSheets[0],"leggio_button");
		if (!window.modelApp.isDesktop && window.config.mobileAssetScale) leggio_button.scaleX = leggio_button.scaleY = window.config.mobileAssetScale;
		leggio_button.framerate = that.animSpritesFramerate;
        leggio_button.gotoAndPlay("leggio_button");
		leggio_button.name = name;
		leggio_button.kind = kind;
		leggio_button.x = 0;
        leggio_button.y = -65;
        leggio_button.visible = false;
		leggioCont.addChild(leggio_button)
		leggioCont.button = leggio_button;
		leggio_button.addEventListener("click", that.clickFunctionLeggio);
		that.leggios.push(leggioCont);
		that.allLeggios.push(leggioCont);
        that.stage.mapContainer["enemies"].addChild(leggioCont);
    };
	function addToponomastica(x,y,key) {
		var stileToponomastica = {
			"font": "15px 'Minimal3x5",
			"style": {
				"strokeStyle": {
					"lineWidth": 1,
					"colorStops": [
						[
							0,
							"#000000"
						],
						[
							1,
							"#000000"
						]
					]
				},
				"fillStyle": {
					"colorStops": [
						[
							0,
							"#ffffff"
						],
						[
							0.75,
							"#ffffff"
						],
						[
							1,
							"#ffffff"
						]
					]
				}
			},
			"color": "#f5ba00",
			"shadow": {
				"x": 0,
				"y": 0,
				"blur": 2,
				"color": "#000000"
			}
		};
		var TOPO = new GradientText("TOPOS", stileToponomastica.font, stileToponomastica.color, stileToponomastica.style);
		TOPO.iniText = "TOPOS";
		TOPO.name = key;
		TOPO.textBaseline = "alphabetic";
		TOPO.shadow = genShadow(stileToponomastica);
		TOPO.lineHeight = 12;
		TOPO.x = x * bW;
		TOPO.y = y * bH;
		TOPO.cid = that.stage.mapContainer.topo.numChildren;
		TOPO.mouseEnabled = false;
		that.toponomastica.addChild(TOPO);
		that.toponomastica[key] = TOPO;
	};
	//---------
	function detectKeys() {
		var pressed = false;
        if (keysPressed[38] === 1) { // up
            moveChar(0,player, 0, -1);
			pressed = true;
        }
        if (keysPressed[40] === 1) { // down
            moveChar(0,player, 0, 1);
			pressed = true;
        }
        if (keysPressed[37] === 1) { // left
            moveChar(0,player, -1, 0);
			pressed = true;
        }
        if (keysPressed[39] === 1) { // right
            moveChar(0,player, 1, 0);
			pressed = true;
        }
		
        // firstKey marks the dominant key which forces rotation that direction
        if (firstKey) {
            switch (firstKey) {
            case 38:
                player.rot = 270;
                break;
            case 40:
                player.rot = 90;
                break;
            case 37:
                player.rot = 180;
                break;
            case 39:
                player.rot = 0;
                break;
            }
        }
		if (!pressed) {
			if (that.stageMouse) {
				pressed = true;
				var pX = that.stageFirst.x //canvasW / 2;
				var pY = that.stageFirst.y //canvasH / 2;
				/*
				if (window.innerWidth < window.innerHeight) {
					pY = canvasW / 2;
					pX = canvasH / 2;
				}*/
				var goUp = false;
				var goDown = false;
				var goLeft = false;
				var goRight = false;
				if (that.stageMouse.x > pX + (player.width)) goRight = true;
				if (that.stageMouse.x < pX - (player.width)) goLeft = true;
				if (that.stageMouse.y > pY + (player.height)) goDown = true;
				if (that.stageMouse.y < pY - (player.height)) goUp = true;
				if (goUp) { // up
					player.rot == 270;
					moveChar(1,player, 0, -1);
				}
				if (goDown) { // down
					player.rot == 90
					moveChar(1,player, 0, 1);
				}
				if (goLeft) { // left
					player.rot == 180
					moveChar(1,player, -1, 0);
				}
				if (goRight) { // right
					player.rot == 0
					moveChar(1,player, 1, 0);
				}
			}
		} 
		if (pressed) {
			if (player.rot == 270) { // up
				if (player.currentAnimation !== player.baseAsset + "up") { player.gotoAndPlay(player.baseAsset + "up"); }
			}
			if (player.rot == 90) { // down
				if (player.currentAnimation !== player.baseAsset + "down") { player.gotoAndPlay(player.baseAsset + "down"); }
			}
			if (player.rot == 180) { // left
				if (player.currentAnimation !== player.baseAsset + "left") { player.gotoAndPlay(player.baseAsset + "left"); }
			}
			if (player.rot == 0) { // right
				if (player.currentAnimation !== player.baseAsset + "right") { player.gotoAndPlay(player.baseAsset + "right"); }
			}
		} else {
			player.gotoAndStop(player.baseAsset + "idle");
		}
    };
	function detectGamepadsMovement() {
		var ret = false;
		if (useGamepadsInput && that.gamepadActive) {
			that.gamepads = navigator.getGamepads();
			//console.log(that.gamepads)
			/*
			[{
					axes: [0.01, 0.01, 0.02, 0.04],
					buttons: [
						{ pressed: true, value: 1 },
						{ pressed: false, value: 0 },
						{ pressed: false, value: 0 },
						{ pressed: false, value: 0 },
						[...]
					],
					connected: true,
					id: "Xbox 360 Controller (XInput STANDARD GAMEPAD)",
					index: 0,
					mapping: "standard",
					timestamp: 177550
				},
				null,
				null,
				null]
			*/
			var gamepad = that.gamepads[0];
			var sogliaMov =  gamepadMapping["thresholds"]["axes"];
			var dirX = 0;
			var dirY = 0;
			if (gamepad.axes[1] <- sogliaMov) {
				player.rot = 270;
				if (player.currentAnimation !== player.baseAsset + "up") { player.gotoAndPlay(player.baseAsset + "up"); }
				dirY = -1;
				ret = true;
			} else  if (gamepad.axes[1] > sogliaMov) {
				player.rot = 90;
				if (player.currentAnimation !== player.baseAsset + "down") { player.gotoAndPlay(player.baseAsset + "down"); }
				dirY = 1;
				ret = true;
			}
			if (gamepad.axes[0] < -sogliaMov) {
				if (!ret) {
					if (player.currentAnimation !== player.baseAsset + "left") { player.gotoAndPlay(player.baseAsset + "left"); }
					player.rot = 180;
				}
				dirX = -1;
				ret = true;
			} else  if (gamepad.axes[0] > sogliaMov) {
				if (!ret) {
					player.rot = 0;
					if (player.currentAnimation !== player.baseAsset + "right") { player.gotoAndPlay(player.baseAsset + "right"); }
				}
				dirX = 1;
				ret = true;
			}
			if (ret) {
				moveChar(0, player, dirX,  0);
				moveChar(0, player, 0,  dirY);
			}
		}
		return ret;
    };
	function detectGamepadsButtons() {
		if (useGamepadsInput && that.gamepadActive) {
			that.gamepads = navigator.getGamepads();
			/*
			[{
					axes: [0.01, 0.01, 0.02, 0.04],
					buttons: [
						{ pressed: true, value: 1 },
						{ pressed: false, value: 0 },
						{ pressed: false, value: 0 },
						{ pressed: false, value: 0 },
						[...]
					],
					connected: true,
					id: "Xbox 360 Controller (XInput STANDARD GAMEPAD)",
					index: 0,
					mapping: "standard",
					timestamp: 177550
				},
				null,
				null,
				null]
			*/
			var gamepad = that.gamepads[0];
			var sogliaButtons = gamepadMapping["thresholds"]["buttons"];
			if (gamepad.buttons[gamepadMapping["buttons"]["A"]].pressed == true) {
				var doSomething = false;
				var e;
				for (e = 0; e < that.gurus.length; e++) {
					if ( !doSomething && that.gurus[e].button.visible == true) {
						//click
						that.gurus[e].button.dispatchEvent("click");
						doSomething = true;
						break;
					}
				}
				if (!doSomething) {
					for (e = 0; e < that.leggios.length; e++) {
						if ( !doSomething && that.leggios[e].button.visible == true) {
							//click
							that.leggios[e].button.dispatchEvent("click");
							doSomething = true;
							break;
						}
					}
				}
			}
			if (gamepad.buttons[gamepadMapping["buttons"]["B"]].pressed == true) {
				if (that.MESSAGE.visible == true && that.MESSAGE.button_exit_message.visible == true) that.closeMessaggio();
			}
			if (gamepad.buttons[gamepadMapping["buttons"]["MENU"]].pressed == true) {
				if (that.messageContainer.visible && that.messageContainer.alpha == 1) that.messageContainer.dispatchEvent("click");
			}


		}
	};
	//---
	function checkCorners(char, dirx, diry) {
        var formulaA, formulaB, formulaC, formulaD;
        if (dirx === 0) {
            formulaC = Math.floor((char.x - char.widthL) / bW);
            formulaD = Math.floor((char.x + char.widthR) / bW);
            formulaE = Math.floor((char.x) / bW);
            if (diry === -1) { // up
                formulaA = Math.floor(((char.y - char.heightU) + (char.speed * diry)) / bH);
                char.topLeft = mapTiles["b-" + formulaA + "-" + formulaC];
				char.topTop = mapTiles["b-" + formulaA + "-" + formulaE];
                char.topRight = mapTiles["b-" + formulaA + "-" + formulaD];
                if ( char.topLeft.walkable && char.topRight.walkable && char.topTop.walkable ) {
                    return true;
                }
            } else if (diry === 1) { // down
                formulaB = Math.floor(((char.y + char.heightD) + (char.speed * diry)) / bH);
                char.bottomLeft = mapTiles["b-" + formulaB + "-" + formulaC];
                char.bottomBottom = mapTiles["b-" + formulaB + "-" + formulaE];
                char.bottomRight = mapTiles["b-" + formulaB + "-" + formulaD];
                if (char.bottomLeft.walkable && char.bottomRight.walkable && char.bottomBottom.walkable) {
                    return true;
                }
            }
        }
        if (diry === 0) {
            formulaC = Math.floor((char.y - char.heightU) / bH);
            formulaD = Math.floor((char.y + char.heightD) / bH);
            formulaE = Math.floor((char.y) / bH);
            if (dirx === -1) { // left
                formulaA = Math.floor(((char.x - char.widthL) + (char.speed * dirx)) / bW);
                char.topLeft = mapTiles["b-" + formulaC + "-" + formulaA];
				char.leftLeft = mapTiles["b-" + formulaE + "-" + formulaA];
                char.bottomLeft = mapTiles["b-" + formulaD + "-" + formulaA];
                if ((char.topLeft.walkable && char.bottomLeft.walkable)||char.leftLeft.walkable) {
                    return true;
                }
            } else if (dirx === 1) { // right
                formulaB = Math.floor(((char.x + char.widthR) + (char.speed * dirx)) / bW);
                char.topRight = mapTiles["b-" + formulaC + "-" + formulaB];
                char.rightRight = mapTiles["b-" + formulaE + "-" + formulaB];
                char.bottomRight = mapTiles["b-" + formulaD + "-" + formulaB];
                if ((char.topRight.walkable && char.bottomRight.walkable)|| char.rightRight.walkable) {
                    return true;
                }
            }
        }
        
    };
	var moveLayers = {
			x: function (v) {
				for (var l=0; l<that.layerz.length;l++) {
					var layo = that.layerz[l];
					layo.x+=v;
				}
			},
			y: function (v) {
				for (var l=0; l<that.layerz.length;l++) {
					var layo = that.layerz[l];
					layo.y+=v;
				}
			}
		};
    function moveChar(commander, char, dirx, diry) {
		
		var useSpeed = char.speed;
		if (commander == 1) useSpeed *=0.75;
		
		
		/*if (char.name === "player") {
			var formulaY = Math.floor((char.y)  / bH);
			var formulaX = Math.floor((char.x)  / bW);
			console.log("MOVE",char.name, dirx, diry, "from",formulaX, formulaY, mapTiles["b-" + formulaY + "-" + formulaX].walkable , char.x, char.y);
		}*/
		
        if (dirx === 0) {
            if (diry === -1 && checkCorners(char, dirx, diry)) { // up
                if (char.name === "player") {
                    moveLayers.y ( -diry * useSpeed);
                }
                char.y += diry * useSpeed;
                char.rot = 270;
            } else if (diry === 1 && checkCorners(char, dirx, diry)) { // down
                if (char.name === "player") {
                    moveLayers.y ( -diry * useSpeed );
                }
                char.y += diry * useSpeed;
                char.rot = 90;
            }
        }
        if (diry === 0) {
            if (dirx === -1 && checkCorners(char, dirx, diry)) { // left
                if (char.name === "player") {
                    moveLayers.x ( -dirx * useSpeed );
                }
                char.x += dirx * useSpeed;
                char.rot = 180;
            } else if (dirx === 1 && checkCorners(char, dirx, diry)) { // right
                if (char.name === "player") {
                    moveLayers.x ( -dirx * useSpeed );
                }
                char.x += dirx * useSpeed;
                char.rot = 0;
            }
        }
		
        
    };
	//---------
	function enemyBrain() {
        var e, distToPlayer, angleToPlayer;
		var goal = player;
		var dist = player.width;
		var nose = 3;
        if (that.enemyGoal != null) {
			goal = that.enemyGoal;
			nose = 2;
			dist = bW;
		//console.log(that.enemyGoal.name);
		}
        function walkForward() {
            switch (enemies[e].rot) {
            case 0:
                if (checkCorners(enemies[e], 1, 0)) {
                    moveChar(0,enemies[e], 1, 0);
                } else {
                    enemies[e].rot = directions[Math.floor(Math.random() * directions.length)];
                }
                break;
            case 90:
                if (checkCorners(enemies[e], 0, 1)) {
                    moveChar(0,enemies[e], 0, 1);
                } else {
                    enemies[e].rot = directions[Math.floor(Math.random() * directions.length)];
                }
                break;
            case 180:
                if (checkCorners(enemies[e], -1, 0)) {
                    moveChar(0,enemies[e], -1, 0);
                } else {
                    enemies[e].rot = directions[Math.floor(Math.random() * directions.length)];

                }
                break;
            case 270:
                if (checkCorners(enemies[e], 0, -1)) {
                    moveChar(0,enemies[e], 0, -1);
                } else {
                    enemies[e].rot = directions[Math.floor(Math.random() * directions.length)];
                }
                break;
            default:
                enemies[e].rot = 0;
            }
			if (enemies[e].rot == 270) { // up
				if (enemies[e].currentAnimation !== enemies[e].baseAsset + "up") { enemies[e].gotoAndPlay(enemies[e].baseAsset + "up"); }
			}
			if (enemies[e].rot == 90) { // down
				if (enemies[e].currentAnimation !== enemies[e].baseAsset + "down") { enemies[e].gotoAndPlay(enemies[e].baseAsset + "down"); }
			}
			if (enemies[e].rot == 180) { // left
				if (enemies[e].currentAnimation !== enemies[e].baseAsset + "left") { enemies[e].gotoAndPlay(enemies[e].baseAsset + "left"); }
			}
			if (enemies[e].rot == 0) { // right
				if (enemies[e].currentAnimation !== enemies[e].baseAsset + "right") { enemies[e].gotoAndPlay(enemies[e].baseAsset + "right"); }
			}
        }
        
        for (e = 0; e < enemies.length; e++) {
            distToPlayer = pTheorem({x: enemies[e].x, y: enemies[e].y}, {x: goal.x, y: goal.y});
            angleToPlayer = Math.floor(getAngle({x: enemies[e].x, y: enemies[e].y}, {x: goal.x, y: goal.y}));
            if (distToPlayer < dist * nose) {
                if (angleToPlayer > 315 || angleToPlayer < 45) {
                    enemies[e].rot = 0;
                }
                if (angleToPlayer > 45 && angleToPlayer < 135) {
                    enemies[e].rot = 90;
                }
                if (angleToPlayer > 135 && angleToPlayer < 225) {
                    enemies[e].rot = 180;
                }
                if (angleToPlayer > 225 && angleToPlayer < 315) {
                    enemies[e].rot = 270;
                }
                if (distToPlayer > dist) {
                    walkForward();
                } else {
                    enemies[e].gotoAndStop(enemies[e].baseAsset + "idle");
                }
            } else {
                walkForward();
            }
            
        }
        
    };
	//---
	function guruBrain() {
        var e, distToPlayer, angleToPlayer;
		var goal = player;
		var dist = player.width;
		var nose = 5;
        
        for (e = 0; e < that.gurus.length; e++) {
            distToPlayer = pTheorem({x: that.gurus[e].x, y: that.gurus[e].y}, {x: goal.x, y: goal.y});
            if (distToPlayer < dist * nose) {
                if (! that.gurus[e].spoken) {
					that.gurus[e].button.visible = true;
				} else {
					that.gurus[e].button.visible = false;
				}
            } else {
				that.gurus[e].button.visible = false;
			}
        }
    };
	function leggioBrain() {
        var e, distToPlayer, angleToPlayer;
		var goal = player;
		var dist = player.width;
		var nose = 2;
        
        for (e = 0; e < that.leggios.length; e++) {
            distToPlayer = pTheorem({x: that.leggios[e].x, y: that.leggios[e].y}, {x: goal.x, y: goal.y});
            if (distToPlayer < dist * nose) {
                if (! that.leggios[e].spoken) {
					that.leggios[e].button.visible = true;
				} else {
					that.leggios[e].button.visible = false;
				}
            } else {
				that.leggios[e].button.visible = false;
			}
        }
    };
	//---------
	function lightTiles() {
        var item, distToPlayer, alpha;
		var board = that.layerz[0];
		var over = that.layerz[that.layerz.length-5];//** a -1 c'e' topo, -2 -3 -4 gli indicatori area vetrina moduli
        if (board) {
            for (item = 0; item < board.numChildren; item++) {
                distToPlayer = pTheorem({x: board.getChildAt(item).x, y: board.getChildAt(item).y}, {x: player.x, y: player.y});
				alpha = 0;
				alpha = 1000 / distToPlayer - 0.6;
				if (alpha < 0) alpha = 0;
				if (alpha > 1) alpha = 1;
                board.getChildAt(item).alpha = alpha;
            }
			if (board.cacheCanvas) board.updateCache();
        }
		if (over) {
            for (item = 0; item < over.numChildren; item++) {
                distToPlayer = pTheorem({x: over.getChildAt(item).x, y: over.getChildAt(item).y}, {x: player.x, y: player.y});
				alpha = 0;
				alpha = 1000 / distToPlayer - 0.6;
				if (alpha < 0) alpha = 0;
				if (alpha > 1) alpha = 1;
                over.getChildAt(item).alpha = alpha;
            }
			if (over.cacheCanvas) over.updateCache();
        }
    };
	//---------
	this.getBlock = function(x,y,l) {
		//usage console.log(that.getBlock(10,10,5));
		var layo = 0;
		if (l && l > -1) layo = l;
		var val = that.mapdata.layers[l].matr[y][x];
		if (that.collisionLayerId && layo == that.collisionLayerId) {
			if (val>0) val = 1;
			if (val == collisionDoorId) val = 2;
		}
		return val;
	};

	//---------
	this.idiotClicks = 0;
	this.clickFunction = function (evt) {
		var itemClicked = evt.currentTarget;
		if (that.state == that.BonusStateEnum.READY) if (that.audioInplay) that.bgmLoop = window.timeliner.fx.playBonusExternalSound("click", that.audioGameId);
		switch (itemClicked.name) {
			/*
			button_exit //** TODO bottone exit search e share
			button_search
			button_share
			*/
			case "button_exit_message":
				if (!that.state == that.BonusStateEnum.READY) if (that.audioInplay) that.bgmLoop = window.timeliner.fx.playBonusExternalSound("click", that.audioGameId);
				that.closeMessaggio();
			break;
			case "player_bg":
				if (that.state == that.BonusStateEnum.READY) {
					that.doMessaggio( that.mapAdditionalData["TEXTS"][that.lang]["IDIOT_TITLES"][that.idiotClicks], that.mapAdditionalData["TEXTS"][that.lang]["IDIOT_TEXTS"][that.idiotClicks] );
					that.idiotClicks++;
					if (that.idiotClicks>= that.mapAdditionalData["TEXTS"][that.lang]["IDIOT_TITLES"].length) that.idiotClicks = 0;
				}
			break;
		}
	};
	//--
	this.clickFunctionModules = function (evt) {
		var itemClicked = evt.currentTarget;
		if (that.state == that.BonusStateEnum.READY) if (that.audioInplay) that.bgmLoop = window.timeliner.fx.playBonusExternalSound("click", that.audioGameId);
		var modulo = itemClicked.name;
		var vetrina = "VETRINA" + modulo[1] + (Number(modulo[2]) >= 0? modulo[2]:"" );
		//console.log("CLICK MODULO", modulo, vetrina );
	};
	//--
	this.clickFunctionGuru = function (evt) {
		requestFullScreen();
		var itemClicked = evt.currentTarget;
		if (that.state == that.BonusStateEnum.READY) {
			if (that.audioInplay) that.bgmLoop = window.timeliner.fx.playBonusExternalSound("choose", that.audioGameId);
			//console.log("CLICK GURU", itemClicked.parent.name);
			if (!itemClicked.parent.spoken) {
				var titoli = that.mapAdditionalData["TEXTS"][that.lang]["GURU_"+itemClicked.parent.name.toUpperCase()+"_TITLES"];
				var testi = that.mapAdditionalData["TEXTS"][that.lang]["GURU_"+itemClicked.parent.name.toUpperCase()+"_TEXTS"];
				var completateTutte = false;
				if (that.visitedGurus.indexOf(itemClicked.parent.name) < 0) {
					that.visitedGurus.push(itemClicked.parent.name);
					that.playerPoints += that.mapAdditionalData["POINTS"]["GURUS"];
					that.updateHud();
					that.checkEndGame();
					if (that.visitedGurus.length >= that.mapAdditionalData["COUNTS"]["GURUS"]) completateTutte = true;
				}
				if (completateTutte) {
					if (that.audioInplay) that.bgmLoop = window.timeliner.fx.playBonusExternalSound("win", that.audioGameId);
					//se fa sequenza questo messaggio va comunque alla fine
					//**that.doMessaggio(that.mapAdditionalData["TEXTS"][that.lang]["HAICOMPLETATO_TITLE"].replace("\n", " "), that.mapAdditionalData["TEXTS"][that.lang]["HAICOMPLETATO_TEXT"] + "\n" + that.mapAdditionalData["TEXTS"][that.lang]["GURU"], itemClicked.kind);
				}
				for (var t=titoli.length-1; t>-1; t--) {
					//fa tutta sequenza messaggi sparati con doMessaggio dall'ultimo al primo, e poi settare 
					that.doMessaggio(titoli[t], testi[t], itemClicked.kind);
				}
				if (titoli.length > 0) that.state = that.BonusStateEnum.SEQUENCE
			}
			itemClicked.parent.guru.gotoAndStop(itemClicked.parent.guru.baseAsset + "idle");
			itemClicked.visible = false;
			itemClicked.parent.spoken = true;
			createjs.Tween.get(itemClicked.parent).to({alpha: 0}, 250).to({alpha: 1}, 250).to({alpha: 0}, 200).to({alpha: 1}, 150).to({alpha: 0, visible:false}, 100);
			that.gurus.splice(that.gurus.indexOf(itemClicked.parent),1);
		}
	};
	this.clickFunctionLeggio = function (evt) {
		requestFullScreen();
		var itemClicked = evt.currentTarget;
		if (that.state == that.BonusStateEnum.READY) {
			if (that.audioInplay) that.bgmLoop = window.timeliner.fx.playBonusExternalSound("choose", that.audioGameId);
			//console.log("CLICK GURU", itemClicked.parent.name);
			if (!itemClicked.parent.spoken) {
				var titoli = that.mapAdditionalData["TEXTS"][that.lang]["LEGGIO_"+itemClicked.parent.name.toUpperCase()+"_TITLES"];
				var testi = that.mapAdditionalData["TEXTS"][that.lang]["LEGGIO_"+itemClicked.parent.name.toUpperCase()+"_TEXTS"];
				var completateTutte = false;
				if (that.visitedLeggios.indexOf(itemClicked.parent.name) < 0) {
					that.visitedLeggios.push(itemClicked.parent.name);
					that.playerPoints += that.mapAdditionalData["POINTS"]["LEGGIOS"];
					that.updateHud();
					that.checkEndGame();
					if (that.visitedLeggios.length >= that.mapAdditionalData["COUNTS"]["LEGGIOS"]) completateTutte = true;
				}
				if (completateTutte) {
					if (that.audioInplay) that.bgmLoop = window.timeliner.fx.playBonusExternalSound("win", that.audioGameId);
					//se fa sequenza questo messaggio va comunque alla fine
					//**that.doMessaggio(that.mapAdditionalData["TEXTS"][that.lang]["HAICOMPLETATO_TITLE"].replace("\n", " "), that.mapAdditionalData["TEXTS"][that.lang]["HAICOMPLETATO_TEXT"] + "\n" + that.mapAdditionalData["TEXTS"][that.lang]["LEGGIO"], itemClicked.kind);
				}
				for (var t=titoli.length-1; t>-1; t--) {
					//fa tutta sequenza messaggi sparati con doMessaggio dall'ultimo al primo, e poi settare 
					that.doMessaggio(titoli[t], testi[t], itemClicked.kind);
				}
				if (titoli.length > 0) that.state = that.BonusStateEnum.SEQUENCE
			}
			itemClicked.visible = false;
			itemClicked.parent.spoken = true;
			createjs.Tween.get(itemClicked.parent).to({alpha: 0}, 250).to({alpha: 1}, 250).to({alpha: 0}, 200).to({alpha: 1}, 150).to({alpha: 0, visible:false}, 100);
			that.leggios.splice(that.leggios.indexOf(itemClicked.parent),1);
		}
	};
	//---------
	this.gameEnded = false;
	this.checkEndGame = function () {
		if (!that.gameEnded) {
			var isToEnd = false;
			var ended = 0;
			if (that.visitedAreas.length >= that.mapAdditionalData["COUNTS"]["AREAS"]) ended++;
			if (that.visitedVetrinas.length >= that.mapAdditionalData["COUNTS"]["VETRINAS"]) ended++;
			if (that.visitedModules.length >= that.mapAdditionalData["COUNTS"]["MODULES"]) ended++;
			if (that.visitedGurus.length >= that.mapAdditionalData["COUNTS"]["GURUS"]) ended++;
			if (that.visitedLeggios.length >= that.mapAdditionalData["COUNTS"]["LEGGIOS"]) ended++;
			if (ended >= 5) isToEnd = true;
			if (isToEnd) {
				that.endGame();
			}
		}
	};
	this.endGame = function() {
		if (!that.gameEnded) {
			var outroMsg = that.mapAdditionalData["TEXTS"][that.lang]["GOODBYE"];
			that.outroText.text = outroMsg;
			that.verticalCentralAlignText(that.outroText, 360);
			if(outroMsg.length > 0)
			{
				that.game_logo.alpha = 0;
				createjs.Tween.get(that.game_logo).to({alpha: 0}, that.tempoFadePanel);
				
				that.messageContainer.alpha = 0;
				that.messageContainer.visible = true;
				that.outroText.visible = true;
				createjs.Tween.get(that.messageContainer).to({ alpha: 1 }, that.tempoFadePanel);
				setTimeout(function () {
				   that.closeMessageContainer({currentTarget: that.messageContainer});
				   //that.state = that.BonusHorusStateEnum.CONCLUDED;
					//that.callEndApi();
				}, that.tempoPostFadePanel);
				
			} else {
				//that.state = that.BonusHorusStateEnum.CONCLUDED;
				//that.callEndApi();
			}
			that.gameEnded = true;
			if (that.audioInplay) that.bgmLoop = window.timeliner.fx.playBonusExternalSound("end", that.audioGameId);
		}
		
	};

	//-------------------------
	function requestFullScreen() {
        if (window.modelApp.isDesktop || isFullScreen()) return;

        let elem = document.documentElement;
        let p = null;
        if (elem.requestFullscreen) {
           p = elem.requestFullscreen(Element.ALLOW_KEYBOARD_INPUT);
        } else if (elem.mozRequestFullScreen) { /* Firefox */
            p = elem.mozRequestFullScreen(Element.ALLOW_KEYBOARD_INPUT);
        } else if (elem.webkitRequestFullscreen) { /* Chrome, Safari and Opera */
            p = elem.webkitRequestFullscreen(Element.ALLOW_KEYBOARD_INPUT);
        } else if (elem.msRequestFullscreen) { /* IE/Edge */
            p = elem.msRequestFullscreen(Element.ALLOW_KEYBOARD_INPUT);
        }
        try {       
            if (p) {
                p.then(successFunction, failureFunction);                
            }
        } catch (e) {
        }
        function successFunction() {
        }
        function failureFunction(e) {
        }
        
    };
	function isFullScreen(){
        return (document.fullScreenElement && document.fullScreenElement !== null)
            || document.webkitIsFullScreen;
    };
	//-------------------------

};


