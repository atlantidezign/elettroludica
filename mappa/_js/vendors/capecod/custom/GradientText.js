"use strict";
//GradientText v1.0
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var GradientText = (function (_super) {
    __extends(GradientText, _super);
    function GradientText(text, font, color, style) {
        var _this = _super.call(this, text, font, color) || this;
        _this._drawTextLine = function (ctx, text, y) {
            var startX = 0;
            var height = this.getMeasuredLineHeight();
            if (this.style["strokeStyle"]) {
                var strokeGradient = ctx.createLinearGradient(0, y - height, 0, y);
                for (var _i = 0, _a = this.style["strokeStyle"]["colorStops"]; _i < _a.length; _i++) {
                    var csg = _a[_i];
                    strokeGradient.addColorStop(csg[0], csg[1]);
                }
                ctx.strokeStyle = strokeGradient;
                ctx.lineWidth = this.style["strokeStyle"]["lineWidth"];
                ctx.strokeText(text, startX, y, this.maxWidth || 0xFFFF);
            }
            if (this.style["fillStyle"]) {
                var fillGradient = ctx.createLinearGradient(0, y - height, 0, y);
                for (var _b = 0, _c = this.style["fillStyle"]["colorStops"]; _b < _c.length; _b++) {
                    var csf = _c[_b];
                    fillGradient.addColorStop(csf[0], csf[1]);
                }
                ctx.fillStyle = fillGradient;
                ctx.fillText(text, startX, y, this.maxWidth || 0xFFFF);
            }
        };
        _this.style = style;
        return _this;
    }
    return GradientText;
}(createjs.Text));
