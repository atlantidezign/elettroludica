var configTemplates = [
    {
		/*********************************************************/
		/**************          FIREWORKS          **************/
		/*********************************************************/
		
        name : "Fireworks",
        //spritesheet : array di spritesheets di createjs. random viene estratta per ogni particella quale spritesheet usare.
        //            :  ogni spritesheet puo' in oltre avere piu images, che vengono ciclate tutte nella singola particella.
		
		/************** spritesheet originale ALE **************/
        /*spriteSheet:[
            {
                images: ["/common/img/vfx/coin_anim_32px_stroked_Y.png", "/common/img/vfx/coin_anim_32px_stroked_W.png", "/common/img/vfx/coin_anim_32px_stroked_V.png",
                    "/common/img/vfx/coin_anim_32px_stroked_R.png", "/common/img/vfx/coin_anim_32px_stroked_O.png", "/common/img/vfx/coin_anim_32px_stroked_M.png",
                    "/common/img/vfx/coin_anim_32px_stroked_G.png", "/common/img/vfx/coin_anim_32px_stroked_B.png"],
                frames: {
                    regX: 16,
                    regY: 16,
                    height: 32,
                    width: 32
                }
            }
        ],*/
		
		/************** spritesheet LAVA gemme **************/
		spriteSheet: [{
            images: ["/common/img/vfx/gem_anim_40px_G.png"],
            frames: {
                    regX: 20,
                    regY: 20,
                    height: 40,
                    width: 40
			}
		},
        {
			images: ["/common/img/vfx/gem_anim_40px_R.png"],
			frames: {
                    regX: 20,
                    regY: 20,
                    height: 40,
                    width: 40
			}
		},
        {
			images: ["/common/img/vfx/gem_anim_40px_Y.png"],
			frames: {
                    regX: 20,
                    regY: 20,
                    height: 40,
                    width: 40
			}
		}],		
		
		/************** spritesheet LAVA banconote **************/
		/*spriteSheet:[
            {
                images: ["/common/img/vfx/notes_anim_40px.png"],
                frames: {
                    regX: 20,
                    regY: 20,
                    height: 40,
                    width: 40
                }
            }
        ],*/

		/************** spritesheet LAVA monete irlandesi **************/
		/*spriteSheet: [{
            images: ["/common/img/vfx/irishcoins_anim_40px_Yg.png"],
            frames: {
                    regX: 20,
                    regY: 20,
                    height: 40,
                    width: 40
			}
		},
        {
			images: ["/common/img/vfx/irishcoins_anim_40px_Gy.png"],
			frames: {
                    regX: 20,
                    regY: 20,
                    height: 40,
                    width: 40
			}
		}],		*/
		
		/************** spritesheet LAVA monete oro **************/
		/*spriteSheet:[
            {
                images: ["/common/img/vfx/goldcoins_anim_40px.png"],
                frames: {
                    regX: 20,
                    regY: 20,
                    height: 40,
                    width: 40
                }
            }
        ],*/
				
        //bg
        bgColor: "", //"rgba(0,0,0, 0.5)", //eventuale colore dello sfondo
        bgImage: "", //"/common/img/vfx/bg_test.png",  //"/common/img/vfx/bg_klondike.png", //eventuale immagine di fondo

        //particles
        particleStepNumber: 128, //quante particelle ad ogni emissione
        particleSpeed: 1, //velocità di base
        particleRotationStartMin:0, //angolo iniziale min in gradi
        particleRotationStartMax:360,  //angolo iniziale max in gradi
        particleVelocityMult: 10, //moltiplicatore velocità , random
        particleVelocityMultRandomizer: 0.5, //randomizzatore moltiplicatore velocità, da 0 a 1;
        particleVelocityDir: 1, //direzione: 1 +, 0 random, -1 -
        particleAlphaStart:1, //alpha iniziale - cambia durante loop
        particleScaleStart:1, //scala iniziale
        particleScaleStartVariation: 0.5, //scala iniziale variaizone random
        particleLifeStart:100, //vita particella

        //loop
        simulationSpeed: 32, //velocità simulazione, piu è alta piu è lenta
        alphaThreshold: 0.75, //0.5, //soglia vita da cui comincia alpha
        gravity: 0.2, //gravità
        verticalDecay: 0.9, //decadenza velocità verticale
        horizontalDecay:1, //decadenza velocità orizzontale
        horizontalAmplitude:2, //moltiplicatore  velocità orizzontale

        //emitters sequence
        startTime:500, //tempo iniziale
        stepTime: 1000, //tempo finale
        endTime: 5000, //tempo attesa finale
        steps:[ [{e:5},{e:2},{e:5},{e:10},{e:5},{e:1},{e:5}] ]//per ognuno, e: quanti emettitori
        //c: particelle per emettitore,
        // x, y: posizione,
        // s: velocità ,
        // r: rotazione,
        // in futuro tempi delay emissione?
    },
    {
		/*********************************************************/
		/**************          FOUNTAIN           **************/
		/*********************************************************/

        name : "Fountain",
		/************** spritesheet originale ALE **************/
        /*spriteSheet: [{
            images: ["/common/img/vfx/coin_anim_32px_stroked_Y.png"],
            frames: {
				regX: 16,
				regY: 16,
				height: 32,
				width: 32
			}
		},
        {
			images: ["/common/img/vfx/coin_anim_32px_stroked_O.png"],
			frames: {
				regX: 16,
				regY: 16,
				height: 32,
				width: 32
			}
		}],*/

		/************** spritesheet LAVA gemme **************/
		/*spriteSheet: [{
            images: ["/common/img/vfx/gem_anim_40px_G.png"],
            frames: {
                    regX: 20,
                    regY: 20,
                    height: 40,
                    width: 40
			}
		},
        {
			images: ["/common/img/vfx/gem_anim_40px_R.png"],
			frames: {
                    regX: 20,
                    regY: 20,
                    height: 40,
                    width: 40
			}
		},
        {
			images: ["/common/img/vfx/gem_anim_40px_Y.png"],
			frames: {
                    regX: 20,
                    regY: 20,
                    height: 40,
                    width: 40
			}
		}],*/

		/************** spritesheet LAVA banconote **************/
		/*spriteSheet:[
            {
                images: ["/common/img/vfx/notes_anim_40px.png"],
                frames: {
                    regX: 20,
                    regY: 20,
                    height: 40,
                    width: 40
                }
            }
        ],*/	
		
		/************** spritesheet LAVA monete irlandesi **************/
		spriteSheet: [{
            images: ["/common/img/vfx/irishcoins_anim_40px_Yg.png"],
            frames: {
                    regX: 20,
                    regY: 20,
                    height: 40,
                    width: 40
			}
		},
        {
			images: ["/common/img/vfx/irishcoins_anim_40px_Gy.png"],
			frames: {
                    regX: 20,
                    regY: 20,
                    height: 40,
                    width: 40
			}
		}],		

		/************** spritesheet LAVA monete oro **************/
		/*spriteSheet:[
            {
                images: ["/common/img/vfx/goldcoins_anim_40px.png"],
                frames: {
                    regX: 20,
                    regY: 20,
                    height: 40,
                    width: 40
                }
            }
        ],*/

        //bg
        bgColor: "", //"rgba(0,0,0, 0.5)", //eventuale colore dello sfondo
        bgImage: "", //"/common/img/vfx/bg_klondike.png", //eventuale immagine di fondo

        //particles
        particleStepNumber: 64, //quante particelle ad ogni emissione
        particleSpeed: 1, //velocità di base
        particleRotationStartMin: 270-45, //angolo iniziale min in gradi
        particleRotationStartMax: 270+45,  //angolo iniziale max in gradi
        particleVelocityMult: 10, //moltiplicatore velocità , random
        particleVelocityMultRandomizer: 0.5, //randomizzatore moltiplicatore velocità, da 0 a 1;
        particleVelocityDir: 1, //direzione: 1 +, 0 random, -1 -
        particleAlphaStart:1, //alpha iniziale - cambia durante loop
        particleScaleStart:1, //scala iniziale
        particleScaleStartVariation: 0.5, //scala iniziale variaizone random
        particleLifeStart:200, //vita particella

        //loop
        simulationSpeed: 32, //velocità simulazione, piu è alta piu è lenta
        alphaThreshold: 0.75, //0.5, //soglia vita da cui comincia alpha
        gravity: 0.2, //gravità
        verticalDecay: 0.9, //decadenza velocità verticale
        horizontalDecay:1, //decadenza velocità orizzontale
        horizontalAmplitude:2, //moltiplicatore  velocità orizzontale

        //emitters sequence
        startTime:500, //tempo iniziale
        stepTime: 200, //tempo finale
        endTime: 5000, //tempo attesa finale
		//steps: [[{e:2, c:10, x:640, y:400}], [{e:5, c:10, x:640, y:400}], [{e:5, c:50, x:640, y:400}]]
        steps:[ [{e:5, c:10, x:640, y:400},{e:2, c:10, x:640, y:400}, {e:2, c:10, x:640, y:400}, {e:2, c:10, x:640, y:400}, {e:2, c:10, x:640, y:400}, {e:2, c:10, x:640, y:400}, {e:2, c:10, x:640, y:400}, {e:2, c:10, x:640, y:400}, {e:2, c:10, x:640, y:400}, {e:2, c:10, x:640, y:400}, {e:2, c:10, x:640, y:400}, {e:2, c:10, x:640, y:400}, {e:2, c:10, x:640, y:400}, {e:2, c:10, x:640, y:400}, {e:2, c:10, x:640, y:400}, {e:2, c:10, x:640, y:400}, {e:2, c:10, x:640, y:400},{e:5, c:10, x:640, y:400},{e:10, c:10, x:640, y:400},{e:5, c:10, x:640, y:400},{e:1,c:10, x:640, y:400},{e:5, c:10, x:640, y:400}]]
    }


];