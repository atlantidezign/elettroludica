"use strict";

function _instanceof(left, right) {
  if (
    right != null &&
    typeof Symbol !== "undefined" &&
    right[Symbol.hasInstance]
  ) {
    return right[Symbol.hasInstance](left);
  } else {
    return left instanceof right;
  }
}

function _classCallCheck(instance, Constructor) {
  if (!_instanceof(instance, Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}

function _defineProperties(target, props) {
  for (var i = 0; i < props.length; i++) {
    var descriptor = props[i];
    descriptor.enumerable = descriptor.enumerable || false;
    descriptor.configurable = true;
    if ("value" in descriptor) descriptor.writable = true;
    Object.defineProperty(target, descriptor.key, descriptor);
  }
}

function _createClass(Constructor, protoProps, staticProps) {
  if (protoProps) _defineProperties(Constructor.prototype, protoProps);
  if (staticProps) _defineProperties(Constructor, staticProps);
  return Constructor;
}

function _defineProperty(obj, key, value) {
  if (key in obj) {
    Object.defineProperty(obj, key, {
      value: value,
      enumerable: true,
      configurable: true,
      writable: true
    });
  } else {
    obj[key] = value;
  }
  return obj;
}

var Sparkles =
  /*#__PURE__*/
  (function() {
    function Sparkles() {
      _classCallCheck(this, Sparkles);

      _defineProperty(this, "version", "v0.4");

      _defineProperty(this, "sprites", void 0);

      _defineProperty(this, "bgFader", void 0);

      _defineProperty(this, "bgImage", void 0);

      _defineProperty(this, "vfxContainer", void 0);

      _defineProperty(this, "spkls", void 0);

      _defineProperty(this, "tickActive", false);

      _defineProperty(this, "steps", void 0);

      _defineProperty(this, "stepCurrent", 0);

      _defineProperty(this, "callback", void 0);

      _defineProperty(this, "libWidth", 1280);

      _defineProperty(this, "libHeight", 720);

      _defineProperty(this, "config", void 0);

      _defineProperty(this, "options", void 0);

      _defineProperty(this, "spriteSheet", void 0);

      _defineProperty(this, "timings", void 0);

      _defineProperty(this, "bgColor", void 0);
	  
	  _defineProperty(this, "rateo", void 0);
    }

    _createClass(Sparkles, [
      {
        key: "init",
        //constructor
        value: function init(vfxContainer, config, options, callback) {
          var libWidth =
            arguments.length > 4 && arguments[4] !== undefined
              ? arguments[4]
              : 1280;
          var libHeight =
            arguments.length > 5 && arguments[5] !== undefined
              ? arguments[5]
              : 720;
          console.log("SPARKLES INIT", libWidth, libHeight);
          this.libWidth = libWidth;
          this.libHeight = libHeight;
          this.vfxContainer = vfxContainer;
          this.callback = callback;
          this.config = config;
          this.options = options;
          if (this.config.particleVelocityMultRandomizer > 1)
            this.config.particleVelocityMultRandomizer = 1;
          if (this.config.particleVelocityMultRandomizer < 0)
            this.config.particleVelocityMultRandomizer = 0;
          if (config.spriteSheet) this.spriteSheet = config.spriteSheet;
          var generatedSS = this.generateSpriteSheetsFromOptions();
          if (generatedSS.length > 0) this.spriteSheet = generatedSS;
          this.timings = this.options.timings;
          this.sprites = [];
		  this.rateo = 1;

          for (var ss = 0; ss < this.spriteSheet.length; ss++) {
            this.sprites.push(
              new createjs.Sprite(
                new createjs.SpriteSheet(this.spriteSheet[ss])
              )
            );
          }

          this.bgColor = config.bgColor;
          if (options.bgColor && options.bgColor != "")
            this.bgColor = options.bgColor;
          this.bgImage = config.bgImage;
          if (options.bgImage && options.bgImage != "")
            this.bgImage = options.bgImage;
		
			if (window.modelApp && window.modelApp.baseStaticResxPath && !this.bgImage.startsWith(window.modelApp.baseStaticResxPath))
				this.bgImage = window.modelApp.baseStaticResxPath + this.bgImage;

          if (this.bgColor) {
            this.bgFader = new createjs.Shape();
            this.vfxContainer.addChild(this.bgFader);
            var gfx = this.bgFader.graphics;
            gfx
              .beginFill(this.bgColor)
              .drawRect(0, 0, libWidth, libHeight)
              .endFill();
          }

          if (this.bgImage) {
            this.bgImg = new createjs.Bitmap(this.bgImage);
            this.vfxContainer.addChild(this.bgImg);
          }

          this.steps = [];
          var inserted = 0;
          var currentStep = 0;

          for (var ti = 0; ti < this.timings.length; ti++) {
            var currentTiming = this.timings[ti];
            var actualStep = this.config.steps[currentStep];
            var currentSum = ti == 0 ? this.config.startTime : 0;
            var whichInStep = 0;

            while (currentSum < currentTiming) {
              //if (inserted === 0) {
                //currentSum += this.config.startTime;
              //} else {
                currentSum += this.config.stepTime;
              //}

              this.steps.push(actualStep[whichInStep]);
              whichInStep++;
              if (whichInStep >= actualStep.length) whichInStep = 0;
              inserted++;
            }

            if (currentStep < this.config.steps.length - 1) currentStep++;
          }

          this.spkls = new createjs.Container();
          this.vfxContainer.addChild(this.spkls);
        } //spritesheet
      },
      {
        key: "generateSpriteSheetsFromOptions",
        value: function generateSpriteSheetsFromOptions() {
          var ss = [];

          /*if (this.options.spriteSheet.list !== "") {
            var baseFrames = {
              regX: this.options.spriteSheet.width / 2,
              regY: this.options.spriteSheet.height / 2,
              height: this.options.spriteSheet.height,
              width: this.options.spriteSheet.width
            };
            var baseImages = [];
            var purgedList = this.options.spriteSheet.list.replace(/ /g, "");

            if (purgedList.indexOf(",") >= 0) {
              var spl = purgedList.split(",");

              for (var z = 0; z < spl.length; z++) {
                baseImages.push(spl[z]);
              }
            } else {
              baseImages.push(purgedList);
            }

            if (this.options.spriteSheet.mode == 1) {
              //cycle
              ss.push({
                images: baseImages,
                frames: baseFrames
              });
            } else {
              //persistent
              for (var i = 0; i < baseImages.length; i++) {
                ss.push({
                  images: [baseImages[i]],
                  frames: baseFrames
                });
              }
            }
          }*/

          return this.options.spriteSheet;
        } //tick
      },
      {
        key: "tick",
        value: function tick(event) {
          this.tickActive = true; // loop through all of the active sparkles on stage:

          var l = this.spkls.children.length;
          var m = event.delta / this.config.simulationSpeed;

          for (var i = 0; i < l; i++) {
            var sparkle = this.spkls.getChildAt(i);

            if (--sparkle.life <= 0) {
              this.spkls.removeChild(sparkle);
              i--;
              l--;
              continue;
            } // gravity and friction

            sparkle.vY += this.config.gravity * m; //position

            sparkle.x += sparkle.vX * m;
            sparkle.y += sparkle.vY * m; //alpha

            if (sparkle.life > sparkle.lifeMax * this.config.alphaThreshold) {
              sparkle.alpha =
                sparkle.alphaStart * (sparkle.life / sparkle.lifeMax);
            } //bordi verticali: remove sparkles that are off screen or not invisible

            if (sparkle.y > this.libHeight) {
              sparkle.vY *= -(
                Math.random() * (0.2 * this.config.verticalDecay) +
                0.8 * this.config.verticalDecay
              );
              sparkle.vX +=
                Math.cos(
                  Math.random() *
                    Math.PI *
                    this.config.horizontalAmplitude *
                    this.config.horizontalDecay
                ) *
                2 *
                this.config.horizontalAmplitude *
                this.config.horizontalDecay;
            } else if (sparkle.y < 0) {
              //bordi verticali basso: rimbalzano
              sparkle.vY *= this.config.verticalDecay;
            } //bordi orizzontali: rimbalzano

            if (sparkle.x > this.libWidth || sparkle.x < 0) {
              sparkle.vX *= -this.config.horizontalDecay;
            }
          }
        } // sparkle add
      },
      {
        key: "addSparkles",
        value: function addSparkles(count, x, y, speed, fixedRotation) {
          // create the specified number of sparkles
          for (var i = 0; i < count; i++) {
            // clone the original sparkle
            var whatToUse = Math.floor(
              Math.random() * (this.sprites.length - 0.0000000000001)
            );
            var sparkle = this.sprites[whatToUse].clone(); // set display properties:

            sparkle.x = x;
            sparkle.y = y; //caratteristiche

            if (fixedRotation && fixedRotation !== null) {
              sparkle.rotation = fixedRotation;
            } else {
              sparkle.rotation =
                this.config.particleRotationStartMin +
                Math.random() *
                  (this.config.particleRotationStartMax -
                    this.config.particleRotationStartMin);
            }

            sparkle.alpha = sparkle.alphaStart = this.config.particleAlphaStart; // (Math.random() * 0.1) + 0.9;

            sparkle.scaleX = sparkle.scaleY =
              Math.random() *
                ((0.5 + this.config.particleScaleStartVariation) *
                  this.config.particleScaleStart) +
              0.5 * this.config.particleScaleStart;
            sparkle.life = sparkle.lifeMax =
              Math.random() * (2 * this.config.particleLifeStart) -
              this.config.particleLifeStart; // vettore velocita
			
			sparkle.life *= this.rateo;
			  

            var a = (sparkle.rotation * Math.PI) / 180; // Math.PI * this.config.particleArcMult * (sparkle.rotation/180);

            var d = this.config.particleVelocityDir;

            if (d == 0) {
              if (Math.random() > 0.5) {
                d = 1;
              } else {
                d = -1;
              }
            }

            var v =
              (Math.random() *
                (this.config.particleVelocityMult *
                  this.config.particleVelocityMultRandomizer) +
                this.config.particleVelocityMult *
                  (1 - this.config.particleVelocityMultRandomizer)) *
              speed *
              d;
            sparkle.vX = Math.cos(a) * v;
            sparkle.vY = Math.sin(a) * v; // start the animation on a random frame:

            sparkle.gotoAndPlay(
              (Math.random() * sparkle.spriteSheet.getNumFrames()) | 0
            ); // add to the display list:

            this.spkls.addChild(sparkle);
          }
        } // emitter add
      },
      {
        key: "addEmitter",
        value: function addEmitter() {
          var currentStep = this.steps[this.stepCurrent];
          var currCount =
            (Math.random() * this.config.particleStepNumber) / 2 +
            this.config.particleStepNumber / 2;
          if (currentStep.c !== null && currentStep.c !== undefined)
            currCount = currentStep.c;
          var currX = Math.random() * this.libWidth;
          var currY = Math.random() * this.libHeight;
          if (currentStep.x !== null && currentStep.x !== undefined)
            currX = currentStep.x;
          if (currentStep.y !== null && currentStep.y !== undefined)
            currY = currentStep.y;
          var currSpeed = this.config.particleSpeed;
          if (currentStep.s !== null && currentStep.s !== undefined)
            currSpeed = currentStep.s;
          var fixedRot = null;
          if (currentStep.r !== null && currentStep.r !== undefined)
            fixedRot = currentStep.r;
          this.addSparkles(currCount, currX, currY, currSpeed, fixedRot);
        } //play
      },
      {
        key: "play",
        value: function play() {
          console.log("SPARKLES PLAY", this.steps.length + " steps");
          if (!this.tickActive)
            createjs.Ticker.addEventListener("tick", this.tick.bind(this));
          var self = this;
          var tween = createjs.Tween.get(this, {
            override: true
          });
		  var tweenSparks = createjs.Tween.get(this.spkls, {
            override: true
          });

		  
          this.stepCurrent = 0;

          for (var i = 0; i < this.steps.length; i++) {
            if (i === 0) {
              tween.wait(this.config.startTime);
            } else {
              tween.wait(this.config.stepTime);
            }

            tween.call(function() {
              for (var e = 0; e < self.steps[self.stepCurrent].e; e++) {
				  var tarFps = window.config.fps;
				  var currentFPS = createjs.Ticker.getMeasuredFPS();
				  self.rateo = Math.floor(currentFPS/tarFps * 100)/100;
                self.addEmitter();
              }

              self.stepCurrent++;
            });
          }

          if (this.steps.length > 0) {
            //END
			var totTime = this.config.endTime;
			
			for (var i = 0; i < this.options.timings.length ; i++){
				totTime+=this.options.timings[i];				
			}			
			
		  if(this.bgImg){
			  var tweenBg = createjs.Tween.get(this.bgImg, {
				override: true
			  });
			  tweenBg.wait(totTime - this.config.endTime/3).to({alpha: 0}, this.config.endTime/3);
		  }		
		  if(this.bgFader){
			  var tweenFader = createjs.Tween.get(this.bgFader, {
				override: true
			  });
			  tweenFader.wait(totTime - this.config.endTime/3).to({alpha: 0}, this.config.endTime/3);
		  }				  
			
            tweenSparks.wait(totTime - this.config.endTime).to({alpha: 0}, this.config.endTime); //.wait(this.config.endTime);
            tweenSparks.call(function() {
              self.stop();
            });
          }
        } //stop
      },
      {
        key: "stop",
        value: function stop() {
          createjs.Tween.removeAllTweens();
          createjs.Ticker.removeEventListener("tick", this.tick);
          this.tickActive = false;
          this.vfxContainer.removeAllChildren();
          //if (this.vfxContainer.parent) this.vfxContainer.parent.removeChild(this.vfxContainer);
          this.callback();
        }
      }
    ]);

    return Sparkles;
  })();
