﻿//JSON MANAGER
window.CapecodJsonManager = {
    //PROPERTIES 
    version: 2.56,
    //settings
    centerX: 640,
    centerY: 360,
    iconWidth: 164,
    iconHeight: 164,
    jExpAnimationWidth: 0,
    jExpAnimationHeight: 0,
    jExpAnimationWidthOffset: 0,
    jExpAnimationHeightOffset: 0,
    rows: 3,
    columns: 5,
    selectorWidth: 55,
    selectorHeight: 55,
    selectorContentDisplaceX: [0, 0], //x ombra, sinistra e destra
    selectorContentDisplaceY: [-2, -2], //y ombra, sinistra e destra
    defaultFramerate: 12,
    defaultTickEnabled: true,
    defaultMouseEnabled: true,
    isEditor: false,
    exportRoot: null,
    //--
    defaultExportRoot: "exportRoot",
    defaultFilterOff: [["ColorMatrix", [50, -50, -100, 0]]],
    useDefaultImage: false,
    //internal
    _assetsScale: 1,
    _initialBlur: null, //  [5,90, 1]
    _reelsContainer: null,
    _selectorsContainer: null,
    _symbolsExport: null,
    _pathAdded: false,
    _validationResults: null,
    _defaultImage: null,

    
    //---
    //METHODS
    //init
    init: function (config) {
        window.CapecodJsonManager.LogManager.logC("CapecodJsonManager", "Init v" + window.CapecodJsonManager.version, "info");
        if (config.centerX) window.CapecodJsonManager.centerX = config.centerX;
        if (config.centerY) window.CapecodJsonManager.centerY = config.centerY;
        if (config.iconWidth) window.CapecodJsonManager.iconWidth = config.iconWidth;
        if (config.iconHeight) window.CapecodJsonManager.iconHeight = config.iconHeight;
        if (config.rows) window.CapecodJsonManager.rows = config.rows;
        if (config.columns) window.CapecodJsonManager.columns = config.columns;
        if (config.selectorWidth) window.CapecodJsonManager.selectorWidth = config.selectorWidth;
        if (config.selectorHeight) window.CapecodJsonManager.selectorHeight = config.selectorHeight;
        if (config.selectorContentDisplaceX) window.CapecodJsonManager.selectorContentDisplaceX = Array.isArray(config.selectorContentDisplaceX) ? config.selectorContentDisplaceX : [config.selectorContentDisplaceX, config.selectorContentDisplaceX];
        if (config.selectorContentDisplaceY) window.CapecodJsonManager.selectorContentDisplaceY = Array.isArray(config.selectorContentDisplaceY) ? config.selectorContentDisplaceY : [config.selectorContentDisplaceY, config.selectorContentDisplaceY];
        if (config.defaultFramerate) window.CapecodJsonManager.defaultFramerate = config.defaultFramerate;
        if (config.defaultTickEnabled) window.CapecodJsonManager.defaultTickEnabled = config.defaultTickEnabled;
        if (config.defaultMouseEnabled) window.CapecodJsonManager.defaultMouseEnabled = config.defaultMouseEnabled;
        if (config.isEditor) window.CapecodJsonManager.isEditor = config.isEditor;
        if (config.exportRoot) window.CapecodJsonManager.exportRoot = config.exportRoot;
        if (config.gFontsFamilies) window.CapecodJsonManager.FontManager.gFontsFamilies = config.gFontsFamilies;
        if (config.fontDisplacement) window.CapecodJsonManager.FontManager.fontDisplacement = config.fontDisplacement;
        if (config.useFontDisplacement) window.CapecodJsonManager.FontManager.useFontDisplacement = config.useFontDisplacement;
        if (config.jExpAnimationWidth) window.CapecodJsonManager.jExpAnimationWidth = config.jExpAnimationWidth;
        if (config.jExpAnimationHeight) window.CapecodJsonManager.jExpAnimationHeight = config.jExpAnimationHeight;
        if (config.jExpAnimationWidthOffset) window.CapecodJsonManager.jExpAnimationWidthOffset = config.jExpAnimationWidthOffset;
        if (config.jExpAnimationHeightOffset) window.CapecodJsonManager.jExpAnimationHeightOffset = config.jExpAnimationHeightOffset;

    },
    //ss utils
    whichSpriteSheet: function (source, correct, verbose) {
        var ssId = -1;
        //per ogni spritesheet

        for (var ssi = 0; ssi < window.capecodGameSS.length; ssi++) {
            var ssToUse = window.capecodGameSS[ssi];
            for (var i = 0, len = ssToUse["animations"].length; i < len; i++) {
                var an = ssToUse["animations"][i];
                if (an == source) {
                    //trovato
                    ssId = ssi;
                    i = len;
                    ssi = window.capecodGameSS.length;
                }
            }
        }
        if (ssId < 0) {
            if (verbose === true) {
                window.CapecodJsonManager.LogManager.logC("CapecodJsonManager", source + " NOT FOUND! ", "warn");
            }
            if (correct === 0) {
                //non corregge
            } else {
                //corregge sulla prima ss
                ssId = 0;
            }
        }
        return ssId;
    },
    whichMaxBounds: function (source) {
        var maxW = 0;
        var maxH = 0;
        //per ogni spritesheet
        for (var ssi = 0; ssi < window.capecodGameSS.length; ssi++) {
            var ssToUse = window.capecodGameSS[ssi];
            for (var i = 0, len = ssToUse["animations"].length; i < len; i++) {
                var an = ssToUse["animations"][i];
                if (an == source) {
                    //prendo i bounds
                    var framesUsed = window.capecodGameAssets[ssi]["animations"][source]["frames"];
                    var framesMeasures = window.capecodGameAssets[ssi]["frames"];
                    for (var ffi = 0; ffi < framesUsed.length; ffi++) {
                        //devo trovare il max tra le misure degli used
                        var toEvaluate = framesMeasures[framesUsed[ffi]];
                        var curW = toEvaluate[2];
                        if (curW > maxW) maxW = curW;
                        var curH = toEvaluate[3];
                        if (curH > maxH) maxH = curH;
                    }
                }
            }
        }
        return new createjs.Rectangle(0, 0, maxW, maxH);
    },
    assetExists: function (assetName) {
        if (window.CapecodJsonManager.whichSpriteSheet(assetName, 0, false) < 0) return false;
        else return true;
    },
    localizedAssetName: function (assetName) {
        return assetName + "_" + window.config.lang.toUpperCase();
    },
    bestAssetName: function(assetName){
        if (window.CapecodJsonManager.whichSpriteSheet(window.CapecodJsonManager.localizedAssetName(assetName), 0, false) > -1) 
            return window.CapecodJsonManager.localizedAssetName(assetName);
        return assetName;
    },
    //---
    //generate
    generate: function (children, parent) {
        if (window.CapecodJsonManager.isEditor || (!window.config.fontFiles || (window.config.fontFiles && window.config.fontFiles.length == 0))) {
            setTimeout(function () {
                window.CapecodJsonManager.FontManager.LoadGFonts();
            }, 1500);
        }
        window.CapecodJsonManager.generateFromJson(children, parent);
    },
    generateFromJson: function (children, parent) {
        for (var i = 0, len = children.length; i < len; i++) {
            var item = children[i];

            //override text on mobile h/v
            if (item.type == "Text" && item.mobile && window.modelApp && !window.modelApp.isDesktop) {
                item.font = null;
                item.color = null;
                item.style = null;
                item.fontFamily = null;
                item.fontSize = null;
                item.fontWeight = null;
                item.lineHeight = null;
                item.outline = null;
                item.textAlign = null;
                item.shadow = null;

                if (item.mobile.font) item.font = item.mobile.font;
                if (item.mobile.color) item.color = item.mobile.color;
                if (item.mobile.style) item.style = item.mobile.style;
                if (item.mobile.fontFamily) item.fontFamily = item.mobile.fontFamily;
                if (item.mobile.fontSize) item.fontSize = item.mobile.fontSize;
                if (item.mobile.fontWeight) item.fontWeight = item.mobile.fontWeight;
                if (item.mobile.lineHeight) item.lineHeight = item.mobile.lineHeight;
                if (item.mobile.outline) item.outline = item.mobile.outline;
                if (item.mobile.textAlign) item.textAlign = item.mobile.textAlign;
                if (item.mobile.shadow) item.shadow = item.mobile.shadow;
            }

            var shadowToApply = null;
            if (item.shadow && !jQuery.isEmptyObject(item.shadow)) {
                var x = 0;
                var y = 0;
                var blur = 2;
                var color = "black";

                if (item.shadow["x"]) x = item.shadow["x"];
                if (item.shadow["y"]) y = item.shadow["y"];

                if (item.shadow["x"] && !item.shadow["y"] && item.shadow["y"] != 0) y = x;
                if (item.shadow["y"] && !item.shadow["x"] && item.shadow["x"] != 0) x = y;

                if (item.shadow["blur"]) blur = item.shadow["blur"];
                if (item.shadow["color"]) color = item.shadow["color"];

                //item.shadow
                shadowToApply = [color, x, y, blur];

                //tt.shadow = new createjs.Shadow(color, x, y, blur);
            }
            var obj = null;
            if (item.type == "Sprite") {
                var asset = item.source;
                asset = window.CapecodJsonManager.bestAssetName(asset);
                var ssID = window.CapecodJsonManager.whichSpriteSheet(asset, 0);
                if (ssID >= 0) {                    
                    var ss = new createjs.Sprite(window.capecodGameSS[ssID]);
                    if (item.paused === true) ss.gotoAndStop(asset)
                    else ss.gotoAndPlay(asset);
                    if (item.framerate) ss.framerate = item.framerate
                    else ss.framerate = window.CapecodJsonManager.defaultFramerate;
                    var iconBounds = ss.getBounds(); //window.CapecodJsonManager.whichMaxBounds(item.source); // ss.getBounds();
                    ss.regX = iconBounds.width / 2;
                    ss.regY = iconBounds.height / 2;
                    ss.scaleX = ss.scaleY = window.CapecodJsonManager._assetsScale;
                    obj = ss;
                } else if (window.CapecodJsonManager.useDefaultImage && window.CapecodJsonManager._defaultImage) {
                    //usa default image e genera comunque? -----------------------------
                }
            } else if (item.type == "Video") {
                if (!this.isEditor) {
                    var ss = window.capecodGameVideos[item.source];
                    //console.log(ss);
                    if (ss) {
                        if (ss.image) {
                            ss.regX = ss.image.videoWidth / 2;
                            ss.regY = ss.image.videoHeight / 2;
                        }
                        ss.scaleX = ss.scaleY = window.CapecodJsonManager._assetsScale;
                        obj = ss;
                    }
                    
                }
            } else if (item.type == "Text") {
                var tt;
                if (item.style) {
                    //gradientText
                    tt = new GradientText(item.text, item.font, item.color, item.style);
                } else {
                    //testo
                    // if(item.font){
                    tt = new createjs.Text(item.text, item.font, item.color);
                    // }else{
                    //     var font = item.fontWeight + " " + item.fontSize + " '" + item.fontFamily +"' ";
                    //     tt = new createjs.Text(item.text, font, item.color);
                    // }
                }

                if (item.fontFamily) tt.fontFamily = item.fontFamily;
                if (item.fontSize) tt.fontSize = item.fontSize;
                if (item.fontWeight) tt.fontWeight = item.fontWeight;

                if (item.lineHeight) tt.lineHeight = item.lineHeight;
                if (item.outline) tt.outline = item.outline;
                if (item.textAlign) tt.textAlign = item.textAlign
                else tt.textAlign = "center";

                tt.textBaseline = "alphabetic";

                tt.x = tt.y = 0;
                window.CapecodJsonManager.FontManager.updateTextCache(tt);
                //gfont
                window.CapecodJsonManager.FontManager.addToFontManager(tt);
                obj = tt;
            } else if (item.type == "Container") {
                var cc = new createjs.Container();
                obj = cc;
                if (item.states) {
                    obj.states = item.states;
                }
                if (item.children) {
                    var haveChildrenReels = false;
                    var haveChildrenSelectors = false;
                    for (var z = 0, lenz = item.children.length; z < lenz; z++) {
                        var child = item.children[z];
                        if (child.type === "Reel") haveChildrenReels = true;
                        if (child.type === "Selector") haveChildrenSelectors = true;
                    }
                    if (haveChildrenReels) {
                        if (window.CapecodJsonManager._reelsContainer) {
                            window.CapecodJsonManager.LogManager.logC("CapecodJsonManager", "MULTIPLE reelsContainer", "warn");
                        }
                        window.CapecodJsonManager._reelsContainer = obj;
                        obj.mouseChildren = false;
                    }
                    if (haveChildrenSelectors) {
                        if (window.CapecodJsonManager._selectorsContainer) {
                            window.CapecodJsonManager.LogManager.logC("CapecodJsonManager", "MULTIPLE selectorsContainer", "info");
                        }
                        window.CapecodJsonManager._selectorsContainer = obj;
                    }
                    //---
                    if (item.mouseChildren) {
                        obj.mouseChildren = item.mouseChildren;
                    } else {
                        obj.mouseChildren = true;
                    }
                    if (item.tickChildren) {
                        obj.tickChildren = item.tickChildren;
                    } else {
                        obj.tickChildren = true;
                    }
                }
            } else if (item.type == "Reel") {
                window.CapecodJsonManager.generateReel(item.name, item.x, item.y, item.scaleX, item.scaleY, item.rotation, item);
            } else if (item.type == "Selector") {
                //if(item.font){
                window.CapecodJsonManager.generateSelector(item.id, item.name, item.alt, item.sources, item.x, item.y, item.scaleX, item.scaleY, item.rotation, item.font, item.color, item.noText, item, parent);
                //}else{
                //    window.CapecodJsonManager.generateSelector(item.id, item.name, item.alt, item.sources, item.x, item.y, item.scaleX, item.scaleY, item.rotation, '', item.color, item.noText, item);
                //}
            } else if (item.type == "Symbols") {
                if (window.CapecodJsonManager._symbolsExport == true) {
                    window.CapecodJsonManager.LogManager.logC("CapecodJsonManager", "MULTIPLE symbolsExport", "warn");
                } else {
                    //creo il container
                    var ccc = new createjs.Container();
                    obj = ccc;
                    window.CapecodJsonManager._symbolsExport = ccc;
                    var filterOn = null;
                    var filterOff = null;
                    var filterBlur = null;
                    var filterBlur2 = null;
                    if (item.filters) filterOn = item.filters;
                    if (item.filtersOff) filterOff = item.filtersOff;
                    if (item.filtersBlur) filterBlur = item.filtersBlur;
                    if (item.filtersBlur2) filterBlur2 = item.filtersBlur2;
                    window.CapecodJsonManager.generateSymbolsExport(ccc, filterOn, filterOff, filterBlur, filterBlur2);
                }
            }
            if (obj) {
                /*
                obj.onlyDestop = false;
                obj.onlyMobile = false;
                */

                if (item.id) obj.name = item.id;
                if (item.mask) {
                    var iconBounds2 = new createjs.Rectangle(item.mask[0] / window.CapecodJsonManager._assetsScale, item.mask[1] / window.CapecodJsonManager._assetsScale, item.mask[2] / window.CapecodJsonManager._assetsScale, item.mask[3] / window.CapecodJsonManager._assetsScale);
                    obj.cache(iconBounds2.x, iconBounds2.y, iconBounds2.width, iconBounds2.height);
                    obj.regX = iconBounds2.x + (iconBounds2.width / 2);
                    obj.regY = iconBounds2.y + (iconBounds2.height / 2);
                    obj.mask = iconBounds2;
                }
                if (item.x) obj.x = item.x;
                if (item.y) obj.y = item.y;

                obj.originalX = item.x ? item.x : 0;
                obj.originalY = item.y ? item.y : 0;

                if (item.scaleX) {
                    if (item.type == "Sprite" || item.type == "Video") obj.scaleX = window.CapecodJsonManager._assetsScale * item.scaleX //aggiunta condizione || item.type == "Video"
                    else obj.scaleX = item.scaleX;
                }
                if (item.scaleY) {
                    if (item.type == "Sprite" || item.type == "Video") obj.scaleY = window.CapecodJsonManager._assetsScale * item.scaleY //aggiunta condizione || item.type == "Video"
                    else obj.scaleY = item.scaleY;
                }
                if (item.rotation) obj.rotation = item.rotation;
                obj.visible = true;
                if (item.visible === false) obj.visible = false;
                if (item.alpha) obj.alpha = item.alpha;
                if (shadowToApply) {
                    window.CapecodJsonManager.applyShadow(obj, shadowToApply);
                    if (item.type == "Text") {
                        window.CapecodJsonManager.FontManager.updateTextCache(obj);
                    }
                }
                if (item.filters) {
                    window.CapecodJsonManager.applyFilters(obj, item.filters);
                }
                if (item.cursor && item.cursor !== "") {
                    obj.cursor = item.cursor;
                } else {
                    obj.cursor = "";
                    obj.cursor = null;
                }
                if (item.mouseEnabled === true) {
                    obj.mouseEnabled = true;
                } else {
                    if (item.mouseEnabled === false) {
                        obj.mouseEnabled = false;
                    } else {
                        obj.mouseEnabled = window.CapecodJsonManager.defaultMouseEnabled;
                    }
                }
                if (item.tickEnabled === true) {
                    obj.tickEnabled = true;
                } else {
                    if (item.tickEnabled === false) {
                        obj.tickEnabled = false;
                    } else {
                        obj.tickEnabled = window.CapecodJsonManager.defaultTickEnabled;
                    }
                }

                /*
                if (item.deviceRestriction) {
                    if (item.deviceRestriction.onlyDestop) obj.onlyDestop = true;
                    if (item.deviceRestriction.onlyMobile) obj.onlyMobile = true;
                }
                */

                /*
                if (item.mobileH) {
                    obj.mobileH = item.mobileH;
                }

                if (item.mobileV) {
                    obj.mobileV = item.mobileV;
                }

                */

                //IDs LIBRARY
                //parent[item.id] = obj;
                switch (item.id) {
                    case "active":
                    case "hover":
                    case "normal":
                        //casi speciali
                        parent[item.id] = obj;
                        break;
                    case "animClose":
                        parent.animClose = obj;
                        break;
                    case "animOpen":
                        parent.animOpen = obj;
                        break;
                    case "animRight":
                        parent.animRight = obj;
                        break;
                    case "animLeft":
                        parent.animLeft = obj;
                        break;
                    case "autoplay":
                        parent.autoplay = obj;
                        break;
                    case "bg":
                        parent.bg = obj;
                        break;
                    case "bgAnimLeft":
                        parent.bgAnimLeft = obj;
                        break;
                    case "bgAnimRight":
                        parent.bgAnimRight = obj;
                        break;
                    case "bgDxClose":
                        parent.bgDxClose = obj;
                        break;
                    case "bgDxOpen":
                        parent.bgDxOpen = obj;
                        break;
                    case "bgGame":
                        parent.bgGame = obj;
                        break;
                    case "bgGrid":
                        parent.bgGrid = obj;
                        break;
                    case "bgSxClose":
                        parent.bgSxClose = obj;
                        break;
                    case "bgSxOpen":
                        parent.bgSxOpen = obj;
                        break;
                    case "blue":
                        parent.blue = obj;
                        break;
                    case "bonuswin":
                        parent.bonuswin = obj;
                        break;
                    case "button10":
                        parent.button10 = obj;
                        break;
                    case "button100":
                        parent.button100 = obj;
                        break;
                    case "button5":
                        parent.button5 = obj;
                        break;
                    case "button50":
                        parent.button50 = obj;
                        break;
                    case "button500":
                        parent.button500 = obj;
                        break;
                    case "buttonAutoplay":
                        parent.buttonAutoplay = obj;
                        break;
                    case "buttonMaxbet":
                        parent.buttonMaxbet = obj;
                        break;
                    case "buttonHome":
                        parent.buttonHome = obj;
                        break;
                    case "buttonMenu":
                        parent.buttonMenu = obj;
                        break;
                    case "buttonMinus":
                        parent.buttonMinus = obj;
                        break;
                    case "buttonPlus":
                        parent.buttonPlus = obj;
                        break;
                    case "buttonSpin":
                        parent.buttonSpin = obj;
                        break;
                    case "containerAnimWin":
                        parent.containerAnimWin = obj;
                        break;
                    case "containerAnimWinOver":
                        parent.containerAnimWinOver = obj;
                        break;
                    case "exit":
                        parent.exit = obj;
                        break;
                    case "particlesContainer":
                        parent.particlesContainer = obj;
                        break;
                    case "expandContainer":
                        parent.expandContainer = obj;
                        break;
                    case "randomWildContainer":
                        parent.randomWildContainer = obj;
                        break;
                    case "stickyContainer":
                        parent.stickyContainer = obj;
                        break;
                    case "verticalExpContainer":
                        parent.verticalExpContainer = obj;
                        break;
                    case "collectorContainer":
                        parent.collectorContainer = obj;
                        break;
                    case "fieldCoins":
                        parent.fieldCoins = obj;
                        break;
                    case "fieldTotalBet":
                        parent.fieldTotalBet = obj;
                        break;
                    case "freerounds":
                        parent.freerounds = obj;
                        break;
                    case "freespin":
                        parent.freespin = obj;
                        break;
                    case "freespinLabel":
                        parent.freespinLabel = obj;
                        break;
                    case "gameContainer":
                        parent.gameContainer = obj;
                        break;
                    case "grid":
                        parent.grid = obj;
                        break;
                    case "highprize":
                        parent.highprize = obj;
                        break;
                    case "iconsContainer":
                        parent.iconsContainer = obj;
                        break;
                    case "inputCoinLines":
                        parent.inputCoinLines = obj;
                        break;
                    case "inputCoinVal":
                        parent.inputCoinVal = obj;
                        break;
                    case "inputLines":
                        parent.inputLines = obj;
                        break;
                    case "label":
                        parent.label = obj;
                        break;
                    case "labelBottom":
                        parent.labelBottom = obj;
                        break;
                    case "labelDown":
                        parent.labelDown = obj;
                        break;
                    case "labelLeft":
                        parent.labelLeft = obj;
                        break;
                    case "labelRight":
                        parent.labelRight = obj;
                        break;
                    case "labelTitle":
                        parent.labelTitle = obj;
                        break;
                    case "labelUp":
                        parent.labelUp = obj;
                        break;
                    case "lineLabel":
                        parent.lineLabel = obj;
                        break;
                    case "linesContainer":
                        parent.linesContainer = obj;
                        break;
                    case "logo":
                        parent.logo = obj;
                        break;
                    case "magicspin":
                        parent.magicspin = obj;
                        break;
                    case "moneyContainer":
                        parent.moneyContainer = obj;
                        break;
                    case "panel":
                        parent.panel = obj;
                        break;
                    case "paytable":
                        parent.paytable = obj;
                        break;
                    case "producer":
                        parent.producer = obj;
                        break;
                    case "brand":
                        parent.brand = obj;
                        break;
                    case "red":
                        parent.red = obj;
                        break;
                    case "reel1":
                        parent.reel1 = obj;
                        break;
                    case "reel2":
                        parent.reel2 = obj;
                        break;
                    case "reel3":
                        parent.reel3 = obj;
                        break;
                    case "reel4":
                        parent.reel4 = obj;
                        break;
                    case "reel5":
                        parent.reel5 = obj;
                        break;
                    case "reelsContainer":
                        parent.reelsContainer = obj;
                        break;
                    case "riepilogLabel":
                        parent.riepilogLabel = obj;
                        break;
                    case "selector1":
                        parent.selector1 = obj;
                        break;
                    case "selector10":
                        parent.selector10 = obj;
                        break;
                    case "selector11":
                        parent.selector11 = obj;
                        break;
                    case "selector12":
                        parent.selector12 = obj;
                        break;
                    case "selector13":
                        parent.selector13 = obj;
                        break;
                    case "selector14":
                        parent.selector14 = obj;
                        break;
                    case "selector15":
                        parent.selector15 = obj;
                        break;
                    case "selector16":
                        parent.selector16 = obj;
                        break;
                    case "selector17":
                        parent.selector17 = obj;
                        break;
                    case "selector18":
                        parent.selector18 = obj;
                        break;
                    case "selector19":
                        parent.selector19 = obj;
                        break;
                    case "selector2":
                        parent.selector2 = obj;
                        break;
                    case "selector20":
                        parent.selector20 = obj;
                        break;
                    case "selector21":
                        parent.selector21 = obj;
                        break;
                    case "selector22":
                        parent.selector22 = obj;
                        break;
                    case "selector23":
                        parent.selector23 = obj;
                        break;
                    case "selector24":
                        parent.selector24 = obj;
                        break;
                    case "selector25":
                        parent.selector25 = obj;
                        break;
                    case "selector26":
                        parent.selector26 = obj;
                        break;
                    case "selector3":
                        parent.selector3 = obj;
                        break;
                    case "selector4":
                        parent.selector4 = obj;
                        break;
                    case "selector5":
                        parent.selector5 = obj;
                        break;
                    case "selector6":
                        parent.selector6 = obj;
                        break;
                    case "selector7":
                        parent.selector7 = obj;
                        break;
                    case "selector8":
                        parent.selector8 = obj;
                        break;
                    case "selector9":
                        parent.selector9 = obj;
                        break;
                    case "selectorsContainer":
                        parent.selectorsContainer = obj;
                        break;
                    case "states":
                        parent.states = obj;
                        break;
                    case "symbolsExport":
                        parent.symbolsExport = obj;
                        break;
                    case "textContainer":
                        parent.textContainer = obj;
                        break;
                    case "textEndLabel":
                        parent.textEndLabel = obj;
                        break;
                    case "textInLabel":
                        parent.textInLabel = obj;
                        break;
                    case "textOutLabel":
                        parent.textOutLabel = obj;
                        break;
                    case "textOutLabelNoRoller":
                        parent.textOutLabelNoRoller = obj;
                        break;
                    case "title":
                        parent.title = obj;
                        break;
                    case "toolbarContainer":
                        parent.toolbarContainer = obj;
                        break;
                    case "value":
                        parent.value = obj;
                        break;
                    case "viewIntro":
                        parent.viewIntro = obj;
                        break;
                    case "viewMinigame":
                        parent.viewMinigame = obj;
                        break;
                    case "postIntro":
                        parent.postIntro = obj;
                        break;
                    case "win":
                        parent.win = obj;
                        break;
                    case "wipeContainer":
                        parent.wipeContainer = obj;
                        break;
                    case "wonderprize":
                        parent.wonderprize = obj;
                        break;
                    case "play":
                        parent.play = obj;
                        break;
                    case "s00":
                        parent.s00 = obj;
                        break;
                    case "s01":
                        parent.s01 = obj;
                        break;
                    case "s02":
                        parent.s02 = obj;
                        break;
                    case "s03":
                        parent.s03 = obj;
                        break;
                    case "s04":
                        parent.s04 = obj;
                        break;
                    case "s05":
                        parent.s05 = obj;
                        break;
                    case "s06":
                        parent.s06 = obj;
                        break;
                    case "s07":
                        parent.s07 = obj;
                        break;
                    case "s08":
                        parent.s08 = obj;
                        break;
                    case "s09":
                        parent.s09 = obj;
                        break;
                    case "s10":
                        parent.s10 = obj;
                        break;
                    case "s11":
                        parent.s11 = obj;
                        break;
                    case "s12":
                        parent.s12 = obj;
                        break;
                    case "s13":
                        parent.s13 = obj;
                        break;
                    case "s14":
                        parent.s14 = obj;
                        break;
                    case "s15":
                        parent.s15 = obj;
                        break;
                    case "s16":
                        parent.s16 = obj;
                        break;
                    case "s17":
                        parent.s17 = obj;
                        break;
                    case "s18":
                        parent.s18 = obj;
                        break;
                    case "s19":
                        parent.s19 = obj;
                        break;
                    case "s20":
                        parent.s20 = obj;
                        break;
                    case "s21":
                        parent.s21 = obj;
                        break;
                    case "s22":
                        parent.s22 = obj;
                        break;
                    case "s23":
                        parent.s23 = obj;
                        break;
                    case "s24":
                        parent.s24 = obj;
                        break;
                    case "s25":
                        parent.s25 = obj;
                        break;
                    case "s26":
                        parent.s26 = obj;
                        break;
                    case "s27":
                        parent.s27 = obj;
                        break;
                    case "s28":
                        parent.s28 = obj;
                        break;
                    case "s29":
                        parent.s29 = obj;
                        break;
                    case "s30":
                        parent.s30 = obj;
                        break;


                    case "s00_on":
                        parent.s00_on = obj;
                        break;
                    case "s01_on":
                        parent.s01_on = obj;
                        break;
                    case "s02_on":
                        parent.s02_on = obj;
                        break;
                    case "s03_on":
                        parent.s03_on = obj;
                        break;
                    case "s04_on":
                        parent.s04_on = obj;
                        break;
                    case "s05_on":
                        parent.s05_on = obj;
                        break;
                    case "s06_on":
                        parent.s06_on = obj;
                        break;
                    case "s07_on":
                        parent.s07_on = obj;
                        break;
                    case "s08_on":
                        parent.s08_on = obj;
                        break;
                    case "s09_on":
                        parent.s09_on = obj;
                        break;
                    case "s10_on":
                        parent.s10_on = obj;
                        break;
                    case "s11_on":
                        parent.s11_on = obj;
                        break;
                    case "s12_on":
                        parent.s12_on = obj;
                        break;
                    case "s13_on":
                        parent.s13_on = obj;
                        break;
                    case "s14_on":
                        parent.s14_on = obj;
                        break;
                    case "s15_on":
                        parent.s15_on = obj;
                        break;
                    case "s16_on":
                        parent.s16_on = obj;
                        break;
                    case "s17_on":
                        parent.s17_on = obj;
                        break;
                    case "s18_on":
                        parent.s18_on = obj;
                        break;
                    case "s19_on":
                        parent.s19_on = obj;
                        break;
                    case "s20_on":
                        parent.s20_on = obj;
                        break;
                    case "s21_on":
                        parent.s21_on = obj;
                        break;
                    case "s22_on":
                        parent.s22_on = obj;
                        break;
                    case "s23_on":
                        parent.s23_on = obj;
                        break;
                    case "s24_on":
                        parent.s24_on = obj;
                        break;
                    case "s25_on":
                        parent.s25_on = obj;
                        break;
                    case "s26_on":
                        parent.s26_on = obj;
                        break;
                    case "s27_on":
                        parent.s27_on = obj;
                        break;
                    case "s28_on":
                        parent.s28_on = obj;
                        break;
                    case "s29_on":
                        parent.s29_on = obj;
                        break;
                    case "s30_on":
                        parent.s30_on = obj;
                        break;

                    case "s00_off":
                        parent.s00_off = obj;
                        break;
                    case "s01_off":
                        parent.s01_off = obj;
                        break;
                    case "s02_off":
                        parent.s02_off = obj;
                        break;
                    case "s03_off":
                        parent.s03_off = obj;
                        break;
                    case "s04_off":
                        parent.s04_off = obj;
                        break;
                    case "s05_off":
                        parent.s05_off = obj;
                        break;
                    case "s06_off":
                        parent.s06_off = obj;
                        break;
                    case "s07_off":
                        parent.s07_off = obj;
                        break;
                    case "s08_off":
                        parent.s08_off = obj;
                        break;
                    case "s09_off":
                        parent.s09_off = obj;
                        break;
                    case "s10_off":
                        parent.s10_off = obj;
                        break;
                    case "s11_off":
                        parent.s11_off = obj;
                        break;
                    case "s12_off":
                        parent.s12_off = obj;
                        break;
                    case "s13_off":
                        parent.s13_off = obj;
                        break;
                    case "s14_off":
                        parent.s14_off = obj;
                        break;
                    case "s15_off":
                        parent.s15_off = obj;
                        break;
                    case "s16_off":
                        parent.s16_off = obj;
                        break;
                    case "s17_off":
                        parent.s17_off = obj;
                        break;
                    case "s18_off":
                        parent.s18_off = obj;
                        break;
                    case "s19_off":
                        parent.s19_off = obj;
                        break;
                    case "s20_off":
                        parent.s20_off = obj;
                        break;
                    case "s21_off":
                        parent.s21_off = obj;
                        break;
                    case "s22_off":
                        parent.s22_off = obj;
                        break;
                    case "s23_off":
                        parent.s23_off = obj;
                        break;
                    case "s24_off":
                        parent.s24_off = obj;
                        break;
                    case "s25_off":
                        parent.s25_off = obj;
                        break;
                    case "s26_off":
                        parent.s26_off = obj;
                        break;
                    case "s27_off":
                        parent.s27_off = obj;
                        break;
                    case "s28_off":
                        parent.s28_off = obj;
                        break;
                    case "s29_off":
                        parent.s29_off = obj;
                        break;
                    case "s30_off":
                        parent.s30_off = obj;
                        break;

                    case "statusBlur":
                        parent.statusBlur = obj;
                        break;
                    case "statusBlur2":
                        parent.statusBlur2 = obj;
                        break;
                    case "statusOff":
                        parent.statusOff = obj;
                        break;
                    case "statusOn":
                        parent.statusOn = obj;
                        break;
                    case "statusWin":
                        parent.statusWin = obj;
                        break;
                    case "stop":
                        parent.stop = obj;
                        break;
                    case "freespinWin":
                        parent.freespinWin = obj;
                        break;
                    case "playa":
                        parent.playa = obj;
                        break;
                    case "bonus":
                        parent.bonus = obj;
                        break;
                    case "jackpot":
                        parent.jackpot = obj;
                        break;
                    case "jackpotInfoValue":
                        parent.jackpotInfoValue = obj;
                        break;
                    case "jackpots":
                        parent.jackpots = obj;
                        break;
                    case "jackpot0":
                        parent.jackpot0 = obj;
                        break;
                    case "jackpot1":
                        parent.jackpot1 = obj;
                        break;
                    case "jackpot2":
                        parent.jackpot2 = obj;
                        break;
                    case "jackpot3":
                        parent.jackpot3 = obj;
                        break;

                    case "splash_anim_A":
                        parent.splash_anim_A = obj;
                        break;
                    case "splash_anim_B":
                        parent.splash_anim_B = obj;
                        break;

                    case "splash_anim_C":
                        parent.splash_anim_C = obj;
                        break;
                    case "splash_anim_D":
                        parent.splash_anim_D = obj;
                        break;

                    case "splash_anim_E":
                        parent.splash_anim_E = obj;
                        break;
                    case "splash_anim_F":
                        parent.splash_anim_F = obj;
                        break;

                    case "splash_anim_G":
                        parent.splash_anim_G = obj;
                        break;
                    case "splash_anim_H":
                        parent.splash_anim_H = obj;
                        break;

                    case "splash_anim_I":
                        parent.splash_anim_I = obj;
                        break;
                    case "splash_anim_J":
                        parent.splash_anim_J = obj;
                        break;

                    case "splash_anim_K":
                        parent.splash_anim_K = obj;
                        break;
                    case "splash_anim_L":
                        parent.splash_anim_L = obj;
                        break;

                    case "splash_anim_M":
                        parent.splash_anim_M = obj;
                        break;
                    case "splash_anim_N":
                        parent.splash_anim_N = obj;
                        break;

                    case "splash_anim_O":
                        parent.splash_anim_O = obj;
                        break;
                    case "splash_anim_P":
                        parent.splash_anim_P = obj;
                        break;

                    case "splash_anim_Q":
                        parent.splash_anim_Q = obj;
                        break;
                    case "splash_anim_R":
                        parent.splash_anim_R = obj;
                        break;

                    case "splash_anim_S":
                        parent.splash_anim_S = obj;
                        break;
                    case "splash_anim_T":
                        parent.splash_anim_T = obj;
                        break;
                    case "splash_anim_U":
                        parent.splash_anim_U = obj;
                        break;
                    case "splash_anim_V":
                        parent.splash_anim_V = obj;
                        break;
                    case "splash_anim_W":
                        parent.splash_anim_W = obj;
                        break;
                    case "splash_anim_X":
                        parent.splash_anim_X = obj;
                        break;
                    case "splash_anim_Y":
                        parent.splash_anim_Y = obj;
                        break;
                    case "splash_anim_Z":
                        parent.splash_anim_Z = obj;
                        break;
                    case "charge_to1":
                        parent.charge_to1 = obj;
                        break;
                    case "charge_to2":
                        parent.charge_to2 = obj;
                        break;
                    case "charge_to3":
                        parent.charge_to3 = obj;
                        break;
                    case "charge_to4":
                        parent.charge_to4 = obj;
                        break;
                    case "charge_to5":
                        parent.charge_to5 = obj;
                        break;
                    case "charge_to6":
                        parent.charge_to6 = obj;
                        break;
                    case "charge_to7":
                        parent.charge_to7 = obj;
                        break;
                    case "charge_to8":
                        parent.charge_to8 = obj;
                        break;
                    case "charge_to9":
                        parent.charge_to9 = obj;
                        break;
                    case "charge_to10":
                        parent.charge_to10 = obj;
                        break;
                    case "charge_bg":
                        parent.charge_bg = obj;
                        break;
                    case "charge_idle0":
                        parent.charge_idle0 = obj;
                        break;
                    case "charge_idle1":
                        parent.charge_idle1 = obj;
                        break;
                    case "charge_idle2":
                        parent.charge_idle2 = obj;
                        break;
                    case "charge_idle3":
                        parent.charge_idle3 = obj;
                        break;
                    case "charge_idle4":
                        parent.charge_idle4 = obj;
                        break;
                    case "charge_idle5":
                        parent.charge_idle5 = obj;
                        break;
                    case "charge_idle6":
                        parent.charge_idle6 = obj;
                        break;
                    case "charge_idle7":
                        parent.charge_idle7 = obj;
                        break;
                    case "charge_idle8":
                        parent.charge_idle8 = obj;
                        break;
                    case "charge_idle9":
                        parent.charge_idle9 = obj;
                        break;
                    case "charge_idle10":
                        parent.charge_idle10 = obj;
                        break;

                    case "charge_from1":
                        parent.charge_from1 = obj;
                        break;
                    case "charge_from2":
                        parent.charge_from2 = obj;
                        break;
                    case "charge_from3":
                        parent.charge_from3 = obj;
                        break;
                    case "charge_from4":
                        parent.charge_from4 = obj;
                        break;
                    case "charge_from5":
                        parent.charge_from5 = obj;
                        break;
                    case "charge_from6":
                        parent.charge_from6 = obj;
                        break;
                    case "charge_from7":
                        parent.charge_from7 = obj;
                        break;
                    case "charge_from8":
                        parent.charge_from8 = obj;
                        break;
                    case "charge_from9":
                        parent.charge_from9 = obj;
                        break;
                    case "charge_from10":
                        parent.charge_from10 = obj;
                        break;
                    case "chargeContainer":
                        parent.chargeContainer = obj;
                        break;
                    case "changeBetInfoBox":
                        parent.changeBetInfoBox = obj;
                        break;
                    case "normal_charge0":
                        parent.normal_charge0 = obj;
                        break;
                    case "normal_charge1":
                        parent.normal_charge1 = obj;
                        break;
                    case "normal_charge2":
                        parent.normal_charge2 = obj;
                        break;
                    case "normal_charge3":
                        parent.normal_charge3 = obj;
                        break;
                    case "normal_charge4":
                        parent.normal_charge4 = obj;
                        break;
                    case "normal_charge5":
                        parent.normal_charge5 = obj;
                        break;
                    case "normal_charge6":
                        parent.normal_charge6 = obj;
                        break;
                    case "normal_charge7":
                        parent.normal_charge7 = obj;
                        break;
                    case "normal_charge8":
                        parent.normal_charge8 = obj;
                        break;
                    case "normal_charge9":
                        parent.normal_charge9 = obj;
                        break;
                    case "normal_charge10":
                        parent.normal_charge10 = obj;
						break
                    case "freespin_charge0":
                        parent.freespin_charge0 = obj;
                        break;
                    case "freespin_charge1":
                        parent.freespin_charge1 = obj;
                        break;
                    case "freespin_charge2":
                        parent.freespin_charge2 = obj;
                        break;
                    case "freespin_charge3":
                        parent.freespin_charge3 = obj;
                        break;
                    case "freespin_charge4":
                        parent.freespin_charge4 = obj;
                        break;
                    case "freespin_charge5":
                        parent.freespin_charge5 = obj;
                        break;
                    case "freespin_charge6":
                        parent.freespin_charge6 = obj;
                        break;
                    case "freespin_charge7":
                        parent.freespin_charge7 = obj;
                        break;
                    case "freespin_charge8":
                        parent.freespin_charge8 = obj;
                        break;
                    case "freespin_charge9":
                        parent.freespin_charge9 = obj;
                        break;
                    case "freespin_charge10":
                        parent.freespin_charge10 = obj;
                        break;
                    case "description":
                        parent.description = obj;
                        break;
                    case "cbBox":
                        parent.cbBox = obj;
                        break;
                    case "cbText":
                        parent.cbText = obj;
                        break;
                    case "checkboxWin":
                        parent.checkboxWin = obj;
                        break;
                    case "normalhover":
                        parent.normalhover = obj;
                        break;
                    case "by_capecod":
                        parent.by_capecod = obj;
                        break;
                    case "jackpotWin":
                        parent.jackpotWin = obj;
                        break;
                    case "jackpotWinName":
                        parent.jackpotWinName = obj;
                        break;
                    case "jackpotWinValidation":
                        parent.jackpotWinValidation = obj;
                        break;
                    case "jackpotWinYouWon":
                        parent.jackpotWinYouWon = obj;
                        break;
                    case "jackpotWinAmount":
                        parent.jackpotWinAmount = obj;
                        break;
                    case "multipliers":
                        parent.multipliers = obj;
                        break;
                    case "specialSymbol":
                        parent.specialSymbol = obj;
                        break;
                    case "labelContainer":
                        parent.labelContainer = obj;
                        break;

                    case "videointro":
                        parent.videointro = obj;
                        break;

                    case "progressContainer":
                        parent.progressContainer = obj;
                        break;
                    case "suspenseContainer":
                        parent.suspenseContainer = obj;
                        break;

                    case "energies":
                        parent.energies = obj;
                        break;
                    case "energySelector":
                        parent.energySelector = obj;
                        break;
                    case "energy0":
                        parent.energy0 = obj;
                        break;
                    case "energy1":
                        parent.energy1 = obj;
                        break;
                    case "energy2":
                        parent.energy2 = obj;
                        break;
                    case "energy3":
                        parent.energy3 = obj;
                        break;
                    case "summary":
                        parent.summary = obj;
                        break;
                    case "total":
                        parent.total = obj;
                        break;

                        
                    case "extraElementsContainer":
                        parent.extraElementsContainer = obj;
                        break;
                    case "extraMultiplySpecial":
                        parent.extraMultiplySpecial = obj;
                        break;
                    case "extraMultiply":
                        parent.extraMultiply = obj;
                        break;
                    case "extraSpecial":
                        parent.extraSpecial = obj;
                        break;

                    case "cmdVideoPoker":
                        parent.cmdVideoPoker = obj;
                        break;
                    case "holder":
                        parent.holder = obj;
                        break;
                    case "vpClickableAreas":
                        parent.vpClickableAreas = obj;
                        break;

                    case "videoPokerContainer":
                        parent.videoPokerContainer = obj;
                        break;
                    case "vpReel1":
                        parent.vpReel1 = obj;
                        break;
                    case "vpReel2":
                        parent.vpReel2 = obj;
                        break;
                    case "vpReel3":
                        parent.vpReel3 = obj;
                        break;
                    case "vpReel4":
                        parent.vpReel4 = obj;
                        break;
                    case "vpReel5":
                        parent.vpReel5 = obj;
                        break;

                    case "target":
                        parent.target = obj;
                        break;
                    case "lives":
                        parent.lives = obj;
                        break;

                    case "custom1":
                        parent.custom1 = obj;
                        break;
                    case "custom2":
                        parent.custom2 = obj;
                        break;
                    case "custom3":
                        parent.custom3 = obj;
                        break;
                    case "custom4":
                        parent.custom4 = obj;
                        break;
                    case "custom5":
                        parent.custom5 = obj;
                        break;
                    case "custom6":
                        parent.custom6 = obj;
                        break;
                    case "custom7":
                        parent.custom7 = obj;
                        break;
                    case "custom8":
                        parent.custom8 = obj;
                        break;
                    case "custom9":
                        parent.custom9 = obj;
                        break;
                    case "custom10":
                        parent.custom10 = obj;
                        break;
                    case "custom11":
                        parent.custom11 = obj;
                        break;
                    case "custom12":
                        parent.custom12 = obj;
                        break;

                    case "v_normal":
                        parent.v_normal = obj;
                        break;
                    case "v_freespin":
                        parent.v_freespin = obj;
                        break;
                    case "v_magicspin":
                        parent.v_magicspin = obj;
                        break;
                    case "v_bonus":
                        parent.v_bonus = obj;
                        break;
                    case "v_freerounds":
                        parent.v_freerounds = obj;
                        break;
                    case "v_jackpot":
                        parent.v_jackpot = obj;
                        break;

                    case "multipliersPanel":
                        parent.multipliersPanel = obj;
                        break;
                    case "extraLbl":
                        parent.extraLbl = obj;
                        break;
                    case "txtCount":
                        parent.txtCount = obj;
                        break;
                    case "txtCountOff":
                        parent.txtCountOff = obj;
                        break;
                    case "bgTxt":
                        parent.bgTxt = obj;
                        break;
                    case "multi0":
                        parent.multi0 = obj;
                        break;
                    case "multi1":
                        parent.multi1 = obj;
                        break;
                    case "multi2":
                        parent.multi2 = obj;
                        break;
                    case "multi3":
                        parent.multi3 = obj;
                        break;
                    case "multi4":
                        parent.multi4 = obj;
                        break;
					case "requiredVertical":
							parent.requiredVertical = obj;
                        break;
                    default:
                        {
                            window.CapecodJsonManager.LogManager.logC("CapecodJsonManager", "GenerateFromJson symbols mapper " + item.id + " not found!", "warn");
                        }
                        break;
                }
                parent.addChild(obj);

                if (item.kind == "Button") {
                    if (window.CapecodJsonManager.isEditor) {
                        //-----------------------------------
                        //SOLO PER EDITOR
                        obj.cursor = "pointer";
                        obj.addEventListener("mouseover", function (e) {
                            if (e.currentTarget["hover"]) {
                                e.currentTarget["hover"].visible = true;
                                e.currentTarget["normal"].visible = false;
                            } else e.currentTarget["normal"].visible = true;

                            if (e.currentTarget.cacheCanvas) e.currentTarget.updateCache();
                        });
                        obj.addEventListener("mouseout", function (e) {
                            if (e.currentTarget["hover"]) e.currentTarget["hover"].visible = false;
                            e.currentTarget["normal"].visible = true;
                            if (e.currentTarget.cacheCanvas) e.currentTarget.updateCache();
                        });
                    }
                }
                if (item.children) {
                    window.CapecodJsonManager.generateFromJson(item.children, obj);
                }
                //resizables
                if (item.mobileV || item.mobileH) {
                    window.CapecodJsonManager.ResizablesManager.add(obj, item);
                }
            }
        }
    },
    generateSelector: function (id, number, alt, sources, x, y, sx, sy, r, font, color, noText, item, parent) {
        var selectorsSpriteSheet;
        //definisco sources
        if (sources) {
            //uso quelle passate dal capecod
            selectorsSpriteSheet = window.CapecodJsonManager.whichSpriteSheet(sources[0], 0, false);
        } else {
            var num = number;
            if (alt) {
                num = alt;
            }
            var numSSiD = window.CapecodJsonManager.whichSpriteSheet("l_" + ((num < 10) ? "0" : "") + num + "_off", 0, false);
            if (numSSiD < 0) {
                //defaults in base a sx o dx
                if (x > 0) sources = ["l_right_on", "l_right_off"]
                else sources = ["l_left_on", "l_left_off"];
                selectorsSpriteSheet = window.CapecodJsonManager.whichSpriteSheet(sources[0], 0, false);
            } else {
                //usa numerica
                sources = ["l_" + ((num < 10) ? "0" : "") + num + "_on", "l_" + ((num < 10) ? "0" : "") + num + "_off"];
                selectorsSpriteSheet = numSSiD;
            }
        }
        //contenitore singolo selector
        var selectorContainer = new createjs.Container();
        window.CapecodJsonManager._selectorsContainer.addChild(selectorContainer);
        window.CapecodJsonManager._selectorsContainer[id] = selectorContainer;
        //bg
        var tmpBgCont = new createjs.Container();
        tmpBgCont.name = "bg-" + id;
        selectorContainer.addChild(tmpBgCont);

        var tmpBgDisabled = new createjs.Sprite(window.capecodGameSS[selectorsSpriteSheet]);
        tmpBgDisabled.name = tmpBgCont.name + "-Disabled";
        tmpBgDisabled.gotoAndStop(sources[1]);
        tmpBgDisabled.visible = false;
        tmpBgCont.addChild(tmpBgDisabled);
        var iconBounds2 = tmpBgDisabled.getBounds();
        tmpBgDisabled.regX = iconBounds2.width / 2;
        tmpBgDisabled.regY = iconBounds2.height / 2;
        tmpBgDisabled.x = tmpBgDisabled.y = 0;
        tmpBgDisabled.scaleX = tmpBgDisabled.scaleY = window.CapecodJsonManager._assetsScale;

        var tmpBgHover = new createjs.Sprite(window.capecodGameSS[selectorsSpriteSheet]);
        tmpBgHover.name = tmpBgCont.name + "-Hover";
        tmpBgCont.addChild(tmpBgHover);
        tmpBgHover.gotoAndStop(sources[0]);
        tmpBgHover.filters = [new createjs.ColorMatrixFilter(new createjs.ColorMatrix(10, 10, 0, 0))];
        var iconBounds3 = tmpBgHover.getBounds();
        tmpBgHover.cache(0, 0, iconBounds3.width, iconBounds3.height);
        tmpBgHover.regX = iconBounds3.width / 2;
        tmpBgHover.regY = iconBounds3.height / 2;
        tmpBgHover.x = tmpBgHover.y = 0;
        tmpBgHover.scaleX = tmpBgHover.scaleY = window.CapecodJsonManager._assetsScale;
        tmpBgHover.visible = false;

        var tmpBgActive = new createjs.Sprite(window.capecodGameSS[selectorsSpriteSheet]);
        tmpBgActive.name = tmpBgCont.name + "-Active";
        tmpBgActive.gotoAndStop(sources[0]);
        tmpBgActive.visible = false;
        tmpBgCont.addChild(tmpBgActive);
        var iconBounds4 = tmpBgActive.getBounds();
        tmpBgActive.regX = iconBounds4.width / 2;
        tmpBgActive.regY = iconBounds4.height / 2;
        tmpBgActive.x = tmpBgActive.y = 0;
        tmpBgActive.scaleX = tmpBgActive.scaleY = window.CapecodJsonManager._assetsScale;

        var tmpBgNormal = new createjs.Sprite(window.capecodGameSS[selectorsSpriteSheet]);
        tmpBgNormal.name = tmpBgCont.name + "-Normal";
        tmpBgNormal.gotoAndStop(sources[0]);
        tmpBgCont.addChild(tmpBgNormal);
        var iconBounds = tmpBgNormal.getBounds();
        tmpBgNormal.regX = iconBounds.width / 2;
        tmpBgNormal.regY = iconBounds.height / 2;
        tmpBgNormal.x = tmpBgNormal.y = 0;
        tmpBgNormal.scaleX = tmpBgNormal.scaleY = window.CapecodJsonManager._assetsScale;

        tmpBgCont.statusOff = tmpBgDisabled;
        tmpBgCont.statusActive = tmpBgActive;
        tmpBgCont.statusHover = tmpBgHover;
        tmpBgCont.statusOn = tmpBgNormal;
        selectorContainer.bg = tmpBgCont;

        //label
        var tmpLabelCont = new createjs.Container();
        tmpLabelCont.name = "label-" + id;
        selectorContainer.addChild(tmpLabelCont);

        var computedStates = item.states;

        if (parent.states) {
            computedStates = this.generateSelectorStates(item, parent);
        }
        
        //normal
        var tmpLabel = this.generateSelectorText(number, x, computedStates[0]);
        tmpLabel.name = tmpLabelCont.name + "-Normal";
        tmpLabel.visible = false;
        tmpLabelCont.addChild(tmpLabel);

        //disabled
        var tmpLabelDisabled = this.generateSelectorText(number, x, computedStates[1]);
        tmpLabelDisabled.name = tmpLabelCont.name + "-Disabled";
        tmpLabelDisabled.visible = false;
        tmpLabelCont.addChild(tmpLabelDisabled);

        //win
        var tmpLabelActive = this.generateSelectorText(number, x, computedStates[2]);
        tmpLabelActive.name = tmpLabelCont.name + "-Active";
        tmpLabelActive.visible = false;
        tmpLabelCont.addChild(tmpLabelActive);

        //hover
        var tmpLabelHover = this.generateSelectorText(number, x, computedStates[3]);
        tmpLabelHover.name = tmpLabelCont.name + "-Hover";
        tmpLabelHover.visible = false;
        tmpLabelCont.addChild(tmpLabelHover);

        tmpLabelCont.statusOff = tmpLabelDisabled;
        tmpLabelCont.statusActive = tmpLabelActive;
        tmpLabelCont.statusHover = tmpLabelHover;
        tmpLabelCont.statusOn = tmpLabel;

        selectorContainer.label = tmpLabelCont;

        selectorContainer.states = item.states;

        //var fontFont = "normal 20px '" + window.CapecodJsonManager.FontManager.gFontsFamilies[0] + "'";
        //if (font && font !== "") fontFont = font;
        //var fontColor = "#ffff99";
        //if (color && color !== "") fontColor = color;

        //var tt;

        //// if (fontFamily){
        //// fontFont = (fontWeight ? fontWeight : "normal")  + " " + (fontSize ? fontSize : "12px") + " '" + fontFamily + "'";
        //// }
        //if (item.style) {
        //    //gradientText
        //    tt = new GradientText(number, fontFont, fontColor, item.style);
        //} else {
        //    tt = new createjs.Text(number, fontFont, fontColor);
        //}

        //if (parent.states) {
        //    item.states = this.generateSelectorStates(item, parent);            

        //    item.fontFamily = item.states[0].fontFamily;
        //    item.fontSize = item.states[0].fontSize;
        //    item.fontWeight = item.states[0].fontWeight;
        //    item.color = item.states[0].color;
        //    item.style = item.states[0].style;
        //    item.lineHeight = item.states[0].lineHeight;
        //    item.outline = item.states[0].outline;
        //    item.textAlign = item.states[0].textAlign;
        //}

        //if (item.states) tt.states = item.states;

        //if (item.fontFamily) tt.fontFamily = item.fontFamily;
        //if (item.fontSize) tt.fontSize = item.fontSize;
        //if (item.fontWeight) tt.fontWeight = item.fontWeight;

        //if (item.lineHeight) tt.lineHeight = item.lineHeight;
        //if (item.outline) tt.outline = item.outline;
        //if (item.textAlign) tt.textAlign = item.textAlign
        //else tt.textAlign = "center";


        //window.CapecodJsonManager.FontManager.updateTextCache(tt);
        ////gfont
        //window.CapecodJsonManager.FontManager.addToFontManager(tt);

        //if (x > 0) {
        //    tt.x = window.CapecodJsonManager.selectorContentDisplaceX[1];
        //    tt.y = window.CapecodJsonManager.selectorContentDisplaceY[1];
        //} else {
        //    tt.x = window.CapecodJsonManager.selectorContentDisplaceX[0];
        //    tt.y = window.CapecodJsonManager.selectorContentDisplaceY[0];
        //}

        //tt.mouseEnabled = false;
        //tt.tickEnabled = false;

        //tt.name = "text";
        //selectorContainer.addChild(tt);
        //selectorContainer.label = tt;

        var scaledSize = { width: iconBounds.width * window.CapecodJsonManager._assetsScale, height: iconBounds.height * window.CapecodJsonManager._assetsScale };
        selectorContainer.cache(-scaledSize.width / 2, -scaledSize.height / 2, scaledSize.width, scaledSize.height);

        /*
        //HIT
        var hit = new createjs.Shape();
        var sW = 20; // CapecodJsonManager.selectorWidth;
        var sH = 14; //CapecodJsonManager.selectorHeight;
        hit.graphics.beginFill("rgba(0,0,0,1)").drawRect(-sW / 2, -sH / 2, sW, sH);
        selectorContainer.hitArea = hit;
        selectorContainer.addChild(hit);
        */

        //if (noText && noText === 1) {
        //    tt.visible = false;
        //}
        //-----------------------------------
        //POSIZIONO
        selectorContainer.x = x;
        selectorContainer.y = y;
        selectorContainer.scaleX = sx || 1;
        selectorContainer.scaleY = sy || 1;
        selectorContainer.rotation = r || 0;

        //resizables
        if (item.mobileV || item.mobileH) {
            window.CapecodJsonManager.ResizablesManager.add(selectorContainer, item);
        }

        if (window.CapecodJsonManager.isEditor && window.EditorManager && !window.EditorManager._isMobile) {
            //-----------------------------------
            //SOLO PER EDITOR
            if (tmpLabelCont.statusOn.noText != 1) tmpLabelCont.statusOn.visible = true;

            selectorContainer.addEventListener("mouseover", function (e) {
                var doIt = true;
                if (e.currentTarget.bg.statusOff && e.currentTarget.bg.statusOff.visible == true) doIt = false;
                if (doIt) {
                    if (e.currentTarget.bg.statusHover) {
                        if (e.currentTarget.label.statusHover.noText == 1) {
                            e.currentTarget.label.statusHover.visible = false;
                        } else {
                            e.currentTarget.label.statusHover.visible = true;
                        }
                        e.currentTarget.bg.statusHover.visible = true;
                        e.currentTarget.bg.statusOn.visible = false;
                        e.currentTarget.label.statusOn.visible = false;
                        e.currentTarget.bg.statusOff.visible = false;
                        e.currentTarget.label.statusOff.visible = false;
                    } else {
                        if (e.currentTarget.label.statusOn.noText == 1) {
                            e.currentTarget.label.statusOn.visible = false;
                        } else {
                            e.currentTarget.label.statusOn.visible = true;
                        }
                        e.currentTarget.bg.statusOn.visible = true;
                        e.currentTarget.bg.statusOff.visible = false;
                        e.currentTarget.label.statusOff.visible = false;
                        e.currentTarget.bg.statusHover.visible = false;
                        e.currentTarget.label.statusHover.visible = false;
                    }
                    e.currentTarget.updateCache();
                }
            });
            selectorContainer.addEventListener("mouseout", function (e) {
                var doIt = true;
                if (e.currentTarget.bg.statusOff && e.currentTarget.bg.statusOff.visible == true) doIt = false;
                if (doIt) {
                    if (e.currentTarget.label.statusOn.noText == 1) {
                        e.currentTarget.label.statusOn.visible = false;
                    } else {
                        e.currentTarget.label.statusOn.visible = true;
                    }
                    e.currentTarget.bg.statusOn.visible = true;
                    e.currentTarget.bg.statusOff.visible = false;
                    e.currentTarget.label.statusOff.visible = false;
                    e.currentTarget.bg.statusHover.visible = false;
                    e.currentTarget.label.statusHover.visible = false;
                    e.currentTarget.updateCache();
                }
            });
            selectorContainer.addEventListener("click", function (e) {
                if (e.currentTarget.bg.statusHover) e.currentTarget.bg.statusHover.visible = false;
                if (e.currentTarget.bg.statusOff) {
                    if (e.currentTarget.bg.statusOff.visible == true) {
                        if (e.currentTarget.label.statusOn.noText == 1) {
                            e.currentTarget.label.statusOn.visible = false;
                        } else {
                            e.currentTarget.label.statusOn.visible = true;
                        }
                        e.currentTarget.bg.statusOn.visible = true;
                        e.currentTarget.bg.statusOff.visible = false;
                        e.currentTarget.label.statusOff.visible = false;
                        e.currentTarget.bg.statusHover.visible = false;
                        e.currentTarget.label.statusHover.visible = false;
                    } else {
                        e.currentTarget.bg.statusOn.visible = false;
                        e.currentTarget.label.statusOn.visible = false;
                        if(e.currentTarget.label.statusOff.noText == 1) {
                            e.currentTarget.label.statusOff.visible = false;
                        } else {
                            e.currentTarget.label.statusOff.visible = true;
                        }
                        e.currentTarget.bg.statusOff.visible = true;
                        e.currentTarget.bg.statusHover.visible = false;
                        e.currentTarget.label.statusHover.visible = false;
                    }
                } else {
                    if (e.currentTarget.label.statusOn.noText == 1) {
                        e.currentTarget.label.statusOn.visible = false;
                    } else {
                        e.currentTarget.label.statusOn.visible = true;
                    }
                    e.currentTarget.bg.statusOn.visible = true;
                    e.currentTarget.bg.statusOff.visible = false;
                    e.currentTarget.label.statusOff.visible = false;
                    e.currentTarget.bg.statusHover.visible = false;
                    e.currentTarget.label.statusHover.visible = false;
                }
                e.currentTarget.updateCache();
            });
        }
    },

    generateSelectorText: function(number, x, state){
        var tmpLabel;
        var font = (state.fontWeight ? state.fontWeight : "normal") + " " + (state.fontSize ? state.fontSize : "12px") + " '" + state.fontFamily + "'";

        if (state.style && !jQuery.isEmptyObject(state.style)) {
            tmpLabel = new GradientText(number, font, state.color, state.style);
        } else {
            tmpLabel = new createjs.Text(number, font, state.color);
        }

        if (state.fontFamily) tmpLabel.fontFamily = state.fontFamily;
        if (state.fontSize) tmpLabel.fontSize = state.fontSize;
        if (state.fontWeight) tmpLabel.fontWeight = state.fontWeight;

        if (state.lineHeight) tmpLabel.lineHeight = state.lineHeight;
        if (state.outline) tmpLabel.outline = state.outline;
        if (state.textAlign) tmpLabel.textAlign = state.textAlign;
        else tmpLabel.textAlign = "center";

        tmpLabel.textBaseline = "alphabetic";

        if (state.noText == 1) tmpLabel.noText = 1;

        if (state.shadow && !jQuery.isEmptyObject(state.shadow)) {
            var x = 0;
            var y = 0;
            var blur = 2;
            var color = "black";

            if (state.shadow["x"]) x = state.shadow["x"];
            if (state.shadow["y"]) y = state.shadow["y"];

            if (state.shadow["x"] && !state.shadow["y"] && state.shadow["y"] != 0) y = x;
            if (state.shadow["y"] && !state.shadow["x"] && state.shadow["x"] != 0) x = y;

            if (state.shadow["blur"]) blur = state.shadow["blur"];
            if (state.shadow["color"]) color = state.shadow["color"];

            tmpLabel.shadow = new createjs.Shadow(color, x, y, blur);
            //window.CapecodJsonManager.applyShadow(tmpLabel, [color, x, y, blur]);
        } else {
            tmpLabel.shadow = null;
            //window.CapecodJsonManager.applyShadow(tmpLabel, null);
        }

        window.CapecodJsonManager.FontManager.updateTextCache(tmpLabel, true);
        window.CapecodJsonManager.FontManager.addToFontManager(tmpLabel);

        if (x > 0) {
            tmpLabel.x = window.CapecodJsonManager.selectorContentDisplaceX[1];
            tmpLabel.y = window.CapecodJsonManager.selectorContentDisplaceY[1];
        } else {
            tmpLabel.x = window.CapecodJsonManager.selectorContentDisplaceX[0];
            tmpLabel.y = window.CapecodJsonManager.selectorContentDisplaceY[0];
        }

        tmpLabel.tickEnabled = false;
        //tmpLabel.mouseEnabled = false;

        return tmpLabel;
    },

    generateSelectorStates: function (item, parent, state) {
        var color = "black";
        var fontFamily = "comic sans";
        var fontWeight = "normal";
        var fontSize = "12px";
        var style;
        var lineHeight;
        var outline;
        var textAlign;
        var noText = 0;
        var shadow;

        var tmpColor;
        var tmpFontFamily;
        var tmpFontWeight;
        var tmpFontSize;
        var tmpStyle;
        var tmpLineHeight;
        var tmpOutline;
        var tmpTextAlign;
        var tmpNoText;
        var tmpShadow;

        var states = []; //0 normal - 1 off - 2 win - 3 hover

        if (parent.states) {
            var aNormal = parent.states.filter(function (e) { return e.id == "normal" });
            var aOff = parent.states.filter(function (e) { return e.id == "off" });
            var aWin = parent.states.filter(function (e) { return e.id == "win" });
            var aHover = parent.states.filter(function (e) { return e.id == "hover" });

            if (!state || state == "normal") {

                if (aNormal && aNormal.length == 1) {
                    var normal = aNormal[0];
                    color = normal.color;
                    fontFamily = normal.fontFamily;
                    fontWeight = normal.fontWeight;
                    fontSize = normal.fontSize;
                    style = normal.style;
                    lineHeight = normal.lineHeight;
                    outline = normal.outline;
                    textAlign = normal.textAlign;
                    noText = normal.noText;
                    shadow = normal.shadow;
                }

                if (item && item.states) {
                    var aoNormal = item.states.filter(function (e) { return e.id == "normal" });
                    if (aoNormal && aoNormal.length == 1) {
                        var oNormal = aoNormal[0];
                        if (oNormal.color) color = oNormal.color;
                        if (oNormal.fontFamily) fontFamily = oNormal.fontFamily;
                        if (oNormal.fontWeight) fontWeight = oNormal.fontWeight;
                        if (oNormal.fontSize) fontSize = oNormal.fontSize;
                        if (oNormal.style) style = oNormal.style;
                        if (oNormal.lineHeight) lineHeight = oNormal.lineHeight;
                        if (oNormal.outline) outline = oNormal.outline;
                        if (oNormal.textAlign) textAlign = oNormal.textAlign;
                        if (oNormal.noText) noText = oNormal.noText;
                        if (oNormal.shadow) shadow = oNormal.shadow;
                    }
                }

                //normal 0
                states.push({
                    id: "normal",
                    fontFamily: fontFamily,
                    fontSize: fontSize,
                    fontWeight: fontWeight,
                    color: color,
                    style: style,
                    lineHeight: lineHeight,
                    outline: outline,
                    textAlign: textAlign,
                    noText: noText,
                    shadow: shadow,
                });

            }

            tmpColor = color;
            tmpFontFamily = fontFamily;
            tmpFontWeight = fontWeight;
            tmpFontSize = fontSize;
            tmpStyle = style;
            tmpLineHeight = lineHeight;
            tmpOutline = outline;
            tmpTextAlign = textAlign;
            tmpNoText = noText;
            tmpShadow = shadow;

            if (!state || state == "off") {

                if (aOff && aOff.length == 1) {
                    var off = aOff[0];
                    if (off.color) tmpColor = off.color;
                    if (off.fontFamily) tmpFontFamily = off.fontFamily;
                    if (off.fontWeight) tmpFontWeight = off.fontWeight;
                    if (off.fontSize) tmpFontSize = off.fontSize;
                    if (off.style) tmpStyle = off.style;
                    if (off.lineHeight) tmpLineHeight = off.lineHeight;
                    if (off.outline) tmpOutline = off.outline;
                    if (off.textAlign) tmpTextAlign = off.textAlign;
                    if (off.noText) tmpNoText = off.noText;
                    if (off.shadow) tmpShadow = off.shadow;
                }

                if (item && item.states) {
                    var aoOff = item.states.filter(function (e) { return e.id == "off" });
                    if (aoOff && aoOff.length == 1) {
                        var oOff = aoOff[0];
                        if (oOff.color) tmpColor = oOff.color;
                        if (oOff.fontFamily) tmpFontFamily = oOff.fontFamily;
                        if (oOff.fontWeight) tmpFontWeight = oOff.fontWeight;
                        if (oOff.fontSize) tmpFontSize = oOff.fontSize;
                        if (oOff.style) tmpStyle = oOff.style;
                        if (oOff.lineHeight) tmpLineHeight = oOff.lineHeight;
                        if (oOff.outline) tmpOutline = oOff.outline;
                        if (oOff.textAlign) tmpTextAlign = oOff.textAlign;
                        if (oOff.noText) tmpNoText = oOff.noText;
                        if (oOff.shadow) tmpShadow = oOff.shadow;
                    }
                }

                //off 1
                states.push({
                    id: "off",
                    fontFamily: tmpFontFamily,
                    fontSize: tmpFontSize,
                    fontWeight: tmpFontWeight,
                    color: tmpColor,
                    style: tmpStyle,
                    lineHeight: tmpLineHeight,
                    outline: tmpOutline,
                    textAlign: tmpTextAlign,
                    noText: tmpNoText,
                    shadow: tmpShadow,
                });

            }

            tmpColor = color;
            tmpFontFamily = fontFamily;
            tmpFontWeight = fontWeight;
            tmpFontSize = fontSize;
            tmpStyle = style;
            tmpLineHeight = lineHeight;
            tmpOutline = outline;
            tmpTextAlign = textAlign;
            tmpNoText = noText;
            tmpShadow = shadow;

            if (!state || state == "win") {

                if (aWin && aWin.length == 1) {
                    var win = aWin[0];
                    if (win.color) tmpColor = win.color;
                    if (win.fontFamily) tmpFontFamily = win.fontFamily;
                    if (win.fontWeight) tmpFontWeight = win.fontWeight;
                    if (win.fontSize) tmpFontSize = win.fontSize;
                    if (win.style) tmpStyle = win.style;
                    if (win.lineHeight) tmpLineHeight = win.lineHeight;
                    if (win.outline) tmpOutline = win.outline;
                    if (win.textAlign) tmpTextAlign = win.textAlign;
                    if (win.noText) tmpNoText = win.noText;
                    if (win.shadow) tmpShadow = win.shadow;
                }

                if (item && item.states) {
                    var aoWin = item.states.filter(function (e) { return e.id == "win" });
                    if (aoWin && aoWin.length == 1) {
                        var oWin = aoWin[0];
                        if (oWin.color) tmpColor = oWin.color;
                        if (oWin.fontFamily) tmpFontFamily = oWin.fontFamily;
                        if (oWin.fontWeight) tmpFontWeight = oWin.fontWeight;
                        if (oWin.fontSize) tmpFontSize = oWin.fontSize;
                        if (oWin.style) tmpStyle = oWin.style;
                        if (oWin.lineHeight) tmpLineHeight = oWin.lineHeight;
                        if (oWin.outline) tmpOutline = oWin.outline;
                        if (oWin.textAlign) tmpTextAlign = oWin.textAlign;
                        if (oWin.noText) tmpNoText = oWin.noText;
                        if (oWin.shadow) tmpShadow = oWin.shadow;
                    }
                }

                //win 2
                states.push({
                    id: "win",
                    fontFamily: tmpFontFamily,
                    fontSize: tmpFontSize,
                    fontWeight: tmpFontWeight,
                    color: tmpColor,
                    style: tmpStyle,
                    lineHeight: tmpLineHeight,
                    outline: tmpOutline,
                    textAlign: tmpTextAlign,
                    noText: tmpNoText,
                    shadow: tmpShadow,
                });

            }

            tmpColor = color;
            tmpFontFamily = fontFamily;
            tmpFontWeight = fontWeight;
            tmpFontSize = fontSize;
            tmpStyle = style;
            tmpLineHeight = lineHeight;
            tmpOutline = outline;
            tmpTextAlign = textAlign;
            tmpNoText = noText;
            tmpShadow = shadow;

            if (!state || state == "hover") {

                if (aHover && aHover.length == 1) {
                    var hover = aHover[0];
                    if (hover.color) tmpColor = hover.color;
                    if (hover.fontFamily) tmpFontFamily = hover.fontFamily;
                    if (hover.fontWeight) tmpFontWeight = hover.fontWeight;
                    if (hover.fontSize) tmpFontSize = hover.fontSize;
                    if (hover.style) tmpStyle = hover.style;
                    if (hover.lineHeight) tmpLineHeight = hover.lineHeight;
                    if (hover.outline) tmpOutline = hover.outline;
                    if (hover.textAlign) tmpTextAlign = hover.textAlign;
                    if (hover.noText) tmpNoText = hover.noText;
                    if (hover.shadow) tmpShadow = hover.shadow;
                }

                if (item.states) {
                    var aoHover = item.states.filter(function (e) { return e.id == "hover" });
                    if (aoHover && aoHover.length == 1) {
                        var oHover = aoHover[0];
                        if (oHover.color) tmpColor = oHover.color;
                        if (oHover.fontFamily) tmpFontFamily = oHover.fontFamily;
                        if (oHover.fontWeight) tmpFontWeight = oHover.fontWeight;
                        if (oHover.fontSize) tmpFontSize = oHover.fontSize;
                        if (oHover.style) tmpStyle = oHover.style;
                        if (oHover.lineHeight) tmpLineHeight = oHover.lineHeight;
                        if (oHover.outline) tmpOutline = oHover.outline;
                        if (oHover.textAlign) tmpTextAlign = oHover.textAlign;
                        if (oHover.noText) tmpNoText = oHover.noText;
                        if (oHover.shadow) tmpShadow = oHover.shadow;
                    }
                }

                //hover 3
                states.push({
                    id: "hover",
                    fontFamily: tmpFontFamily,
                    fontSize: tmpFontSize,
                    fontWeight: tmpFontWeight,
                    color: tmpColor,
                    style: tmpStyle,
                    lineHeight: tmpLineHeight,
                    outline: tmpOutline,
                    textAlign: tmpTextAlign,
                    noText: tmpNoText,
                    shadow: tmpShadow,
                });

            }
        } else {
            if (!state) {
                states.push({
                    id: "normal",
                    fontFamily: fontFamily,
                    fontSize: fontSize,
                    fontWeight: fontWeight,
                    color: color,
                    style: style,
                    lineHeight: lineHeight,
                    outline: outline,
                    textAlign: textAlign,
                    noText: noText,
                    shadow: shadow,
                });
                states.push({
                    id: "off",
                    fontFamily: fontFamily,
                    fontSize: fontSize,
                    fontWeight: fontWeight,
                    color: color,
                    style: style,
                    lineHeight: lineHeight,
                    outline: outline,
                    textAlign: textAlign,
                    noText: noText,
                    shadow: shadow,
                });
                states.push({
                    id: "win",
                    fontFamily: fontFamily,
                    fontSize: fontSize,
                    fontWeight: fontWeight,
                    color: color,
                    style: style,
                    lineHeight: lineHeight,
                    outline: outline,
                    textAlign: textAlign,
                    noText: noText,
                    shadow: shadow,
                });
                states.push({
                    id: "hover",
                    fontFamily: fontFamily,
                    fontSize: fontSize,
                    fontWeight: fontWeight,
                    color: color,
                    style: style,
                    lineHeight: lineHeight,
                    outline: outline,
                    textAlign: textAlign,
                    noText: noText,
                    shadow: shadow,
                });
            }
        }

        return states;
    },

    generateReel: function (id, x, y, sx, sy, r, item) {
        var symbolsSpriteSheet = window.CapecodJsonManager.whichSpriteSheet("s00");  //quella dei symbols
        //-----------------------------------
        var startX = 0;
        var startY = window.CapecodJsonManager.iconHeight;
        //creazione
        var reelContainer = new createjs.Container();
        reelContainer.name = id;
        window.CapecodJsonManager._reelsContainer.addChild(reelContainer);
        //window.CapecodJsonManager._reelsContainer["reel" + id] = reelContainer;

        switch (id) {
            case "1":
                window.CapecodJsonManager._reelsContainer.reel1 = reelContainer;
                break;
            case "2":
                window.CapecodJsonManager._reelsContainer.reel2 = reelContainer;
                break;
            case "3":
                window.CapecodJsonManager._reelsContainer.reel3 = reelContainer;
                break;
            case "4":
                window.CapecodJsonManager._reelsContainer.reel4 = reelContainer;
                break;
            case "5":
                window.CapecodJsonManager._reelsContainer.reel5 = reelContainer;
                break;
            case "6":
                window.CapecodJsonManager._reelsContainer.reel6 = reelContainer;
                break;
            case "7":
                window.CapecodJsonManager._reelsContainer.reel7 = reelContainer;
                break;
            case "8":
                window.CapecodJsonManager._reelsContainer.reel8 = reelContainer;
                break;
            case "9":
                window.CapecodJsonManager._reelsContainer.reel9 = reelContainer;
                break;
            case "10":
                window.CapecodJsonManager._reelsContainer.reel10 = reelContainer;
                break;
        }

        var iconsContainer = new createjs.Container();
        iconsContainer.x = startX;
        iconsContainer.y = startY;
        reelContainer.addChild(iconsContainer);
        reelContainer.iconsContainer = iconsContainer;

        if (window.CapecodJsonManager.isEditor) {
            //-----------------------------------
            //SOLO PER EDITOR
            var scrollerInstance = new createjs.Container();
            scrollerInstance.x = startX;
            scrollerInstance.y = startY;
            reelContainer.addChild(scrollerInstance);
            reelContainer["toScroll"] = scrollerInstance;

            //setup matrix
            var matrics = [];
            var randomics = [];
            var i;
            for (i = 0; i < window.CapecodJsonManager.rows; i++) matrics.push(getRandomIcon()); //x righe
            for (i = 0; i < window.CapecodJsonManager.rows; i++) randomics.push(getRandomIcon()); //x righe
            var slotstart = window.CapecodJsonManager.rows - 1;
            var ss = 0;
            for (ss = 0; ss < window.CapecodJsonManager.rows; ss++) { //x righe
                var tmpBmp = new createjs.Sprite(window.capecodGameSS[symbolsSpriteSheet]);
                tmpBmp.gotoAndStop("s" + matrics[ss]);
                tmpBmp.scaleX = tmpBmp.scaleY = window.CapecodJsonManager._assetsScale;
                tmpBmp.x = 0;
                tmpBmp.y = (window.CapecodJsonManager.iconHeight * slotstart);
                scrollerInstance.addChild(tmpBmp);
                slotstart--;
            }
            for (ss = 0; ss < window.CapecodJsonManager.rows; ss++) { //x righe
                var tmpBmp2 = new createjs.Sprite(window.capecodGameSS[symbolsSpriteSheet]);
                tmpBmp2.gotoAndStop("s" + randomics[ss]);
                tmpBmp2.scaleX = tmpBmp2.scaleY = window.CapecodJsonManager._assetsScale;
                tmpBmp2.x = 0;
                tmpBmp2.y = (window.CapecodJsonManager.iconHeight * slotstart);
                scrollerInstance.addChild(tmpBmp2);
                slotstart--;
            }
            for (ss = 0; ss < window.CapecodJsonManager.rows; ss++) { //x righe
                var tmpBmp3 = new createjs.Sprite(window.capecodGameSS[symbolsSpriteSheet]);
                tmpBmp3.gotoAndStop("s" + matrics[ss]);
                tmpBmp3.scaleX = tmpBmp3.scaleY = window.CapecodJsonManager._assetsScale;
                tmpBmp3.x = 0;
                tmpBmp3.y = (window.CapecodJsonManager.iconHeight * slotstart);
                scrollerInstance.addChild(tmpBmp3);
                slotstart--;
            }
            //-----------------------------------
            //FILTRO SU TUTTO e CACHEMASK
            if (window.CapecodJsonManager._initialBlur && window.CapecodJsonManager._initialBlur.length == 3) {
                var blurFilter = new createjs.BlurFilter(window.CapecodJsonManager._initialBlur[0], window.CapecodJsonManager._initialBlur[1], window.CapecodJsonManager._initialBlur[2]);
                scrollerInstance.filters = [blurFilter];
            }
            //cache
            scrollerInstance.cache(0, -window.CapecodJsonManager.iconHeight * window.CapecodJsonManager.rows * 2, window.CapecodJsonManager.iconWidth, window.CapecodJsonManager.iconHeight * window.CapecodJsonManager.rows * 3);
            //scrollerInstance.removeAllChildren();

        }
        //-----------------------------------
        //POSIZIONO
        reelContainer.cache(0, 0, window.CapecodJsonManager.iconWidth, window.CapecodJsonManager.iconHeight * window.CapecodJsonManager.rows);
        reelContainer.regX = window.CapecodJsonManager.iconWidth / 2; //icon width/2
        reelContainer.regY = window.CapecodJsonManager.iconHeight * window.CapecodJsonManager.rows * 0.5; //icon height * rows / 2
        reelContainer.x = x;
        reelContainer.y = y;

        reelContainer.originalX = x ? x : 0;
        reelContainer.originalY = y ? y : 0;

        reelContainer.scaleX = sx;
        reelContainer.scaleY = sy;
        reelContainer.rotation = r;

        //resizables
        if (item.mobileV || item.mobileH) {
            window.CapecodJsonManager.ResizablesManager.add(reelContainer, item);
        }

        //-----------------------------------
        function getRandomIcon() {
            var nn = Math.floor(Math.random() * 12.9999999); //genero icone da 0 a 12
            return ((nn < 10) ? "0" : "") + nn;
        }
    },
    generateSymbolsExport: function (cont, filterOn, filterOff, filterBlur, filterBlur2) {
        var useConfigSymbolsItems;
        var doExe = true;
        if (!window.config.symbols) {
            doExe = false;
        } else if (!window.config.symbols.items) {
            doExe = false;
        } else {
            //faccio come modelDedicated.initConfigs
            var symbolItems = window.config.symbols_list;
            symbolItems.sort(function (obj1, obj2) {
                return obj1.id - obj2.id;
            });
            window.config.symbols = { items: window.CapecodJsonManager.removeDuplicatesInArray(symbolItems, "id") };
            //faccio come modelDedicated.initGameVariables
            var expandStartingId = window.config.symbols.items.length;
            for (var i in window.config.symbols.items) {
                if (i.type == 9) { //Wild
                    i.expandable = 1;
                }
            }
        }
        if (doExe) useConfigSymbolsItems = window.config.symbols.items;
        //analizzo gli expandables se presenti
        var expandables = [];
        var expandStartingId = useConfigSymbolsItems.length;
        var i = 0;
        for (i = 0; i < useConfigSymbolsItems.length; i++) {
            var it = useConfigSymbolsItems[i];
            if (it.expandable) {
                var toPush = [];
                //-> deve generare texture realtime spezzettando il primo frame di: "e" + it.id + "_loop_anim";
                for (var e = 0; e < window.CapecodJsonManager.rows; e++) {
                    toPush.push({ id: expandStartingId + e, source: "e" + ((it.id < 10) ? "0" : "") + it.id + "_loop_anim" });
                }
                expandStartingId += window.CapecodJsonManager.rows;
                expandables.push(toPush);
            }
        }
        if (doExe) {
            //definisco il json da generare
            var symbolsExportJson = [
                {
                    "editor": {
                        "required": 1
                    },
                    "id": "statusOff",
                    "type": "Container",
                    "children": []
                },
                {
                    "editor": {
                        "required": 1
                    },
                    "id": "statusOn",
                    "type": "Container",
                    "children": []
                },
                {
                    "editor": {
                        "required": 1
                    },
                    "id": "statusWin",
                    "type": "Container"
                }
            ];
            //on e off li fa sempre, se c'e' filters li applica
            if (!filterOff) filterOff = window.CapecodJsonManager.defaultFilterOff;
            for (var c = 0; c < symbolsExportJson.length; c++) {
                var child = symbolsExportJson[c];
                if (child.id == "statusOff") {
                    var contOff = child.children;
                    for (i = 0; i < useConfigSymbolsItems.length; i++) {
                        var it2 = useConfigSymbolsItems[i];
                        contOff.push(createSymbolItem(it2.id, sourceFromId(it2), filterOff, 0, "off"));
                    }
                    createExpandables(contOff, filterOff);
                }
                if (child.id == "statusOn") {
                    var contOn = child.children;
                    for (i = 0; i < useConfigSymbolsItems.length; i++) {
                        var it3 = useConfigSymbolsItems[i];
                        contOn.push(createSymbolItem(it3.id, sourceFromId(it3), filterOn, 0, "on"));
                    }
                    createExpandables(contOn, filterOn);
                }
            }
            //se ci sono filtersBlur e filtersBlur2 li crea, fa i figli, e li aggiunge in testa a  symbolsExportJson
            if (filterBlur) {
                var contBlur = {
                    "id": "statusBlur",
                    "type": "Container",
                    "children": []
                }
                for (i = 0; i < useConfigSymbolsItems.length; i++) {
                    var it4 = useConfigSymbolsItems[i];
                    contBlur.children.push(createSymbolItem(it4.id, sourceFromId(it4), filterBlur, 0, "blur"));
                }
                createExpandables(contBlur.children, filterBlur);
                symbolsExportJson.unshift(contBlur);
            }
            if (filterBlur2) {
                var contBlur2 = {
                    "id": "statusBlur2",
                    "type": "Container",
                    "children": []
                }
                for (i = 0; i < useConfigSymbolsItems.length; i++) {
                    var it5 = useConfigSymbolsItems[i];
                    contBlur2.children.push(createSymbolItem(it5.id, sourceFromId(it5), filterBlur2, 0, "blur2"));
                }
                createExpandables(contBlur2.children, filterBlur2);
                symbolsExportJson.unshift(contBlur2);
            }
            //genero il tutto
            //window.CapecodJsonManager.logTreeJson(symbolsExportJson, "json");
            window.CapecodJsonManager.generateFromJson(symbolsExportJson, cont);
            //window.CapecodJsonManager.logTreeCanvas(cont, "canvas");
        } else {
            window.CapecodJsonManager.LogManager.logC("CapecodJsonManager", "ERRORE SYMBOLSEXPORT -> DATI IN CONFIG INCONGRUENTI!", "error");
        }
        function createSymbolItem(id, source, filter, cuttingRule, what) {
            var idToUse = "s" + ((id < 10) ? "0" : "") + id;
            var filterToUse = filter;
            if (what == "off") {
                //Se esiste off
                var testSource = "s" + ((id < 10) ? "0" : "") + id + "_off";
                var offExists = window.CapecodJsonManager.assetExists(testSource);
                if (offExists) {
                    source = testSource;
                    filterToUse = null;
                }
            }

            if (what == "blur") {
                //Se esiste blur
                var testSource = "s" + ((id < 10) ? "0" : "") + id + "_blur";
                var blurExists = window.CapecodJsonManager.assetExists(testSource);
                if (blurExists) {
                    source = testSource;
                    filterToUse = null;
                }
            }

            var toIns = {
                "id": idToUse,
                "type": "Sprite",
                "source": source
            };
            if (filterToUse) {
                toIns["filters"] = filterToUse;
            }

            if (cuttingRule > 0) {
                //var refers = window.CapecodJsonManager._reelsContainer;
                //var correctionY = refers.getChildAt(0).y;

                var iconExpW = window.CapecodJsonManager.iconWidth;
                var offsetX = window.CapecodJsonManager.jExpAnimationWidthOffset;
                // if(window.CapecodJsonManager.jExpAnimationWidth > 0){
                //     offsetX = (window.CapecodJsonManager.jExpAnimationWidth - window.CapecodJsonManager.iconWidth) / 2;
                // }

                var iconExpH = window.CapecodJsonManager.iconHeight;
                var offsetY = window.CapecodJsonManager.jExpAnimationHeightOffset;
                // if(window.CapecodJsonManager.jExpAnimationHeight > 0){
                //     offsetY = (window.CapecodJsonManager.jExpAnimationHeight - (window.CapecodJsonManager.iconHeight*3)) / 2;
                // }

                toIns["mask"] = [offsetX, iconExpH * (cuttingRule - 1) + offsetY, iconExpW, iconExpH];
                // toIns["mask"] = [0, window.CapecodJsonManager.iconHeight * (cuttingRule - 1), window.CapecodJsonManager.iconWidth, window.CapecodJsonManager.iconHeight];
            }
            return toIns;
        };
        function createExpandables(cont, filter) {
            for (var t = 0; t < expandables.length; t++) {
                var tris = expandables[t];
                for (var i = 0; i < tris.length; i++) {
                    var it = tris[i];
                    cont.push(createSymbolItem(it.id, it.source, filter, i + 1, "expand"));
                }
            }
        };
        function sourceFromId(item) {
            var id = item.id;
            //if (item["null"] && item["null"] == true) id--;
            var source = "s" + ((id < 10) ? "0" : "") + id;
            return source;
        };
    },
    //---
    //update
    updateReelBlur: function (reelContainer, bx, by) {
        var blurFilter = new createjs.BlurFilter(bx, by);
        reelContainer["toScroll"].filters = [blurFilter];
        //cache
        reelContainer["toScroll"].updateCache();
        reelContainer.updateCache();
    },
    updateJson: function (jsonToUpdate, objectToUpdate, objPath) {
        var safeProp = ["id", "type", "name", "kind", "editor", "path", "children"];
        processJson(jsonToUpdate, objectToUpdate, objPath);
        function processJson(children, objectToUpdate, objPath) {
            for (var i = 0, len = children.length; i < len; i++) {
                var item = children[i];
                if (item.path === objPath /*&& item.id === objectToUpdate.id*/) {
                    //update this
                    for (var attr in objectToUpdate) {
                        item[attr] = objectToUpdate[attr];
                    }
                    for (var itemAttr in item) {
                        if (safeProp.indexOf(itemAttr) == -1 && typeof (objectToUpdate[itemAttr]) === "undefined")
                            item[itemAttr] = undefined;
                    }
                } else {
                    //se ha children
                    if (item.children) {
                        processJson(item.children, objectToUpdate, objPath);
                    }
                }
            }
        }
    },
    applyText: function (sprite, parameters) {
        var modified = false;
        if (parameters) {
            //cambia
            if (parameters.text) {
                sprite.text = parameters.text;
                modified = true;
            }
            if (parameters.fontWeight) {
                sprite.fontWeight = parameters.fontWeight;
                modified = true;
            }
            if (parameters.fontSize) {
                sprite.fontSize = parameters.fontSize;
                modified = true;
            }
            if (parameters.fontFamily) {
                sprite.fontFamily = parameters.fontFamily;
                modified = true;
            }
            if (parameters.font) {
                sprite.font = parameters.font
                modified = true;
            }
            if (parameters.color) {
                sprite.color = parameters.color;
                modified = true;
            }
            if (parameters.lineHeight) {
                sprite.lineHeight = parameters.lineHeight;
                modified = true;
            }
            if (parameters.outline) {
                sprite.outline = parameters.outline;
                modified = true;
            }
            if (parameters.textAlign) {
                sprite.textAlign = parameters.textAlign;
                modified = true;
            }
            if (parameters.style) {
                sprite.style = parameters.style;
                modified = true;
            }
        }
        //se deve rigenera la cache stando attenti anche alla mask eventuale e shadow ----------------------------------------------------
        if (modified) {
            window.CapecodJsonManager.FontManager.updateTextCache(sprite);
        }
    },
    applyShadow: function (sprite, parameters) {
        var modified = false;
        if (parameters) {
            //fa l'ombra
            sprite.shadow = new createjs.Shadow(parameters[0], parameters[1], parameters[2], parameters[3]);
            modified = true;
        } else {
            //se cel'aveva la toglie
            if (sprite.shadow) {
                sprite.shadow = null;
                modified = true;
            }
        }
        //se deve rigenera la cache stando attenti anche alla mask eventuale ----------------------------------------------------
        if (modified) {
            if (sprite.cacheCanvas) {
                sprite.updateCache();
            } else {
                var cacheRect = sprite.getBounds();
                if (cacheRect) {
                    cacheRect.width *= window.CapecodJsonManager._assetsScale; // AGGIUNTA RIGA DOPO
                    cacheRect.height *= window.CapecodJsonManager._assetsScale; //*** AGGIUNTA RIGA  - DA VERIFICARE
                    sprite.cache(cacheRect.x - 30, cacheRect.y - 30, cacheRect.width + 60, cacheRect.height + 60);
                    var oldX = sprite.x;
                    var oldY = sprite.y;
                    sprite.regX = (cacheRect.width + 60) / 2;
                    sprite.regY = (cacheRect.height + 60) / 2;
                    sprite.x = oldX;
                    sprite.y = oldY;
                }
            }
        }
    },
    applyFilters: function (sprite, parameters) {
        var modified = false;
        if (parameters) {
            //fa filtri in base al tipo
            sprite.filters = [];
            for (var i = 0; i < parameters.length; i++) {
                var useParams = parameters[i];
                var type = useParams[0];
                var params = useParams[1];
                var newFilter;
                if (type == "Blur") {
                    newFilter = new createjs.BlurFilter(params[0], params[1], params[2]);
                } else if (type == "ColorMatrix") {
                    newFilter = new createjs.ColorMatrixFilter(new createjs.ColorMatrix(params[0], params[1], params[2], params[3]));
                } else if (type == "Color") {
                    newFilter = new createjs.ColorFilter(params[0], params[1], params[2], params[3], params[4], params[5], params[6], params[7]);
                } else if (type == "AlphaMask") {
                    var spriteOfMask = null; // in params[0] c'e' l'id del fratello
                    newFilter = new createjs.AlphaMaskFilter(spriteOfMask);
                }
                sprite.filters.push(newFilter);
            }
            modified = true;
        } else {
            //se cel'aveva li toglie
            if (sprite.filters && sprite.filters.length > 0) {
                modified = true;
            }
            sprite.filters = [];
        }

        //se deve rigenera la cache stando attenti anche alla mask eventuale ----------------------------------------------------
        if (modified) {
            if (sprite.cacheCanvas) {
                sprite.updateCache();
            } else {
                var cacheRect = sprite.getBounds();
                //cacheRect.width *= window.CapecodJsonManager._assetsScale; //*** DA VERIFICARE
                //cacheRect.height *= window.CapecodJsonManager._assetsScale; //*** AGGIUNTA RIGA  - DA VERIFICARE
                var outerSpace = 0;
                if (type == "Blur") {
                    outerSpace = 30;
                    var bb = sprite.filters[0].getBounds();
                    if (bb) outerSpace = Math.max(bb.width - cacheRect.width, bb.height - cacheRect.height);
                }
                //sprite.cache(cacheRect.x - outerSpace, cacheRect.y - outerSpace, cacheRect.width + outerSpace + outerSpace, cacheRect.height + outerSpace + outerSpace);
                sprite.cache(cacheRect.x, cacheRect.y, cacheRect.width, cacheRect.height);
                var oldX = sprite.x;
                var oldY = sprite.y;
                sprite.regX = (cacheRect.width) / 2;//outerSpace + (cacheRect.width) / 2;
                sprite.regY = (cacheRect.height) / 2;//outerSpace + (cacheRect.height) / 2;
                sprite.x = oldX;
                sprite.y = oldY;
            }
        }
    },
    //---
    //preprocess
    addPathToJson: function (children, fullPath) {
        for (var i = 0, len = children.length; i < len; i++) {
            var item = children[i];
            if (fullPath !== "") item.path = fullPath + "." + item.id;
            if (item.children) {
                window.CapecodJsonManager.addPathToJson(item.children, item.path);
            }
        }
        window.CapecodJsonManager._pathAdded = true;
    },
    //---
    //log
    logTreeJson: function (children, fullPath) {
        for (var i = 0, len = children.length; i < len; i++) {
            var item = children[i];
            var path = item.id
            if (fullPath !== "") path = fullPath + "." + path;
            if (item.children) {
                window.CapecodJsonManager.logTreeJson(item.children, path);
            }
        }
    },
    logTreeCanvas: function (cont, fullPath) {
        if (cont.children) {
            logTreeCanvas_inner(cont.children, fullPath);
        } else {
            logTreeCanvas_inner(cont, fullPath);
        }
        function logTreeCanvas_inner(children, fullPath) {
            for (var i = 0, len = children.length; i < len; i++) {
                var item = children[i];
                var path = item.name
                if (fullPath !== "") path = fullPath + "." + path;
                if (item.children) {
                    logTreeCanvas_inner(item.children, path);
                }
            }
        }
    },
    //---
    //validate
    validate: function (jsonn, parent) {
        window.CapecodJsonManager._validationResults = {
            errors: [],
            warnings: [],
            reelsContainer: false,
            selectorsContainer: false,
            exportRoot: false,
            result: true
        };
        if (!window.CapecodJsonManager._pathAdded) window.CapecodJsonManager.addPathToJson(jsonn, window.CapecodJsonManager.defaultExportRoot);
        window.CapecodJsonManager.validateLoop(jsonn, parent);
    },
    validateLoop: function (children, parent) {
        for (var i = 0, len = children.length; i < len; i++) {
            var item = children[i];
            if (item.type == "Sprite") {
                var asset = item.source;
                asset = window.CapecodJsonManager.bestAssetName(asset);
                var ssID = window.CapecodJsonManager.whichSpriteSheet(asset, 0);
                if (ssID < 0) {
                    //ERROR!
                    window.CapecodJsonManager._validationResults.errors.push({ path: item.path, type: "missing source", source: asset });
                    window.CapecodJsonManager._validationResults.result = false;
                }
            } else if (item.type == "Symbols") {
                if (window.CapecodJsonManager._validationResults.exportRoot == true) {
                    window.CapecodJsonManager._validationResults.warnings.push({ path: item.path, type: "duplicate symbolsExport" });
                    //window.CapecodJsonManager._validationResults.result = false;
                }
                window.CapecodJsonManager._validationResults.exportRoot = true;
            } else if (item.type == "Container") {
                if (item.children) {
                    var haveChildrenReels = false;
                    var haveChildrenSelectors = false;
                    for (var z = 0, lenz = item.children.length; z < lenz; z++) {
                        var child = item.children[z];
                        if (child.type === "Reel") haveChildrenReels = true;
                        if (child.type === "Selector") haveChildrenSelectors = true;
                    }
                    if (haveChildrenReels) {
                        if (window.CapecodJsonManager._validationResults.reelsContainer == true) {
                            window.CapecodJsonManager.LogManager.logC("CapecodJsonManager", "MULTIPLE reelsContainer", "warn");
                            window.CapecodJsonManager._validationResults.warnings.push({ path: item.path, type: "duplicate reelsContainer" });
                            window.CapecodJsonManager._validationResults.result = false;
                        }
                        window.CapecodJsonManager._validationResults.reelsContainer = true;
                    }
                    if (haveChildrenSelectors) {
                        if (window.CapecodJsonManager._validationResults.selectorsContainer == true) {
                            window.CapecodJsonManager.LogManager.logC("CapecodJsonManager", "MULTIPLE selectorsContainer", "warn");
                            window.CapecodJsonManager._validationResults.warnings.push({ path: item.path, type: "duplicate selectorsContainer" });
                            window.CapecodJsonManager._validationResults.result = false;
                        }
                        window.CapecodJsonManager._validationResults.selectorsContainer = true;
                    }
                    //---
                }
            }

        }
    },
    //---
    //utils
    removeDuplicatesInArray: function (originalArray, prop) {
        var newArray = [];
        var lookupObject = {};

        for (var i in originalArray) {
            lookupObject[originalArray[i][prop]] = originalArray[i];
        }

        for (i in lookupObject) {
            newArray.push(lookupObject[i]);
        }
        return newArray;
    },
    //---
    //managers
    LoaderManager: {
        loaderPercentageRanges: [],
        loaderCursor: 0,
        loaderTarget: 0,
        assetsArray: [],
        images: {},
        callback: null,
        manifests: [],
        basePath: "",
        baseVersioning: "",
        defaultLoaderTimeout: 3000000,
        haltOnMissingResource: true,
        missingResx: false,
        maxConnections: -1,
        //---
        loadAssets: function (assetsArray, images, basePath, versioning, percentages, whatis, maxConns, callback, update, error) {
            this.assetsArray = assetsArray;
            this.images = images;
            this.maxConnections = maxConns;
            this.callback = callback;
            this.update = update;
            this.error = error;
            this.basePath = basePath;
            this.whatis = whatis;
            this.baseVersioning = "?v=" + versioning;
            this.loaderPercentageRanges = percentages;
            //START
            this.loaderCursor = 0;
            this.loaderTarget = this.assetsArray.length;
            window.CapecodJsonManager.LogManager.logC("CapecodJsonManager", "LOADING OF " + this.loaderTarget + " MANIFESTS OF " + this.whatis + " ASSETS STARTED...", "info");

            for (var ss = 0; ss < this.assetsArray.length; ss++) {
                var manifest = [];
                for (var ll = 0; ll < this.assetsArray[ss].images.length; ll++) {
                    var source = this.assetsArray[ss].images[ll]; //CI VA PATH PRIMA
                    var sourceVer = source;
                    var idx = sourceVer.indexOf('?');
                    if (idx >= 0)
                        source = source.substring(0, idx);
                    else
                        sourceVer += this.baseVersioning;
                    var manifestItem = { src: this.basePath + sourceVer, id: source, crossOrigin: true};
                    manifest.push(manifestItem);
                }
                this.manifests.push(manifest);
                this.assetsArray[ss].images = [];
            }

            //start
            this.load();
        },
        handleFileLoad: function (evt) {
            if (evt.item.type == "image") {
                this.images[evt.item.id] = evt.result;
            }
        },
        handleProgress: function (evt) {
            if (this.loaderPercentageRanges) {
                var percentagesRange = this.loaderPercentageRanges[this.loaderCursor];
                var progressToSend = percentagesRange[0] + (evt.progress * (percentagesRange[1] - percentagesRange[0]));
                if (this.update) this.update(progressToSend / 100);
            } else {
                var cursor = this.loaderCursor;
                if (this.update) this.update(evt.progress, this.loaderCursor);
            }
        },
        handleError: function (evt) {
            if (this.error) {
                resxUrl = "unknown";
                if (evt)
                    if (evt.data)
                        if (evt.data.src)
                            resxUrl = evt.data.src;
                this.error({ url: resxUrl, fatal: this.haltOnMissingResource });
            }
            this.missingResx = true;
        },
        load: function () {
            if (this.loaderCursor < this.loaderTarget) {
                //carico il prossimo
                var loader = new createjs.LoadQueue(false);
                createjs.LoadItem.LOAD_TIMEOUT_DEFAULT = this.defaultLoaderTimeout;
                if (this.maxConnections > 0)
                    loader.setMaxConnections(this.maxConnections);
                loader.addEventListener("fileload", this.handleFileLoad.bind(this));
                loader.addEventListener("progress", this.handleProgress.bind(this));
                loader.addEventListener("error", this.handleError.bind(this));
                loader.addEventListener("complete", this.handleComplete.bind(this));
                loader.loadManifest(this.manifests[this.loaderCursor]);
            } else {
                //finito
                window.CapecodJsonManager.LogManager.logC("CapecodJsonManager", "LOADING OF " + this.whatis + " ASSETS COMPLETED!", "info");
                this.callback();
            }
        },
        handleComplete: function (evt) {
            //SPACCHETTO GRAFICA
            if (this.missingResx && this.haltOnMissingResource)
                return;
            var queue = evt.target;
            var cursor = this.loaderCursor;
            this.assetsArray[cursor].images = [];
            for (var i = 0; i < this.manifests[cursor].length; i++) {
                this.assetsArray[cursor].images.push(queue.getResult(this.manifests[cursor][i].id));
            }
            if (!this.loaderPercentageRanges)
                if (this.update)
                    this.update(1, this.loaderCursor, true);
            this.loaderCursor++;
            //CONTINUO
            this.load();
        }
        //---
    },
    //---
    FontManager: {
        gFontsFamilies: [],
        fontDisplacement: 0.1,
        loadedGoogleCount: 0,
        gFontsUpdateCacheList: [],
        webfonts: {},
        webFontTxtInst: {},
        useFontDisplacement: true,
        LoadGFonts: function () {
            // var googleObject = {
            //     type: "Google",
            //     loadedFonts: 0,
            //     totalFonts: window.CapecodJsonManager.FontManager.gFontsFamilies.length,
            //     callOnLoad: window.CapecodJsonManager.FontManager.gfontAvailable
            // };

            window.CapecodJsonManager.LogManager.logC("CapecodJsonManager", "EDITOR! OR fontFiles NOT FOUND IN slotConfig - PATCH", "warn");

            for (var i = 0; i < window.CapecodJsonManager.FontManager.gFontsFamilies.length; i++) {
                // if(window.CapecodJsonManager.isEditor) {
                // window.CapecodJsonManager.FontManager.isFontAvailable(window.CapecodJsonManager.FontManager.gFontsFamilies[i], googleObject);
                // } else {
                window.CapecodJsonManager.FontManager.gfontAvailable(window.CapecodJsonManager.FontManager.gFontsFamilies[i], window.CapecodJsonManager.FontManager.gFontsFamilies.length);
                // }
            }
        },
        // isFontAvailable: function (font, obj) {
        //     var timeOut = 200;
        //     var delay = 100;
        //     var interval = 0;
        //     var timeElapsed = 0;

        //     function checkFont() {
        //         var node = document.createElement("span");
        //         node.innerHTML = "giItT1WQy@!-/#";
        //         node.style.position = "absolute";
        //         node.style.left = "-1000px";
        //         node.style.top = "-1000px";
        //         node.style.fontSize = "300px";
        //         node.style.fontFamily = "sans-serif";
        //         node.style.fontVariant = "normal";
        //         node.style.fontStyle = "normal";
        //         node.style.fontWeight = "normal";
        //         node.style.letterSpacing = "0";
        //         document.body.appendChild(node);
        //         var width = node.offsetWidth;
        //         node.style.fontFamily = font + "," + node.style.fontFamily;
        //         var returnVal = false;
        //         if ((node && node.offsetWidth !== width) || timeElapsed >= timeOut) {
        //             obj.loadedFonts++;
        //             if (interval)
        //                 clearInterval(interval);
        //             obj.callOnLoad(font, obj.totalFonts);
        //             returnVal = true;
        //         }
        //         if (node) {
        //             node.parentNode.removeChild(node);
        //             node = null;
        //         }
        //         timeElapsed += delay;
        //         return returnVal;
        //     }

        //     if (!checkFont()) {
        //         interval = setInterval(checkFont, delay);
        //     }
        // },
        gfontAvailable: function (family, totalGoogleCount) {
            window.CapecodJsonManager.FontManager.webfonts[family] = true;
            var txtInst = window.CapecodJsonManager.FontManager.webFontTxtInst && window.CapecodJsonManager.FontManager.webFontTxtInst[family] || [];
            for (var f = 0; f < txtInst.length; ++f) {
                window.CapecodJsonManager.FontManager.addElementsToCache(txtInst[f], window.CapecodJsonManager.FontManager.gFontsUpdateCacheList);
            }
            window.CapecodJsonManager.FontManager.loadedGoogleCount++;
            if (window.CapecodJsonManager.FontManager.loadedGoogleCount == totalGoogleCount) {
                window.CapecodJsonManager.FontManager.updateListCache(window.CapecodJsonManager.FontManager.gFontsUpdateCacheList);
            }
        },
        updateListCache: function (cacheList) {
            for (var i = 0; i < cacheList.length; i++) {
                window.CapecodJsonManager.FontManager.updateTextCache(cacheList[i]);
            }
        },
        addElementsToCache: function (textInst, cacheList) {
            cacheList.push(textInst);
        },
        updateTextCache: function (textField, selector) {
            if (textField !== null && textField !== undefined) {

                //if (window.CapecodJsonManager.isEditor && textField.states) {
                //    textField.fontWeight = textField.states[0].fontWeight;
                //    textField.fontSize = textField.states[0].fontSize;
                //    textField.fontFamily = textField.states[0].fontFamily;
                //    textField.style = textField.states[0].style;
                //    textField.color = textField.states[0].color;
                //}


                if (textField.fontFamily) {
                    textField.font = (textField.fontWeight ? textField.fontWeight : "normal") + " " + (textField.fontSize ? textField.fontSize : "12px") + " '" + textField.fontFamily + "'";
                }

                var securityPaddingBottom = 20; // per non rischiare che font con le "gambe" lunghe vengano tagliati
                var securityPaddingTop = 20; // per non rischiare che font (numeri) "alti" vengano tagliati
                var securityPaddingLeft = 20; // per non rischiare che font vengano tagliati
                var securityPaddingRight = 20; // per non rischiare che font vengano tagliati

                //AGGIORNO LA CACHE DI UN Text
                var oldX = textField.x;
                var oldY = textField.y;
                var oldTextAlign = textField.textAlign;
                textField.textAlign = "left";
                
                var tx = 0; //textField.getMeasuredWidth()/2;
                var ty = 0; //textField.getMeasuredHeight()/2;
                var tw = textField.getMeasuredWidth();
                var th = textField.getMeasuredHeight();
                var linesWidthPercMolt = 1;
                var lineHeight = textField.lineHeight || th;
                //CALCOLO SE MULTILINEA
                var linesCount = 1;
                var ttt = "";
                if (textField.text && textField.text != "") {
                    ttt = "" + textField.text;
                    linesCount = 1 + (ttt.match(/\n/g) || []).length;
                }
                //FIX WIDTH PER MULTILINEA
                if (linesCount > 1) {
                    var txtSpl = ttt.split('\n');
                    var totSpl = 0;
                    var maxSpl = 0;
                    for (var ti = 0; ti < txtSpl.length; ti++) {
                        var txslen = txtSpl[ti].length;
                        totSpl += txslen;
                        if (txslen > maxSpl) maxSpl = txslen;
                    }
                    linesWidthPercMolt = (maxSpl) / totSpl;
                }
                tw *= linesWidthPercMolt;
                //ADDITIONS PER STYLES
                var cacheAdditions = null;
                if (textField.style && textField.style.strokeStyle && textField.style.strokeStyle.lineWidth && textField.style.strokeStyle.lineWidth > 0) {
                    cacheAdditions = {
                        x: (1 + textField.style.strokeStyle.lineWidth) * 2,
                        y: (1 + textField.style.strokeStyle.lineWidth) * 2
                    };
                }
                if (cacheAdditions) {
                    if (cacheAdditions.x) {
                        tx -= cacheAdditions.x;
                        tw += (2 * cacheAdditions.x);
                    }
                    if (cacheAdditions.y) {
                        ty -= cacheAdditions.y;
                        th += (2 * cacheAdditions.y);
                    }
                }

                var offShadowX = 0;
                var offShadowY = 0;
                var negOffShadowX = 0;
                var negOffShadowY = 0;
                
                if (textField.shadow) {
                    offShadowX = Math.abs(textField.shadow.offsetX);
                    offShadowY = Math.abs(textField.shadow.offsetY);
                    negOffShadowX = textField.shadow.offsetX < 0 ? textField.shadow.offsetX : 0;
                    negOffShadowY = textField.shadow.offsetY < 0 ? textField.shadow.offsetY : 0;
                }

                //DO CACHE
                textField.uncache();
                //fix cache position //baseText = 'alphabet'
                ty -= textField.getMeasuredHeight() / (linesCount > 1 ? 2 : 1);

                //ty -= securityPaddingTop;
                //tx -= securityPaddingLeft;

                textField.cache(tx - securityPaddingLeft + negOffShadowX, ty - securityPaddingTop + negOffShadowY, tw + offShadowX + securityPaddingRight + securityPaddingLeft, th + offShadowY + securityPaddingBottom + securityPaddingTop);

                if (textField.stage && textField.shadow) {
                    var ctx = textField.cacheCanvas.getContext('2d');
                    textField.updateContext(ctx);
                    textField.updateCache();
                }

                //ALIGN
                if (oldTextAlign == "left") {
                    //left
                    textField.regX = tw;
                } else if (oldTextAlign == "right") {
                    //right
                    textField.regX = 0;
                } else {
                    //center
                    textField.regX = (tw / 2);
                }
                textField.regY = (th / 2);

                if (cacheAdditions) {
                    textField.regX -= cacheAdditions.x;
                    textField.regY -= cacheAdditions.y;
                }
                textField.x = oldX;
                textField.y = oldY;
                textField.textAlign = oldTextAlign;
                // if (window.CapecodJsonManager.FontManager.useFontDisplacement) {
                //     if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
                //         textField.regY -= (lineHeight * window.CapecodJsonManager.FontManager.fontDisplacement);
                //     } else {
                //         textField.regY += (lineHeight * window.CapecodJsonManager.FontManager.fontDisplacement);
                //     }
                // }
                if (textField.parent) {
                    if (textField.parent.cacheCanvas) textField.parent.updateCache();
                    if (textField.parent.parent && textField.parent.parent.cacheCanvas) textField.parent.parent.updateCache();
                }
                //HIT
                var hit = new createjs.Shape();
                // var boundy = textField.getBounds();
                //var hitTW = Math.min(tw, boundy.width); //no textField.getMeasuredWidth()
                //var hitTH = Math.max(boundy.height, textField.getMeasuredHeight()); //boundy.height; //no th
                if(selector){
                    var sW = CapecodJsonManager.selectorWidth;
                    var sH = CapecodJsonManager.selectorHeight;
                    var dW = sW-tw;
                    var dH = sH-th;
                    hit.graphics.beginFill("rgba(0,0,0,0.9)").drawRect(tx-dW/2, ty-dH/2, sW, sH);      
                    // hit.graphics.beginFill("rgba(0,0,0,0.9)").drawRect(tx, ty, hitTW, hitTH);
                }else{
                    hit.graphics.beginFill("rgba(0,0,0,0.9)").drawRect(tx, ty, tw, th);                    
                }    
                
                textField.hitArea = hit;

                //hit.cache(tx, ty, hitTW, hitTH);                

                // console.log(" TIME: " + new Date().getHours() + ":" + new Date().getMinutes() + ":" + new Date().getSeconds() + ":" + new Date().getMilliseconds() + " CACHATO: " + textField.text + " FONT: " + textField.font);

                //** DEBUG - togliere
                /*
                if (textField.parent && (linesCount > 1 || textField.text == "LINEE") && textField.color != "#ffffff" && textField.color != "#FFFFFF" ) {
                    var hitD = new createjs.Shape();
                    hitD.graphics.beginFill("rgba(0,0,0,0.75)").drawRect(tx, ty, hitTW, hitTH);
                    hitD.regX = textField.regX;
                    hitD.regY = textField.regY;
                    hitD.x = textField.x;
                    hitD.y = textField.y;
                    hitD.name ="HITTIC";
                    hitD.cache(tx, ty, hitTW, hitTH);
                    textField.parent.addChild(hitD); 
                }
                */
                //** DEBUG - fine
            }
        },
        addToFontManager: function (textField) {
            var addToGfont = "";
            for (var i = 0; i < window.CapecodJsonManager.FontManager.gFontsFamilies.length; i++) {
                if (textField.font) {
                    if (textField.font.indexOf(window.CapecodJsonManager.FontManager.gFontsFamilies[i]) > -1) {
                        addToGfont = window.CapecodJsonManager.FontManager.gFontsFamilies[i];
                    }
                } else {
                    //debugger;
                }

            }
            if (addToGfont !== "") {
                if (!window.CapecodJsonManager.FontManager.webfonts[addToGfont]) {
                    window.CapecodJsonManager.FontManager.webFontTxtInst[addToGfont] = window.CapecodJsonManager.FontManager.webFontTxtInst[addToGfont] || [];
                    window.CapecodJsonManager.FontManager.webFontTxtInst[addToGfont].push(textField);
                }
            }
        },
        doUpdateAll: function () {
            window.CapecodJsonManager.FontManager.updateListCache(window.CapecodJsonManager.FontManager.gFontsUpdateCacheList);
        }
    },
    //---
    AudioManager: {
        requireds: ["NormLoop",
            //"BonusLoop",
            "LineWinGeneric",
            "LineWinScatter",
            "TotalWin0",
            //"MaxWin",
            //"CreditGain",
            //"ReelPlay",
            "ReelStop"
            //"BonusClick",
            //"BonusWin",
            //"BonusLose"
        ],
        _requiredsToVerify: [],
        verifyAudioSprite: function (audioSprite) {
            window.CapecodJsonManager.AudioManager._requiredsToVerify = JSON.parse(JSON.stringify(window.CapecodJsonManager.AudioManager.requireds));
            for (var asi = 0; asi < audioSprite.length; asi++) {
                var audioSpriteItems = audioSprite[asi].data.audioSprite;
                for (var asj = 0; asj < audioSpriteItems.length; asj++) {
                    var requiredPos = window.CapecodJsonManager.AudioManager._requiredsToVerify.indexOf(audioSpriteItems[asj].id);
                    if (requiredPos >= 0) {
                        window.CapecodJsonManager.AudioManager._requiredsToVerify.splice(requiredPos, 1);
                    }
                }
            }
            if (window.CapecodJsonManager.AudioManager._requiredsToVerify.length > 0) {
                //error
                window.CapecodJsonManager.LogManager.logC("CapecodJsonManager", "AUDIOSPRITE VERIFY: MISSING REQUIRED ITEMS", "error");
                window.CapecodJsonManager.LogManager.logC("CapecodJsonManager", window.CapecodJsonManager.AudioManager._requiredsToVerify, "error");
            } else {
                //passed
                window.CapecodJsonManager.LogManager.logC("CapecodJsonManager", "AUDIOSPRITE VERIFY: OK", "trace");
                //console.log()
            }
        },
        playSound: function (id, volume, loop) {
            //if (window.model) window.model.log("GLOBALPLAY: " + id + " , " + volume + " , " + loop, 7);
            var _vol = 1;
            /*if (volume && volume >= 0) _vol = volume;*/
            //LAVA
            //if(volume) valuta false anche se volume == 0 mentre if(volume >= 0) valuta true anche se volume � null
            if ((volume == 0) || (volume > 0)) _vol = volume;

            var _loo = 1;
            /*if (loop && loop >= -1) _loo = loop;*/
            if (loop >= -1) _loo = loop;
            var ppc = new createjs.PlayPropsConfig().set({ interrupt: createjs.Sound.INTERRUPT_EARLY, loop: _loo, volume: _vol });
            //console.log("PLAY", id, ppc);
            return createjs.Sound.play(id, ppc);
        }
    },
    LogManager: {
        logC: function (channel, msg, level, group) {
            if (window.cclog) {
                window.cclog.logC(channel, msg, level, group);
            }
            else {
                if (!level)
                    level = "log";
                else if (!console[level])
                    level = "log";
                console[level](channel + ":" + msg);
            }
        }
    },
    //---
    ResizablesManager: {
        resizables: [],
        add: function (object, capecodSnippet) {
            window.CapecodJsonManager.ResizablesManager.resizables.push({ obj: object, props: capecodSnippet });
        },
        loadObjSnippet: function(object) {
            var res = window.CapecodJsonManager.ResizablesManager.resizables.filter(function (value) { return value.obj == object });
            if (res.length > 0 && res[0].props) {
                return res[0].props;
            }
            return null;
        },
        execute: function (device) {
            //device = "mh" || "mv"
            window.CapecodJsonManager.ResizablesManager.resizables.forEach(function (item) {

                var baseScale = 1;
                if (item.props.type == "Sprite" || item.props.type == "Video") baseScale = window.CapecodJsonManager._assetsScale;

                var baseCfg = {
                    "x" : item.props.x != null ? item.props.x : 0,
                    "y": item.props.y != null ? item.props.y : 0,
                    "scaleX": item.props.scaleX != null ? item.props.scaleX : 1,
                    "scaleY": item.props.scaleY != null ? item.props.scaleY : 1,
                    "visible": item.props.visible != null ? item.props.visible : true,
                    "rotation": item.props.rotation != null ? item.props.rotation : 0
                }

                if (device == "mh" && item.props.mobileH) {
                    if (item.props.mobileH.x != null) baseCfg.x = item.props.mobileH.x;
                    if (item.props.mobileH.y != null) baseCfg.y = item.props.mobileH.y;
                    if (item.props.mobileH.scaleX != null) baseCfg.scaleX = item.props.mobileH.scaleX;
                    if (item.props.mobileH.scaleY != null) baseCfg.scaleY = item.props.mobileH.scaleY;
                    if (item.props.mobileH.visible != null) baseCfg.visible = item.props.mobileH.visible;
                    if (item.props.mobileH.rotation != null) baseCfg.rotation = item.props.mobileH.rotation;
                }

                if (device == "mv" && item.props.mobileV) {
                    if (item.props.mobileV.x != null) baseCfg.x = item.props.mobileV.x;
                    if (item.props.mobileV.y != null) baseCfg.y = item.props.mobileV.y;
                    if (item.props.mobileV.scaleX != null) baseCfg.scaleX = item.props.mobileV.scaleX;
                    if (item.props.mobileV.scaleY != null) baseCfg.scaleY = item.props.mobileV.scaleY;
                    if (item.props.mobileV.visible != null) baseCfg.visible = item.props.mobileV.visible;
                    if (item.props.mobileV.rotation != null) baseCfg.rotation = item.props.mobileV.rotation;
                }

                if (baseCfg.x != null) item.obj.x = baseCfg.x;
                if (baseCfg.y != null) item.obj.y = baseCfg.y;
                
                if (baseCfg.scaleX != null) item.obj.scaleX = baseCfg.scaleX * baseScale;
                if (baseCfg.scaleY != null) item.obj.scaleY = baseCfg.scaleY * baseScale;
                if (baseCfg.visible != null) item.obj.visible = baseCfg.visible;
                if (baseCfg.rotation != null) item.obj.rotation = baseCfg.rotation;

            });
        }
    }
    //---
};
//dollar function link
function $_(funcName, p1, p2, p3, p4, p5, p6, p7, p8, p9) {
    switch (funcName) {
        case "appStart":
            var app = new p1.App();
            setTimeout(function mainStart() { app.start(); }, 250);
            break;
        default:
            //ome times can be called before app start
            if (window.managers != null)
                return window.managers.legacyCoreManager.dollar(funcName, p1, p2, p3, p4, p5, p6, p7, p8, p9);
    }
}