//ELETTROLUDICA
//(c)2021 ALESSANDRO DI MICHELE
//---------------------
var APP_NAME = "elettroLudica";
var APP_VERSION = "v1.0";
var inDebug = true; //** PRODUZIONE: FALSE
var isApp = true; //se è applicazione per mobile (non web) 	//unused
var isMobile = (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)); //se è browser per mobile
var isHTTPS = (window.location.href.indexOf("https") >= 0); //se è https
var isLOCAL = (window.location.href.indexOf("localhost") >= 0 || window.location.href.indexOf("alienware") >= 0); //se è local
//---------------------
var appConfig = {
	modules: {
		catalog: true, //sempre true
		news: true,
		events: true,
		gallery: false,
		pubs: false,
	},
	home: {
		jumbo:true,
		banner:true,
		banner2:true,
		faqs:true,
		news: true,
		events: true,
		gallery: true,
		pubs: true,
		catalog: true,
	},
	pages: {
		about: true,
		location: true,
		services: false,
		happenings: false,
		contacts: true,
		donate: true,
		credits: true,
		orari: true,
		mappa: true,
		tour: false,
		qr: true,
	},
}
//---------------------
var baseLoadTimeout = 100;
var alertAutocloseTimeout = 2000;
var baseUrl = "/";
//---------------------
var lang = "it";
var languagePages = labels[lang].languagePages;
var languageLabels = labels[lang].languageLabels;
var languageHelps = labels[lang].languageHelps;
const formatter = new Intl.NumberFormat('it-IT', {
	style: 'currency',
	currency: 'EUR',
	minimumFractionDigits: 2
});
Array.prototype.shuffle = function() {
	let m = this.length, i;
	while (m) {
		i = (Math.random() * m--) >>> 0;
		[this[m], this[i]] = [this[i], this[m]]
	}
	return this;
}
//---------------------
//database
var catalog = JSON.parse(JSON.stringify(dataBase));
var catalogRandom = (JSON.parse(JSON.stringify(dataBase))).shuffle();
/*
for (var d=0; d<dataBase.length; d++) {
	var obbo = JSON.parse(JSON.stringify(obbo[d]))
	obbo.index = d;
	catalog.push(obbo)
}
 */
var filterz = {Tipologia: [], Produttore: [], Nazione:[], Anno: [], Collocazione: []};
for (var d=0; d<dataBase.length; d++) {
	if (filterz.Tipologia.indexOf(dataBase[d].Tipologia)<0 && dataBase[d].Tipologia != ""  && dataBase[d].Tipologia.indexOf("?")<0) filterz.Tipologia.push(dataBase[d].Tipologia)
	if (filterz.Produttore.indexOf(dataBase[d].Produttore)<0 && dataBase[d].Produttore != ""  && dataBase[d].Produttore.indexOf("?")<0) filterz.Produttore.push(dataBase[d].Produttore)
	if (filterz.Nazione.indexOf(dataBase[d].Nazione)<0 && dataBase[d].Nazione != "" && dataBase[d].Nazione.indexOf("?")<0) filterz.Nazione.push(dataBase[d].Nazione)
	if (filterz.Anno.indexOf(dataBase[d].Anno)<0 && dataBase[d].Anno != ""  && dataBase[d].Anno.indexOf("?")<0) filterz.Anno.push(dataBase[d].Anno)
	if (filterz.Collocazione.indexOf(dataBase[d].Collocazione)<0 && dataBase[d].Collocazione != ""  && dataBase[d].Collocazione.indexOf("?")<0) filterz.Collocazione.push(dataBase[d].Collocazione)
}
filterz.Tipologia.sort();
filterz.Produttore.sort();
filterz.Nazione.sort();
filterz.Anno.sort();
filterz.Collocazione.sort();
var sTipologia = [];
var sProduttore = [];
var sNazione = [];
var sAnno = [];
var sCollocazione = [];
var t = 0;
for (var t=0; t<filterz.Tipologia.length; t++) {
	sTipologia.push( {label: filterz.Tipologia[t]} )
}
for (var t=0; t<filterz.Produttore.length; t++) {
	sProduttore.push( {label: filterz.Produttore[t]} )
}
for (var t=0; t<filterz.Nazione.length; t++) {
	sNazione.push( {label: filterz.Nazione[t]} )
}
for (var t=0; t<filterz.Anno.length; t++) {
	sAnno.push( {label: filterz.Anno[t]} )
}
for (var t=0; t<filterz.Collocazione.length; t++) {
	sCollocazione.push( {label: filterz.Collocazione[t]} )
}
//---------------------
if (!user) var user = {utente:null};
//---------------------
var dataToUse = {};
var dataPrevNext = {};
var temp = { //DI SISTEMA: USATA PER INTESCAMBIO
	isApp: isApp,
	isMobile:isMobile,
	isHTTPS:isHTTPS,
	isLOCAL:isLOCAL,

	routeName: "",
	pageSize: 50,
	currentPage: 0,
	item : dataToUse,
	prevNext : dataPrevNext
};
//---------------------
var dati = null;
function createModel() {
	dati = {
		database: dataBase,
		catalog: catalog,
		catalogRandom: catalogRandom,

		news: contentBase.news,
		events: contentBase.events,
		pubs: contentBase.pubs,
		gallery: contentBase.gallery,
		jumbo: contentBase.jumbo,
		banner: contentBase.banner,
		faqs: contentBase.faqs,
		credits: contentBase.credits,
		orari: contentBase.orari,
		contentInfo: contentBaseInfo,
		catalogInfo: dataBaseInfo,

		filterTipologia: sTipologia,
		filterProduttore: sProduttore,
		filterNazione: sNazione,
		filterAnno: sAnno,
		filterCollocazione: sCollocazione,

		languagePages: languagePages,  //DI SISTEMA: LABELS
		languageLabels: languageLabels,  //DI SISTEMA: LABELS
		languageHelps: languageHelps,  //DI SISTEMA: HELPS

		id: -1,  //DI SISTEMA: USATA PER WATCH
		id2: -1, //DI SISTEMA: USATA PER WATCH
		index2: -1, //DI SISTEMA: USATA PER WATCH

		temp: temp, //DI SISTEMA: USATA PER INTESCAMBIO
		config: appConfig, //DI SISTEMA: USATA PER INTESCAMBIO

		app: { //DI SISTEMA
			name: APP_NAME,
			version: APP_VERSION
		}
	};
}

//---------------------
