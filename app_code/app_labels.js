var versionLabels = "v1.0";
var labels = {
    "it": {
        "languagePages": {
            "page-home": {label: "Home Page"},
            "page-about": {label: "Chi siamo"},
            "page-location": {label: "Dove siamo"},
            "page-services": {label: "Servizi"},
            "page-happenings": {label: "Happening"},
            "page-contacts": {label: "Contatti"},
            "page-donate": {label: "Donazioni"},
            "page-credits": {label: "Credits"},
            "page-orari": {label: "Orari"},
            "page-catalog": {label: "Catalogo"},
            "page-search": {label: "Risultati Ricerca"},
            "page-map": {label: "Mappa interattiva"},
            "page-map2": {label: "Mappa"},
            "page-tour": {label: "Virtual Tour"},
            "page-tour2": {label: "Tour"},
            "page-qr": {label: "Scanner QR"},
            "page-qr2": {label: "QR"},
            "page-news": {label: "News"},
            "page-news-home": {label: "News in evidenza"},
            "page-events": {label: "Eventi"},
            "page-events-home": {label: "Eventi importanti"},
            "page-gallery": {label: "Media Gallery"},
            "page-gallery-home": {label: "In evidenza dalla Media Gallery"},
            "page-pubs": {label: "Pubblicazioni"},
            "page-pubs-home": {label: "Pubblicazioni di rilievo"},
        },
        "languageLabels": {
            "footer-title-col1": {label: "Museo"},
            "footer-title-col2": {label: "Scopri"},
            "footer-title-col3": {label: "Contatti"},
            "footer-title-col3b": {label: "Scrivici"},
            "footer-diritti": {label: "Tutti i diritti riservati"},
            "header-title-col1": {label: "Museo"},
            "header-title-col2": {label: ""},
            "header-title-col3": {label: ""},
            "header-title-col4": {label: "Goodies"},
            "header-title-col5": {label: "Info"},
            "label-items": {label: "Elementi"},
            "label-applica": {label: "Applica"},

            "label-ordinamento-Inventario": {label: "Id"},
            "label-ordinamento-Denominazione": {label: "Nome"},
            "label-ordinamento-Tipologia": {label: "Tipologia"},
            "label-ordinamento-Produttore": {label: "Produttore"},
            "label-ordinamento-Nazione": {label: "Nazione"},
            "label-ordinamento-Anno": {label: "Anno"},
            "label-ordinamento-Collocazione": {label: "Collocazione"},
            "label-ordinamento-Asc": {label: "asc"},
            "label-ordinamento-Desc": {label: "disc"},

            "label-catalog-inallestimento": {label: "Nota: Il Catalogo è ancora in allestimento, la lista è provvisoria"},
            "label-search-inallestimento": {label: "Nota:  Il Catalogo è ancora in allestimento, la ricerca è stata effettuata su una lista provvisoria"},

            "label-filtro-Tipologia": {label: "Tipologia"},
            "label-filtro-Produttore": {label: "Produttore"},
            "label-filtro-Nazione": {label: "Nazione"},
            "label-filtro-Anno": {label: "Anno"},
            "label-filtro-Collocazione": {label: "Collocazione"},

            "label-filtri-resetta": {label: "Resetta"},
            "label-filtri-resetta2": {label: "Resetta filtri e ordinamento"},
            "label-filtri-selected": {label: "Scelti"},

            "label-table-Inventario": {label: "ID"},
            "label-table-Denominazione": {label: "Nome"},
            "label-table-Tipologia": {label: "Tipologia"},
            "label-table-Produttore": {label: "Produttore"},
            "label-table-Nazione": {label: "Nazione"},
            "label-table-Anno": {label: "Anno"},
            "label-table-Collocazione": {label: "Collocazione"},

            "label-vai-catalogo": {label: "Vai al catalogo!"},
            "label-vai-catalog": {label: "Torna al catalogo"},
            "label-vai-contatti": {label: "Vai ai contatti"},
            "label-vai-news": {label: "Vai alle News"},
            "label-vai-pubs": {label: "Vai alle Publicazioni"},
            "label-vai-gallery": {label: "Vai alla Multimedia Gallery"},
            "label-vai-events": {label: "Vai agli Eventi"},

            "label-faqs-title": {label: "Frequently Asked Questions"},
            "label-faqs-text": {label: " Ecco una serie di risposte alle domande più frequenti che ci vengono fatte. Se non trovate quello che cercate non esitate comunque a contattarci!"},

            "label-qr-success": {label: "Bene!<br>Ho trovato l'elemento!"},
            "label-qr-error": {label: "Attenzione!<br>Codice non corretto."},
            "label-qr-info": {label: "Inquadra i codici QR che trovi nel museo."},
            "label-qr-notavailable": {label: "Per far funzionare il lettore QR su dispositivi mobili, accedi al sito tramite <b>https</b> (http sicuro) e non <i>http</i> (semplice).<br>E' raccomandato il browser Chrome."},
        },
        "languageHelps": {

        }

    },
    "en": {
        "languagePages": {},
        "languageLabels": {},
        "languageHelps": {}
    }
};

