var versionComms = "v1.0";
// --------------------
function comms_loadData(callback) {
    if (!user)	user = {
        utente: {
            "nome": "Associazione",
            "cognome": "Elettroludica",
            "sesso": "M",
            "indirizzo": "Via Cavour, Avezzano (AQ)",
            "email": "info@elettroludica.it",
            "sito": "https://www.elettroludica.it",
        }
    };
    setTimeout(callback, baseLoadTimeout);
}
// ---
function comms_getIstanza(callbackIstanza, id) {
    if (inDebug) console.log("comms_getIstanza CALL", id);
    var esito = { isOk: true };
    var istanzaId = id;
}
function comms_logOut() {
    if (inDebug) console.log("CALLING LOGOUT");
    user = null;
    var goto = "/";
    var allCookies = document.cookie.split(';');
    for (var i = 0; i < allCookies.length; i++)
        document.cookie = allCookies[i] + "=;expires="
            + new Date(0).toUTCString();
    if (inDebug) console.log("redirecting to ", goto);
    window.location.href=goto;
}
//--------------------
//UTILS
//--------------------
function utils_getArrayIndexFromId (arr, id) {
    var indx = 0;
    for (var d=0; d< arr.length; d++) {
        if (id == arr[d].id) indx = d;
    }
    return indx;
}
//---
function utils_getUrlParameter(sParam) {
    var sPageURL = window.location.search.substring(1),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;
    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
        }
    }
}
//---
function utils_disableButton(buttonObj, doDisable) {
    //console.log("CLICK", buttonObj, "DISABLE", doDisable);
    if (doDisable === true) {
        if (buttonObj.is('button')) {
            buttonObj.prop('disabled', true);
        } else {
            buttonObj.addClass('disabled');
        }
    } else {
        if (buttonObj.is('button')) {
            buttonObj.prop('disabled', false);
        } else {
            buttonObj.removeClass('disabled');
        }
    }
}
function utils_successAlert(esito) {
    if (esito.isOk) {
        $('#alertSuccess').show()
        setTimeout(function() {
            $("#alertSuccess").hide();
        }, alertAutocloseTimeout);
    } else {
        $('#alertWarning').show()
        setTimeout(function() {
            $("#alertWarning").hide();
        }, alertAutocloseTimeout*2);
    }
}
function catalog_checkImage(imageSrc, good, bad) {
        var img = new Image();
        img.onload = good;
        img.onerror = bad;
        img.src = imageSrc;
}
function catalog_putImage(imageSrc, imageId, imageDefault) {
    var _imageSrc = imageSrc
    var _imageId = "#" + imageId;
    var _imageDefault = imageDefault;
    var img = new Image();
    img.onload = function good() {
        jQuery(_imageId).attr("src", _imageSrc)
    };
    img.onerror = function bad() {
        jQuery(_imageId).attr("src", _imageDefault)
    };
    img.src = imageSrc;
}
function catalog_applyFilters() {
    //Tipologia Produttore Nazione Anno
    var useTipologia = null;
    var useProduttore = null;
    var useAnno = null;
    var useCollocazione = null;
    var useNazione = null;
    var useDenominazione = [];
    var filtroTipologia = jQuery("#filter-Tipologia").find("input[type=checkbox]:checked");
    if (filtroTipologia.length > 0) {
        useTipologia = [];
        filtroTipologia.each(function( index ) {
            useTipologia.push($( this ).val())
        });
    }
    var filtroProduttore = jQuery("#filter-Produttore").find("input[type=checkbox]:checked");
    if (filtroProduttore.length > 0) {
        useProduttore = [];
        filtroProduttore.each(function( index ) {
            useProduttore.push($( this ).val())
        });
    }
    var filtroNazione = jQuery("#filter-Nazione").find("input[type=checkbox]:checked");
    if (filtroNazione.length > 0) {
        useNazione = [];
        filtroNazione.each(function( index ) {
            useNazione.push($( this ).val())
        });
    }
    var filtroAnno = jQuery("#filter-Anno").find("input[type=checkbox]:checked");
    if (filtroAnno.length > 0) {
        useAnno = [];
        filtroAnno.each(function( index ) {
            useAnno.push($( this ).val())
        });
    }
    var filtroCollocazione = jQuery("#filter-Collocazione").find("input[type=checkbox]:checked");
    if (filtroCollocazione.length > 0) {
        useCollocazione = [];
        filtroCollocazione.each(function( index ) {
            useCollocazione.push($( this ).val())
        });
    }
    if (dati.temp.routeName == "page-search") if (dati.id) {
        if (dati.id.indexOf(" ")<0) {
            useDenominazione = [dati.id.toLowerCase()]
        } else {
            useDenominazione = dati.id.toLowerCase().split(" ");
        }
    }

    var catalogF =  [];
    if (useTipologia || useProduttore || useAnno || useNazione || useCollocazione || useDenominazione.length > 0) {
        catalogF = [];
        for (var d=0; d<dataBase.length; d++) {
            var toIns = dataBase[d];
            var insable = true;
            if (useTipologia && useTipologia.indexOf(toIns.Tipologia) < 0 ) insable = false;
            if (useProduttore && useProduttore.indexOf(toIns.Produttore) < 0 ) insable = false;
            if (useAnno && useAnno.indexOf(toIns.Anno) < 0 ) insable = false;
            if (useNazione && useNazione.indexOf(toIns.Nazione) < 0 ) insable = false;
            if (useCollocazione && useCollocazione.indexOf(toIns.Collocazione) < 0 ) insable = false;
            if (useDenominazione.length > 0) {
                var qualcunoPresenteDenominazione = 0;
                var qualcunoPresenteProduttore = 0;
                var qualcunoPresenteAnno = 0;
                var qualcunoPresenteNazione = 0;
                for (var c=0; c< useDenominazione.length; c++) {
                    var toFind = useDenominazione[c];
                    if (toIns.Denominazione.toLowerCase().indexOf(toFind) >= 0) qualcunoPresenteDenominazione++;
                }
                for (var c=0; c< useDenominazione.length; c++) {
                    var toFind = useDenominazione[c];
                    if (toIns.Produttore.toLowerCase().indexOf(toFind) >= 0) qualcunoPresenteProduttore++;
                 }
                for (var c=0; c< useDenominazione.length; c++) {
                    var toFind = useDenominazione[c];
                    if (toIns.Anno.toLowerCase().indexOf(toFind) >= 0) qualcunoPresenteAnno++;
                }
                for (var c=0; c< useDenominazione.length; c++) {
                    var toFind = useDenominazione[c];
                    if (toIns.Nazione.toLowerCase().indexOf(toFind) >= 0) qualcunoPresenteNazione++;
                }
                if ( !(qualcunoPresenteDenominazione ==useDenominazione.length ||
                    qualcunoPresenteProduttore ==useDenominazione.length ||
                    qualcunoPresenteAnno ==useDenominazione.length ||
                    qualcunoPresenteNazione ==useDenominazione.length ) ) {
                    insable = false;
                }
            }
            if (insable) catalogF.push(toIns);
        }
    } else {
        catalogF = JSON.parse(JSON.stringify(dataBase))
    }
    dati.catalog = catalogF;
    catalog_applySort();
}
function catalog_applySort() {
    var ordField = jQuery("#filter-Ordinamento").val();
    var ordDir = jQuery("#filter-Direzione").val();
    dati.catalog.sort(utils_compareValues(ordField, ordDir));
}
function utils_compareValues(key, order = 'asc') {
    return function innerSort(a, b) {
        if (!a.hasOwnProperty(key) || !b.hasOwnProperty(key)) {
            // property doesn't exist on either object
            return 0;
        }

        const varA = (typeof a[key] === 'string')
            ? a[key].toUpperCase() : a[key];
        const varB = (typeof b[key] === 'string')
            ? b[key].toUpperCase() : b[key];

        let comparison = 0;
        if (varA > varB) {
            comparison = 1;
        } else if (varA < varB) {
            comparison = -1;
        }
        return (
            (order === 'desc') ? (comparison * -1) : comparison
        );
    };
}
//-------------------
function getLibVersion() {
    console.log("---")
    console.log(APP_NAME, APP_VERSION)
    console.log("Main", versionMain)
    console.log("Comms", versionComms)
    console.log("Labels", versionLabels)
    console.log("---")
    console.log("MOBILE", dati.temp.isMobile, "HTTPS", dati.temp.isHTTPS)
    console.log("---")
}
//--------------------
//ANALYTICS
//--------------------
function gaTrack(path, title) {
    //ga('set', { page: path, title: title });
    //ga('send', 'pageview');
    if (gtagO != undefined && gtagO != null) {
        if (title !== undefined && title !== null) {
            gtagO({event: 'navigation', page: path, title: title})
        } else {
            gtagO({event: 'outboundlink', url: path})
        }
    }
}
//--------------------
function recomputeData() {
    dati.temp.item = {};
    dati.temp.prevNext = {};
    var prevvo = null;
    var trovato = false;
    if (dati.id != -1) {
        for (var i = 0; i < dati.catalog.length; i++) {
            if (dati.catalog[i].id == dati.id) {
                dati.temp.item = dati.catalog[i];
                trovato = true;
                if (!prevvo) prevvo = dati.catalog[dati.catalog.length-1];
                dati.temp.prevNext.prev = prevvo;
                if (i<dati.catalog.length-1)  dati.temp.prevNext.next = dati.catalog[i+1]
                else  dati.temp.prevNext.next = dati.catalog[0]
            } else {
                prevvo = dati.catalog[i];
            }
        }
    } else {
        dati.temp.prevNext.prev = dati.catalog[  Math.floor(Math.random()* dati.catalog.length-1 ) ];
        dati.temp.prevNext.next = dati.catalog[  Math.floor(Math.random()* dati.catalog.length-1 ) ];
    }
    if (!trovato) {
        dati.temp.item = {id:0};
    }
    console.log("DATA TO USE", dati.temp.item, dati.temp.prevNext)
}