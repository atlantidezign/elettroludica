if (window.location.href.toLowerCase().indexOf("alienware")>=0) inDebug = true; //** TOGLIERE
var versionMain = "v1.0";
//start
if (inDebug) console.log(APP_NAME+ " " + APP_VERSION);
if (inDebug) console.log("> 1/4 SETUPPING");
//components: components common
const commonHeader1 = httpVueLoader('./app_components/common/header1.vue');
const commonHeader2 = httpVueLoader('./app_components/common/header2.vue');
const commonHeader3 = httpVueLoader('./app_components/common/header3.vue');
const commonFooter = httpVueLoader('./app_components/common/footer.vue');
const commonSearch = httpVueLoader('./app_components/common/search.vue');
const commonModalLogin = httpVueLoader('./app_components/common/modal-login.vue');
const commonModalRegistration = httpVueLoader('./app_components/common/modal-registration.vue');
const commonModalResetPassword = httpVueLoader('./app_components/common/modal-reset-password.vue');
const commonModalResetPasswordSuccess = httpVueLoader('./app_components/common/modal-reset-password-success.vue');
const commonModalBookNow = httpVueLoader('./app_components/common/modal-book-now.vue');
const commonModalAskQuestion = httpVueLoader('./app_components/common/modal-ask-question.vue');
const commonMessageSuccess = httpVueLoader('./app_components/common/message-success.vue');
const commonMessageError = httpVueLoader('./app_components/common/message-error.vue');
//components: pages root
const pageHome = httpVueLoader('./app_pages/root/home.vue');
const pageLocation= httpVueLoader('./app_pages/root/location.vue');
const pageAbout= httpVueLoader('./app_pages/root/about.vue');
const pageContacts= httpVueLoader('./app_pages/root/contacts.vue');
const pageServices= httpVueLoader('./app_pages/root/services.vue');
const pageDonate= httpVueLoader('./app_pages/root/donate.vue');
const pageHappenings= httpVueLoader('./app_pages/root/happenings.vue');
const pageCredits= httpVueLoader('./app_pages/root/credits.vue');
const pageOrari= httpVueLoader('./app_pages/root/orari.vue');
const pageMap= httpVueLoader('./app_pages/root/map.vue');
const pageQr= httpVueLoader('./app_pages/root/qr.vue');
const pageTour= httpVueLoader('./app_pages/root/tour.vue');
//components: poges root + child
const pageCatalog= httpVueLoader('./app_pages/root/catalog.vue');
const pageItem= httpVueLoader('./app_pages/childs/item.vue');
const pageGallery= httpVueLoader('./app_pages/root/gallery.vue');
const pageMedia= httpVueLoader('./app_pages/childs/media.vue');
const pageNews= httpVueLoader('./app_pages/root/news.vue');
const pageArticle= httpVueLoader('./app_pages/childs/article.vue');
const pageEvents= httpVueLoader('./app_pages/root/events.vue');
const pageEvent= httpVueLoader('./app_pages/childs/event.vue');
const pagePubs= httpVueLoader('./app_pages/root/pubs.vue');
const pagePub= httpVueLoader('./app_pages/childs/pub.vue');

//routes
const routes = [
    { path: '/', component: pageHome, name: "page-home0", props: true  },
    { path: '/home', component: pageHome, name: "page-home", props: true  },

    { path: '/location', component: pageLocation, name: "page-location", props: true  },
    { path: '/about', component: pageAbout, name: "page-about", props: true  },
    { path: '/contacts', component: pageContacts, name: "page-contacts", props: true  },
    { path: '/services', component: pageServices, name: "page-services" , props: true },
    { path: '/donate', component: pageDonate, name: "page-donate" , props: true },
    { path: '/happenings', component: pageHappenings, name: "page-happenings" , props: true },
    { path: '/credits', component: pageCredits, name: "page-credits" , props: true },
    { path: '/orari', component: pageOrari, name: "page-orari" , props: true },

    { path: '/map', component: pageMap, name: "page-map" , props: true },
    { path: '/qr', component: pageQr, name: "page-qr" , props: true },
    { path: '/tour', component: pageTour, name: "page-tour", props: true  },

    { path: '/catalog', component: pageCatalog, name: "page-catalog" , props: true },
    { path: '/search/:id', component: pageCatalog, name: "page-search" , props: true },
    { path: '/item/:id', component: pageItem, name: "page-item" , props: true },
    { path: '/i/:id', component: pageItem, name: "page-item0" , props: true },
    { path: '/gallery', component: pageGallery, name: "page-gallery", props: true  },
    { path: '/media/:id', component: pageMedia, name: "page-media", props: true  },
    { path: '/news', component: pageNews, name: "page-news", props: true  },
    { path: '/article/:id', component: pageArticle, name: "page-article", props: true  },
    { path: '/events', component: pageEvents, name: "page-events" , props: true },
    { path: '/event/:id', component: pageEvent, name: "page-event" , props: true },
    { path: '/pubs', component: pagePubs, name: "page-pubs", props: true  },
    { path: '/pub/:id', component: pagePub, name: "page-pub" , props: true }
];
//router
const router = new VueRouter({
  routes: routes,
  default: pageHome,
    scrollBehavior: function (to, from, savedPosition) {
        if (savedPosition) {
            return savedPosition
        } else {
            return { x: 0, y: 0 }
        }
    }
});
//app
var app;
$( document ).ready(function() {
    if (inDebug) console.log("> 2/4 INITIALIZING");
    initApp();
});
function initApp() {
    if (inDebug) console.log("> 3/4 LOADING DATA");
    comms_loadData(startApp);
}
//start
function startApp() {
    createModel();
    app = new Vue({
        data() {
            return dati
        }, components: {
            'common-header1': commonHeader1,
            'common-header2': commonHeader2,
            'common-header3': commonHeader3,
            'common-footer': commonFooter,
            'common-search': commonSearch,
            'modal-login': commonModalLogin,
            'modal-registration': commonModalRegistration,
            'modal-reset-password': commonModalResetPassword,
            'modal-reset-password-success': commonModalResetPasswordSuccess,
            'modal-book-now': commonModalBookNow,
            'modal-ask-question': commonModalAskQuestion,
            'message-success': commonMessageSuccess,
            'message-error': commonMessageError,
        }, router: router,
        watch: {
            utente: function (nnn, ooo) {
                //check is logged
                if (!dati.utente) {
                    router.push('/home')
                }
            },
            $route(to, from) {
                dati.temp.routeName = this.$route.name;
                dati.id = this.$route.params.id || -1;
                dati.id2 = this.$route.params.id2 || -1;
                dati.index2 = -1;
                if (dati.id != -1) {
                    recomputeData();
                    catalog_applyFilters();
                }
                //--- Google Tag Manager
                gaTrack(this.$route.fullPath, this.$route.name);
                //---
                setTimeout(function updateScripts() {
                    executePageScriptsOnce();
                    executePageScripts();
                    checkmode();
                }, 300);
                //---
                if (this.$route.name.indexOf("home") < 0)  {
                    $("body").removeClass("no-padding");
                } else {
                    $("body").addClass("no-padding");
                }
                /*
                if (! ( $("body").hasClass("dark") || $("body").hasClass("dark-loaded") ) ) {
                        $("#js-panel").removeClass("white-color");
                        $(".mobile-panel").removeClass("white-color");
                    }
                */
            }
        }
    }).$mount('#app');
    if (inDebug) console.log("> 4/4 STARTING UP");

    //END PRELOAD
    //setTimeout(function anPreloadEnd() { }, 1100)
}
